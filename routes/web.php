<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('bpum.count');
// });
// Route::get('/daftarBPUMtahap2', function () {
//     return view('bpum.count');
// });

Route::get('/', function () {
    return view('bpum.akhir');
});
Route::get('/daftarBPUMtahap2', function () {
    return view('bpum.akhir');
});
Route::get('/cekdatatahap2', function () {
    return view('bpum.index1');
});

Route::get('/cekdt2', function () {
    return view('bpum.cekdt2');
});
Route::post('/rescek', 'cekdt2Controller@show');

// Route::get('/', 'BpumController@index');
// Route::get('/daftarBPUMtahap2', 'BpumController@index');

Route::get('/daftarBPUMtahap2121', 'BpumController@index');
Route::post('/cekdata2', 'cekdt2Controller@show');
Route::post('/addbpum', 'BpumController@store');
Route::get('/monitoringBPUMtahap2', 'BpumController@monev');
Route::post('/carikelbpum/{id}','BpumController@cari');
Route::get('/tanda/terima/0/1/bpum/{id}', 'cekdt2Controller@print');
Auth::routes();
Auth::routes(['verify' => true]);



Route::get('/home', function () {
    return view('welcome');
})->middleware('verified');
Route::group(['middleware' => ['auth','checkRole:superadmin']], function () {
    Route::get('/dashboard', 'HomeController@index')->name('home');

    Route::get('/main', 'MainController@index');

    // Route::get('/users', 'UsersController@index')->name('users.index');
    
    Route::get('/users', 'UserController@index');
    Route::get('/users/{id}', 'UserController@show');
    Route::get('/users/{id}/edit', 'UserController@edit');
    Route::patch('/users/{id}', 'UserController@update');

    Route::get('/binaans', 'BinaansController@index')->name('home');

    Route::get('/binaans/Mikro', 'BinaansController@mikro')->name('home');

    Route::get('/binaans/Kecil', 'BinaansController@kecil')->name('home');

    Route::get('/binaans/Menengah', 'BinaansController@menengah')->name('home');

    Route::get('/add', 'BinaansController@create')->name('home');

    Route::post('/create', 'BinaansController@store')->name('home');

    Route::get('/binaans/{pribadi}', 'BinaansController@show')->name('home');

    Route::get('/view/0/sk/umkm/out/{id}', 'OutController@show');

    Route::delete('/binaans/{pribadi}', 'BinaansController@destroy')->name('home');

    Route::get('/binaans/{pribadi}/edit', 'BinaansController@edit')->name('home');

    Route::patch('/binaans/{pribadi}', 'BinaansController@update')->name('home');

    Route::get('/monitoring', 'MonitoringController@index')->name('home');
    
    Route::get('/monitoring/upload/{id}', 'MonitoringController@create')->name('home');
    
    Route::post('/upload/proses', 'MonitoringController@proses_upload');
    
    Route::delete('/upload/hapus/{id}', 'MonitoringController@destroygambar');

    Route::get('/monev', 'MonitoringController@indexshow')->name('home');

    Route::get('/monev/{pribadi}', 'MonitoringController@show')->name('home');

    Route::post('/tampildata', 'MonitoringController@tampildatacari');

    Route::post('/skdata', 'SkController@skdatacari');
    
    Route::post('/skdata1', 'SkController@skdatacari1');

    Route::get('/skprintview/{id}', 'SkController@show');

    Route::get('/printsk/{id}', 'SkController@indexshow');

    Route::get('/testprintsk/{id}', 'SkController@testshow');


    Route::get('/monitoring/{id}/monev', 'MonitoringController@edit')->name('home');

    Route::get('/monitoring/{id}/nonaktif', 'MonitoringController@nonaktif')->name('home');

    Route::delete('/monev/{pribadi}', 'MonitoringController@destroy')->name('home');

    Route::post('/monitoring/monev', 'MonitoringController@store')->name('home');

    Route::get('/datasebaran','DatasebaranController@index');

    Route::get('/dataapi', 'DataapiController@index');

    Route::get('/skumkm', 'SkController@index');

    Route::get('/skumkmview', 'SkController@showskumkm');

    Route::delete('/skumkmview/{id}', 'SkController@destroy')->name('home');

    Route::post('/createsk', 'SkController@store');
    
    Route::post('/updatesk/{id}', 'SkController@update');
    
    Route::post('/viewuser', 'UsersController@index');

    Route::get('/simple-qr-code','GenerateQrCodeController@simpleQrCode');
    Route::get('/color-qr-code','GenerateQrCodeController@colorQrCode');
    Route::get('/image-qr-code','GenerateQrCodeController@imageQrCode');

    Route::post('/carikelurahan',function(){
        return 'id';
        // $kelurahan = DB::table('db_kelurahan')->where('db_kelurahan.id_kecamatan','=', 101)->get();
        // return $kelurahan;
        // return view('dataukm.create',compact('kelurahan'));
    });
    Route::post('/carikel/{id}','BinaansController@cari');

    Route::post('/umkmnik/{id}','BinaansController@carinikumkm');

    // Route::post('/carinik12/{id}','SkController@carinik');

    Route::post('/total','InfoController@total');

    Route::get('/datatest', function()
    {
        $pribadi = App\Pribadi::find('AKUKM201701002');
    
        // $pribadi = App\Pribadi::query();
        // $usaha = App\Usaha::query();
        $result = $pribadi->Usaharelasi()->get();
        // $result = $pribadi->$usaha::query();
    
        $resultkec1 = App\kecamatanmodel::find($pribadi->id_kecamatan);
        
        foreach ($result as $hasil) {
        
            $data[] =[
                'id_anggota' =>$pribadi->id_anggota,
                'nama_usaha' =>$hasil->nama_usaha,
                'nama_depan' =>$pribadi->nama_depan,
                'no_handphone' =>$pribadi->no_handphone,
                'alamat'=>$pribadi->alamat,
                // 'id_kecamatan'=>$resultkec->kecamatan,
                'kecamatan'=>$resultkec1->kecamatan,
            ];
            dd($data);
            return $data;
        }
    });

Route::get('/reportthn','LaporanController@index');
Route::get('/reportall','LaporanController@alldata');
Route::post('/caritahun','LaporanController@resulttahun');
Route::post('/cariklasifikasi','LaporanController@resultjenis');
Route::post('/carijenis','LaporanController@resultjenis1');
Route::post('/carikecamatan','LaporanController@resultkecamatan');
// Route::get('/pdf', 'LaporanController@pdf')->name('print');



});
Route::group(['middleware' => ['auth','checkRole:superadmin,visitor,surveyor,Bid_UMKM,TimInputData,advis1']], function () {
    Route::get('/dashboard', 'HomeController@index')->name('home');
    Route::get('/monitoring', 'MonitoringController@index')->name('home');
    Route::get('/monev', 'MonitoringController@indexshow')->name('home');
    Route::get('/monev/{pribadi}', 'MonitoringController@show')->name('home');
    Route::get('/monitoring/{id}/monev', 'MonitoringController@edit')->name('home');
    Route::get('/monitoring/{id}/nonaktif', 'MonitoringController@nonaktif')->name('home');
    // Route::delete('/monev/{pribadi}', 'MonitoringController@destroy')->name('home');
    Route::post('/monitoring/monev', 'MonitoringController@store')->name('home');
    Route::get('/datasebaran','DatasebaranController@index');
    Route::get('/add', 'BinaansController@create');
    Route::post('/create', 'BinaansController@store');
    Route::get('/binaans/{pribadi}', 'BinaansController@show')->name('home');
    Route::post('/skdata', 'SkController@skdatacari');
    Route::get('/skprintview/{id}', 'SkController@show');
    Route::get('/printsk/{id}', 'SkController@indexshow');
    Route::get('/skumkm', 'SkController@index');
    Route::get('/skumkmview', 'SkController@showskumkm');
    Route::post('/createsk', 'SkController@store');
    Route::get('/users/{id}', 'UserController@show');
    Route::post('/carikel/{id}','BinaansController@cari');
    Route::get('/binaans', 'BinaansController@index')->name('home');
    Route::get('/binaans/Mikro', 'BinaansController@mikro')->name('home');
    Route::get('/binaans/Kecil', 'BinaansController@kecil')->name('home');
    Route::get('/binaans/Menengah', 'BinaansController@menengah')->name('home');
    Route::post('/umkmnik/{id}','BinaansController@carinikumkm');
    Route::post('/tampildata', 'MonitoringController@tampildatacari');
    Route::post('/updatesk/{id}', 'SkController@update');
    Route::post('/skdata1', 'SkController@skdatacari1');
    Route::post('/caridatabansos1','BansosContoller@indexcaridata');
    // Route::get('/inputbansos12','BansosContoller@inputdata');
    Route::get('/inputbansosadddata','BansosContoller@inputdata12');
    Route::get('/inputbansos','BansosContoller@inputdata12');
    Route::post('/carikelbansos/{id}','BansosContoller@cari');
    Route::post('/createbansos','BansosContoller@store');
    Route::post('/updatebansos','BansosContoller@update');
    Route::post('/carinik/{id}','BansosContoller@carinik');
    Route::post('/carinik12/{id}','BansosContoller@carinik12');
    Route::post('/ceknik','BansosContoller@ceknik');
    Route::get('/bansos','BansosContoller@index');
    Route::get('/laporankelurahan/{id}','BansosContoller@laporankelurahan');
    Route::get('/kecamatan','BansosContoller@indexkecamatan');
    Route::get('/kecamatan/{id}','BansosContoller@indexkelurahan');
    Route::get('/caridatabansos','BansosContoller@indexcari');
    Route::get('/masterbansos','BansosContoller@showindex');
    Route::get('/masterbansostahap2','BansosContoller@showindex1');

    Route::get('/kecamatantahap2','BansosContoller@indexkecamatantahap2');
    Route::get('/kecamatantahap2/{id}','BansosContoller@indexkelurahantahap2');
    Route::get('/caridatabansostahap2','BansosContoller@indexcaritahap2');
    Route::get('/updatedata2','BpumcekController@index');
    Route::get('/resenddata','BpumcekController@show');
    Route::get('/validasibpum2','BpumcekController@verifikasi');
    Route::post('/verifikasit2','BpumcekController@verifikasistore');
    Route::post('/cariniktahap2/{id}','BpumcekController@carinik12');
    Route::post('/rubahdatatahap2','BpumcekController@store');
    // Route::get('/masterbansostahap2','BansosContoller@showindextahap2');
    Route::get('/laporankelurahantahap2/{id}','BansosContoller@laporankelurahantahap2');
    

    Route::get('/reportthn','LaporanController@index');
    Route::post('/caritahun','LaporanController@resulttahun');
    Route::post('/cariklasifikasi','LaporanController@resultjenis');
    Route::post('/reportusaha','LaporanController@resultjenis1');
    Route::post('/carikecamatan','LaporanController@resultkecamatan');
    // Route::get('/masterbansos1','BansosContoller@showindex1');

    Route::get('/kegiatan','KegiatanController@index');
    Route::get('/kegiatan/{kegiatan}','KegiatanController@show');
    Route::post('/addpeserta','KegiatanController@store');
    Route::get('/addkegiatan','KegiatanController@create');
    Route::post('/savekegiatan','KegiatanController@storekegiatan');
    
    
    Route::get('/rekon','RekonController@index');
    Route::get('/rekon/{id}','RekonController@indexkelurahan');
});

Route::get('/view/0/sk/umkm/out/{id}', 'OutController@show');
Route::get('/geust', 'GeustController@index');
Route::post('/cekbinaan', 'GeustController@cek');
Route::get('/geust/add_permohonan', 'GeustController@create');
Route::post('/carikel/{id}','GeustController@cari');
Route::post('/carinik1/{id}','GeustController@carinik');


Route::get('/ms', function()
    {
           
        $resultkec1 = App\Bansos::get();
        
        foreach ($resultkec1 as $hasil) {
        
            $data[] =[
               $hasil->NIK,
                $hasil->NAMA_LENGKAP,
                $hasil->ALAMAT_LENGKAP,
               $hasil->JENIS_USAHA,
                $hasil->NO_TELP,
                // 'id_kecamatan'=>$resultkec->kecamatan,
                
            ];
            
            
        }
        return $data;
        // dd($data);
    });





