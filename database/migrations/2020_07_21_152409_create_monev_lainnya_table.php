<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMonevLainnyaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('monev_lainnya', function (Blueprint $table) {
           
            $table->string('id_anggota',25)->unique();
            $table->string('bahan_baku')->nullable();
            $table->string('pemasaran')->nullable();
            $table->string('detailpemasaran')->nullable();
            $table->longText('pelatihan')->nullable();
            $table->string('perijinan')->nullable();
            $table->string('nib');
            $table->string('npwp');
            $table->string('haki')->nullable();
            $table->string('halal')->nullable();
            $table->string('pirt')->nullable();
            $table->string('bpom')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('monev_lainnya');
    }
}
