<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMonevPerusahaanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('monev_perusahaan', function (Blueprint $table) {
          
            $table->string('id_anggota',25)->unique();
            $table->string('nama_usaha');
            $table->longText('alamat_usaha');
            $table->char('rt_usaha')->nullable();
            $table->char('rw_usaha')->nullable();
            $table->string('no_telepon_usaha')->nullable();
            $table->string('thn_daftar');
            $table->string('thn_berdiri');
            $table->string('bentuk_usaha');
            $table->string('jenis_usaha');
            $table->string('produk_1');
            $table->string('produk_2')->nullable();
            $table->string('produk_3')->nullable();
            $table->string('bahan_utama')->nullable();
            $table->string('perolehan')->nullable();
            $table->string('detail_perolehan')->nullable();
            $table->char('jumlah_karyawan');
            $table->string('manajemen_keuangan')->nullable();
            $table->string('detail_manajemen')->nullable();
            $table->bigInteger('modal_sendiri')->nullable();
            $table->bigInteger('modal_luar')->nullable();
            $table->string('penjamin')->nullable();
            $table->string('detail_penjamin')->nullable();
            $table->bigInteger('nominal')->nullable();
            $table->string('jenis_omset');
            $table->bigInteger('jumlah_omset');
            $table->string('jenis_aset');
            $table->bigInteger('jumlah_aset');
            $table->string('media_penjualan')->nullable();
            $table->string('detail_media')->nullable();
            $table->string('kerjasama')->nullable();
            $table->string('detailkerjasama')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('monev_perusahaan');
    }
}
