<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMonevPribadiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('monev_pribadi', function (Blueprint $table) {
           
            $table->string('id_anggota',25)->unique();
            $table->string('nik',16)->unique();
            $table->string('nama_depan');
            $table->string('jenis_kelamin');
            $table->longText('alamat');
            $table->char('rt')->nullable();
            $table->char('rw')->nullable();
            $table->string('no_telepon')->nullable();
            $table->string('no_handphone');
            $table->string('email');
            $table->char('id_kecamatan');
            $table->char('id_kelurahan');
            $table->string('kota');
            $table->char('kode_pos')->nullable();
            $table->string('pendidikan')->nullable();
            $table->string('aktifasi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('monev_pribadi');
    }
}
