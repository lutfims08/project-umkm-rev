<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDbSkUmkmTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('db_sk_umkm', function (Blueprint $table) {
            $table->string('id_anggota',25)->unique();
            $table->string('nik',16)->unique();
            $table->string('nama_depan');
            $table->string('nama_usaha');
            $table->string('tandatangan');
            $table->string('nosurat');
            $table->string('tgl_permohonan');
            $table->string('tgl_pembuatan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('db_sk_umkm');
    }
}
