@extends('layouts.app')

@section('title', 'Users')

@section('content')
@if (session('statustambah'))
    <div class="alert alert-success">
        {{ session('statustambah') }}
    </div>
@endif
<section class="content">
      <div class="container-fluid">
        <div class="card card-default">
            <div class="card-header">
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
              </div>
            </div>
            <div class="card-body">
              <table id="viewukm1" class="table table-bordered table-striped">
                <thead class="thead-drak">
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">NIK</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Email</th>
                    <th scope="col">Level Akses</th>
                    <th scope="col">Verified</th>
                    <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($model as $m)
                    <tr>
                    <th scope="row">{{$loop->iteration}}</th>
                    <td>{{$m->nik}}</td>
                    <td>{{$m->name}}</td>
                    <td>{{$m->email}}</td>
                    <td>{{$m->role}}</td>
                    <td>{{$m->email_verified_at}}</td>
                    <td>
                    <a href="/users/{{$m->nik}}" class="badge badge-info">detail</a>
                    </td>
                    </tr>
                @endforeach    
                </tbody>
                </table>
            </div>
        </div>
      </div>
</section>
        
@endsection

@push('scripts')

<script>
  $(function () {
  
    $('#viewukm1').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true
    });
  });
  
  </script>
@endpush