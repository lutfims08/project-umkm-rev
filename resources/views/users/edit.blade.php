@extends('layouts.app')

@section('title', 'UPDATE DATA UMKM')

@section('content')

<section class="content">
	<div class="form-horizontal">
		<div class="box box-default">
			<div class="card card-primary">
				<div class="card-header">
					<h3 class="card-title"></h3>

					<div class="card-tools">
						<button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
						<i class="fas fa-minus"></i></button>
					</div>
				</div>
				@foreach($model as $result)
				<div class="card-body">
					<div class="row">
						<div class="col-md-12">
							
								<div class="card">
									<div class="card-header p-2">
										<ul class="nav nav-pills">
										<li class="nav-item"><a class="nav-link active" href="#pribadi" data-toggle="tab">Data Pribadi</a></li>
										
										</ul>
									</div><!-- /.card-header -->
									<form class="form-horizontal" method="POST" action="/users/{{$result->id}}">
									@method('patch') 
									@csrf
									<div class="card-body">
										<div class="tab-content">
											<div class="form-group row ">
                                                <label for="inputEmail3" class="col-sm-2 col-form-label" align="right">Name</label>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control @error('name') is-invalid @enderror"  
                                                    id="nama_dpn" name="name"  placeholder="Nama Depan" value="{{$result->name}}">
                                                    <div class="invalid-tooltip">
                                                        Silihkan Isikan Nama Pemilik
                                                    </div>
                                                </div>	
                                            </div>
                                        </div>
                                        <div class="tab-content">
											<div class="form-group row ">
                                                <label for="inputEmail3" class="col-sm-2 col-form-label" align="right">NIK</label>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control @error('nik') is-invalid @enderror"  
                                                    id="nama_dpn" name="nik"  placeholder="Nama Depan" value="{{$result->nik}}">
                                                    <div class="invalid-tooltip">
                                                        Silihkan Isikan Nama Pemilik
                                                    </div>
                                                </div>	
                                            </div>
                                        </div>
                                        <div class="tab-content">
											<div class="form-group row ">
                                                <label for="inputEmail3" class="col-sm-2 col-form-label" align="right">ROLE</label>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control @error('role') is-invalid @enderror"  
                                                    id="nama_dpn" name="role"  placeholder="Nama Depan" value="{{$result->role}}">
                                                    <div class="invalid-tooltip">
                                                        Silihkan Isikan Nama Pemilik
                                                    </div>
                                                </div>	
                                            </div>
                                        </div>
                                        <div class="card-footer" >
                                            <a href="/users" ><button type="button" name="back2" id="back2" class="btn btn-default"><i class="far fa-hand-point-left">  Kembali</i></button></a>
                                            <button type="submit" class="btn btn-info float-center"><i class="fa fa-save">  Simpan</i></button>
                                        </div>
									</div>
									<from>
								</div>
							
						</div>
					</div>

				</div>
				@endforeach
			</div>
		</div>
	</div>
      
</section>

@endsection

@push('scripts')
<script>
$(document).ready(function()
    {    
      $("#nik").keyup(function()
      {   
        var name = $(this).val(); 
        
        if(name.length > 0)
        {   
          $("#result").html('checking...');       
          $.ajax({
            headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            type : 'POST',
            url  : '/carinik/' + name,
            
            success : function(data)
                  {
                       $("#result").html(data);
                    }
            });
            return false;
          
        }
        else
        {
          $("#result").html('');
        }
      });
      
    });
</script>
<script type="text/javascript">
        $("#kecamatan").on('change',function() {
            if ($("#kecamatan").val() != ""){
              var kecamatan = $("#kecamatan").val();
             
              $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: "/carikel/" +kecamatan,
                
                data: {
                    id:kecamatan
                    },    
               
                success: function(html) {
                  $("#cari_kelurahan").html(html);
                }
              });
            }
          });

</script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservationdate').datetimepicker({
        format: 'YYYY'
	});

	$('#reservationdate1').datetimepicker({
        format: 'YYYY'
	});
	
	$('#datepicker').datepicker({
      minViewMode: 2,
       format: 'yyyy',
       autoclose: true
    })
    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY hh:mm A'
      }
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Timepicker
    $('#timepicker').datetimepicker({
      format: 'LT'
    })
    
    //Bootstrap Duallistbox
    $('.duallistbox').bootstrapDualListbox()

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    $('.my-colorpicker2').on('colorpickerChange', function(event) {
      $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    });

    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });

  })
</script>
<script>
$(function () {
    $('#pribadi a').click(function (e) {
        e.preventDefault();
        $('a[href="' + $(this).attr('href') + '"]').tab('show');
    })

     $('#usaha a').click(function (e) {
        e.preventDefault();
        $('a[href="' + $(this).attr('href') + '"]').tab('show');
    })

      $('#lainya a').click(function (e) {
        e.preventDefault();
        $('a[href="' + $(this).attr('href') + '"]').tab('show');
    })
});
 </script>



@endpush