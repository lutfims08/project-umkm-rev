<link rel="stylesheet" href="../../dist/css/cetak.css" type="text/css" />
<style type="text/css">
.media all {
    .page-break {display:none;}
}
.media print {
    .page-break {display:block; page-break-before:always; }
}
.head1 {
	font-family: "Bookman Old Style";
	font-size: 24px;
	text-align: center;
	font-weight: bold;
}
.head2 {
	font-family: "Bookman Old Style";
	font-size: 18px;
	text-align: center;
	font-weight: bold;
}
.logo {
	text-align: center;
}
.head3 {
	font-family: "Bookman Old Style";
	font-size: 16px;
	text-align: center;
}
.title {
	font-family: "Bookman Old Style";
	font-size: 14px;
	font-weight: bold;
	text-align: center;
}
#judul {
	font-family: "Bookman Old Style";
}
#viewukm1 {
	font-family: "Bookman Old Style";
	font-size: 12px;
}
body {
	margin-left: 3px;
	margin-top: 1px;
	margin-right: 3px;
	margin-bottom: 1px;
}
</style>
<body onload="window.print()">
  <table width="100%" border="0" cellspacing="1" cellpadding="1">
    <tr>
      <td width="16%" rowspan="3" class="logo"><img src="../../dist/img/logo_sk.png" width="70" height="62" /></td>
      <td width="78%" class="head1">PEMERINTAH KOTA BANDUNG</td>
      <td width="6%">&nbsp;</td>
    </tr>
    <tr>
      <td class="head2">DINAS KOPERASI USAHA MIKRO KECIL DAN MENENGAH</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td class="head3">Jl. Kawaluyaan No. 2  Telp &amp; Fax (022) 7308358 Bandung</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td colspan="3"><hr /></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td colspan="3" class="title">LAPORAN DATA UMKM TERDAFTAR</td>
    </tr>
    <tr>
      <td colspan="3" class="title">{{$title}}</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </table>
  
  <table width="100%" border="1" align="center" cellspacing="1" class="table table-bordered table-striped" id="viewukm1">
      <thead class="thead-drak">
          <tr>
          <th scope="col"><span id="viewukm1">No</span></th>
          <th scope="col"><span id="viewukm1">NIK</span></th>
          <th scope="col"><span id="viewukm1">Nama Pemilik</span></th>
          <th scope="col"><span id="viewukm1">Alamat</span></th>
          <th scope="col"><span id="viewukm1">Kecamatan</span></th>
          <th scope="col"><span id="viewukm1">Nama Usaha</span></th>
          <th scope="col"><span id="viewukm1">Jenis Usaha</span></th>
          <th scope="col"><span id="viewukm1">Jenis Produk</span></th>
          <th scope="col"><span id="viewukm1">Klasifikasi Usaha</span></th>
          <th scope="col"><span id="viewukm1">Osmet</span></th>
          <th scope="col"><span id="viewukm1">Aset</span></th>
          {{--  <th scope="col"><span id="viewukm1">Aksi</span></th>  --}}
          </tr>
      </thead>
      <tbody>
          @foreach ($thn as $result)
              
          
          <tr>
            <th scope="row">
                {{$loop->iteration}}
            </th>
            <td>{{$result->nik}}</td>
            <td>{{$result->nama_depan}}</td>
            <td>{{$result->alamat}}</td>
            <td>{{$result->kecamatan}}</td>
            <td>{{$result->nama_usaha}}</td>
            <td>{{$result->jenis_usaha}}</td>
            <td>{{$result->produk_1}},{{$result->produk_3}},{{$result->produk_3}}</td>
            <td>{{$result->jenis_omset}}</td>
            <td>{{number_format(($result->jumlah_omset),0,',','.')}}</td>
            <td>{{number_format(($result->jumlah_aset),0,',','.')}}</td>
            
            </tr>
          @endforeach
      </tbody>
  </table>
</body>
