@extends('layouts.app')

@section('title', 'CARI DATA MONITORING UMKM ')

@section('content')

@if (session('statustambah'))
    <div class="alert alert-success">
        {{ session('statustambah') }}
    </div>
@endif

<section class="content">
   <div class="container-fluid">
        <div class="form-horizontal">
           <div class="box box-default">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title"></h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fas fa-minus"></i></button>
                            </button>
                        </div>
                    </div>
                    
                        <div class="row">
                            <div class="col-md-6">                           
                                <div class="card-body">
                                  <div class="form-group">
                                    <label for="thn">DATA AKUMULASI PERTAHUN</label>
                                    <a href="/reportall" target="_blank" rel="noopener noreferrer">
                                      <button type="submit" class="btn btn-primary d-inline">CETAK</button>
                                    </a>
                                  </div>
                                    <form action="/caritahun" method="POST">
                                        <div class="form-group">
                                          <label for="thn">Pilih Tahun</label>
                                          <select class="form-control select2  @error('thn')== null is-invalid @enderror" id="thn" name="thn" style="width: 100%;" >
                                            <option value="">-- Pilih Tahun --</option>
                                            @foreach ($sqlthn as $thn)
                                                <option value="<?php echo $thn->thndaftar ?>" ><?php echo $thn->thndaftar ?></option>
                                            @endforeach
                                          </select>
                                        </div>
                                        <button type="submit" class="btn btn-primary d-inline">Submit</button>
                                    </form>
                                    
                                    <form action="/carikecamatan" method="POST">
                                        <div class="form-group">
                                          <label for="thn">Pilih Kecamatan</label>
                                          <select class="form-control select2  @error('kecamatan')== null is-invalid @enderror" id="kecamatan" name="kecamatan" style="width: 100%;" >
                                            <option value="">-- Pilih kecamatan --</option>
                                            @foreach ($kecamatan as $kec)
                                                <option value="<?php echo $kec->id_kecamatan ?>" ><?php echo $kec->kecamatan ?></option>
                                            @endforeach
                                          </select>
                                        </div>
                                        <button type="submit" class="btn btn-primary d-inline">Submit</button>
                                    </form>
                                </div>
                            </div>
                            <div class="col-md-6">                           
                                <div class="card-body">
                                    <form action="/cariklasifikasi" method="POST">
                                        <div class="form-group">
                                          <label for="thn">Pilih Klasifikasi Usaha</label>
                                          <select class="form-control select2  @error('jenis')== null is-invalid @enderror" id="jenis" name="jenis" style="width: 100%;" >
                                            <option value="">-- Pilih Jenis Usaha --</option>
                                            <option value="Mikro" >Mikro</option>
                                            <option value="Kecil" >Kecil</option>
                                            <option value="Menengah" >Menengah</option>
                                            
                                          </select>
                                        </div>
                                        <button type="submit" class="btn btn-primary d-inline">Submit</button>
                                    </form>
                                    <form action="/carijenis" method="POST">
                                        <div class="form-group">
                                          <label for="thn">Pilih Jenis Usaha</label>
                                          <select class="form-control select2  @error('jenis1')== null is-invalid @enderror" id="jenis1" name="jenis1" style="width: 100%;" >
                                            <option value="">-- Pilih Jenis Usaha --</option>
                                            @foreach ($jenisusaha as $kec)
                                            <option value="<?php echo $kec->jenis ?>" ><?php echo $kec->jenis ?></option>
                                            @endforeach
                                            
                                            
                                          </select>
                                        </div>
                                        <button type="submit" class="btn btn-primary d-inline">Submit</button>
                                    </form>
                                </div>
                            </div> 
                        </div>
                </div>
           </div>
        </div>
   </div>
   
</section>



@endsection

@push('scripts')
<script type="text/javascript">
	

      $(document).on('click','.detail',function(e){
        var kecamatan = $("#kecamatan").val();
        var depan = $("#nama_dpn").val();
        var usaha = $("#nama_usaha").val();
        e.preventDefault();
        $("#modal-default").modal('show');
        $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
          });
       
        $.post("/tampildata",
            {
            nama_dpn: depan,
            nama_usaha: usaha,
            kecamatan:kecamatan
            },
            function(data,html){
              $(".modal-body").html(data);
            });
        
    });
</script>

@endpush