<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>OUTPUT SURAT KETERANGAN</title>
<style type="text/css">
.head {
	font-family: "Bookman Old Style";
	font-size: 16px;
	font-weight: bold;
	text-align: center;
}
.head2 {
	font-family: "Bookman Old Style";
}
.head2 {
	text-align: center;
}
.body1 {
	text-align: center;
	font-family: "Bookman Old Style";
	font-size: 14px;
}
.body3 {
	text-align: center;
}
.body3 {
	font-family: "Bookman Old Style";
	font-size: 14px;
}
.body5 {
	text-align: center;
	font-family: "Bookman Old Style";
	font-size: 14px;
	font-weight: bold;
}
.body3 td table {
	text-align: left;
}
.tab12 {
	text-align: left;
}
</style>
</head>
<body>
<p class="head">SISTEM DATABASE UMKM </p>
<p class="head2">DINAS KOPERASI USAHA MIKRO KECIL DAN MENENGAH</p>
<p class="head2">KOTA BANDUNG</p>
<hr />
<p class="body1">Sistem Informasi Database UMKM Dinas Koperasi Usaha Mikro Kecil dan Menengah Kota Bandung, Menyatakan bahwa :</p>
<table width="69%" border="0" align="center" cellpadding="1" cellspacing="1">
@foreach ($v1 as $result)
<tr class="tab12">
  <td width="35%"><table width="100%" border="0" cellspacing="1" cellpadding="1">
    <tr>
      <td>Nomor Surat</td>
      <td>:</td>
      <td>{{$result->nosurat}}</td>
    </tr>
    <tr>
      <td width="31%">Nama</td>
      <td width="6%">:</td>
      <td width="63%">{{$result->nama_depan}}</td>
    </tr>
    <tr>
      <td>Nama Usaha</td>
      <td>:</td>
      <td>{{$result->nama_usaha}}</td>
    </tr>
    <tr>
      <td>Jenis Usaha</td>
      <td>:</td>
      <td>{{$result->jenis_usaha}}</td>
    </tr>
    <tr>
      <td>Tanggal Permohonan</td>
      <td>:</td>
      <td>{{$result->tgl_permohonan}}</td>
    </tr>
    <tr>
      <td>Tanggal Pembuatan</td>
      <td>:</td>
      <td>{{$result->tgl_pembuatan}}</td>
    </tr>
  </table></td>
</tr>  
@endforeach
</table>
<p class="body5">Adalah benar dan tercatat dalam database kami. di https://umkmbandung.online/</p>
</body>
</html>