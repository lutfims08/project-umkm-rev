<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>cek data</title>
    <meta name="csrf-token" content="{{ Session::token() }}">
</head>
<body>
    <form  action="/rescek" method="POST">
        @csrf
            <div class="">
                <div class="col-md-12">
                    <div class="form-group">
                        <input type="text" class="form-control @error('nik') is-invalid @enderror"  id="nik" name="nik" placeholder="Input NIK " aria-label="Inputkan NIK " aria-describedby="basic-addon2" value="{{ old('nik') }}">
                        @error('nik')
                            <span class="invalid-feedback" role="alert">

                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>                                         
                    <div class="form-group">
                        <input type="submit" class="btnRegister" value="CARI DATA">
                    </div>
                </div>
            </div>
        </form>
</body>
</html>