<html lang="en"><head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex, nofollow">

    <title>DAFTAR BPUM</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <style type="text/css">
    .register{
    background: -webkit-linear-gradient(left, #3931af, #00c6ff);
    margin-top: 2%;
    padding: 2%;
}
.register-left{
    text-align: center;
    color: #fff;
    margin-top: 2%;
}
.register-left input{
    border: none;
    border-radius: 1.5rem;
    padding: 2%;
    width: 60%;
    background: #f8f9fa;
    font-weight: bold;
    color: #383d41;
    margin-top: 30%;
    margin-bottom: 3%;
    cursor: pointer;
}
.register-right{
    background: #f8f9fa;
    border-top-left-radius: 10% 50%;
    border-bottom-left-radius: 10% 50%;
}
.register-left img{
    margin-top: 10%;
    margin-bottom: 0%;
    width: 50%;
    -webkit-animation: mover 2s infinite  alternate;
    animation: mover 1s infinite  alternate;
}
@-webkit-keyframes mover {
    0% { transform: translateY(0); }
    100% { transform: translateY(-20px); }
}
@keyframes mover {
    0% { transform: translateY(0); }
    100% { transform: translateY(-20px); }
}
.register-left p{
    font-weight: lighter;
    padding: 12%;
    margin-top: -9%;
}
.register .register-form{
    padding: 6%;
    margin-top: 10%;
}
.btnRegister{
    float: right;
    margin-top: 10%;
    border: none;
    border-radius: 1.5rem;
    padding: 2%;
    background: #0062cc;
    color: #fff;
    font-weight: 600;
    width: 50%;
    cursor: pointer;
}
.register .nav-tabs{
    margin-top: 3%;
    border: none;
    background: #0062cc;
    border-radius: 1.5rem;
    width: 28%;
    float: right;
}
.register .nav-tabs .nav-link{
    padding: 2%;
    height: 34px;
    font-weight: 600;
    color: #fff;
    border-top-right-radius: 1.5rem;
    border-bottom-right-radius: 1.5rem;
}
.register .nav-tabs .nav-link:hover{
    border: none;
}
.register .nav-tabs .nav-link.active{
    width: 100px;
    color: #0062cc;
    border: 2px solid #0062cc;
    border-top-left-radius: 1.5rem;
    border-bottom-left-radius: 1.5rem;
}
.register-heading{
    text-align: center;
    margin-top: 20%;
    margin-bottom: -15%;
    color: #495057;
}    </style>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        window.alert = function(){};
        var defaultCSS = document.getElementById('bootstrap-css');
        function changeCSS(css){
            if(css) $('head > link').filter(':first').replaceWith('<link rel="stylesheet" href="'+ css +'" type="text/css" />'); 
            else $('head > link').filter(':first').replaceWith(defaultCSS); 
        }
        
    </script>
</head>
<body>
    <div class="container register">
                <div class="row">
                    <div class="col-md-3 register-left">
                        <img src="../../dist/img/LOGO-DISKOP2.png" alt="">
                        <h3>PENDAFTARAN BPUM <br> TAHAP 2</h3>
                        <p>Bagi Para Pelaku Usaha Mikro dan Ultra Mikro <br>
                            
                            <h5>DINAS KOPERASI USAHA MIKRO KECIL DAN MENENGAH</h5><br>KOTA BANDUNG</p>
                        
                    </div>
                    <div class="col-md-9 register-right">
                        <ul class="nav nav-tabs nav-justified" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">DAFTAR</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">CEK DATA</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                <h3 class="register-heading">USULAN BPUM</h3>
                                @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                                @endif
                                <form class="" action="/addbpum" method="post">
                                    <div class="row register-form">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                
                                                <input type="text" class="form-control @error('nik') is-invalid @enderror"  id="nik" name="nik" placeholder="Isikan Nomor KTP " aria-label="Isikan Nomor KTP " aria-describedby="basic-addon2" >
                                                @error('nik')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control @error('nama_dpn') is-invalid @enderror"  id="nama_dpn" name="nama_dpn"  placeholder="Isikan Nama Lengkap Sesuai KTP"   >
                                                @error('nama_dpn')
                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <input type="hidden" class="form-control @error('nama_usaha') is-invalid @enderror"  id="nama_usaha" name="nama_usaha"  placeholder="Isikan Nama Usaha"   >
                                                @error('nama_usaha')
                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <textarea name="alamat" id="alamat" class="form-control" rows="2" cols="45" placeholder=" Isikan Alamat Lengkap Sesuai KTP"  ></textarea>
                                                @error('alamat')
                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <select class="form-control select2 @error('kelurahan') is-invalid @enderror" id="cari_kelurahan" name="kelurahan" style="width: 100%;" required >
                                                    <option selected disabled value="" >-- Pilih Kelurahan --</option>
                                                    <?php
                                                    $kec = DB::table('db_kelurahan_1')
                                                    
                                                    ->orderBy('kelurahan')
                                                    ->get();
                                                    foreach($kec as $k1){
                                                        ?>
                                                        <option 
                                                         {{--  <?php 
                                                            if (old('kelurahan') == $k1->kelurahan){
                                                            echo "selected";	
                                                            }
                                                            else{
                                                            }
                                                            ?>   --}}
                                                        value = "<?php echo $k1->kelurahan?>"><?php echo $k1->kelurahan; ?>
                                                        </option>
                                                       <?php } ?>
                                                    
                                                    </select>
                                                    @error('kelurahan')
                                                        <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control @error('kecamatan') is-invalid @enderror"  id="kecamatan" name="kecamatan" placeholder="Input Kecamatan " aria-label="Inputkan Kecamatan " aria-describedby="basic-addon2"  readonly>
                                                @error('kecamatan')
                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                                                                    
                                        </div>
                                        <div class="col-md-6">
                                            
                                            <div class="form-group">
                                                <select class="form-control"  id="bidang_usaha" name="bidang_usaha" required >
                                                    <option selected disabled value="" >Pilih Bidang Usaha</option>
                                                    <option value="Kuliner" >KULINER</option>
                                                    <option value="Fashion" >FASHION</option>
                                                    <option value="Craft" >CRAFT</option>
                                                    <option value="Jasa" >JASA</option>
                                                    <option value="Perdagangan" >PERDAGANGAN</option>
                                                    <option value="Argobisnis">ARGOBISNIS</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <input type="text" minlength="10" maxlength="13" name="txtEmpPhone" class="form-control" placeholder="Isikan No. Handphone"   id="hp" >
                                                @error('txtEmpPhone')
                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control @error('no_reg') is-invalid @enderror"  id="no_reg" name="no_reg"  placeholder="No. Register Dari Kelurahan Setempat"  >
                                                Contoh Nomer Register : nomorsurat/perihalsurat/namakelurahan/bulan/tahun <br> 
                                                disesuaikan dengan yang ada dikelurahan.
                                                @error('no_reg')
                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <input type="submit" class="btnRegister" value="Register">
                                        </div>
                                        <div class="col-md-12">
                                            <div id="countdown">
                                                <br>
                                                Waktu Pendaftaran Hingga : <br>
                                                <span id="days"></span> Hari ||
                                                <span id="hours"></span> Jam || 
                                                <span id="minutes"></span> Menit ||
                                                <span id="seconds"></span> Detik 
                                               
                                              </div> <br>
                                        </div>
                                        
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane fade show" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                <h3 class="register-heading">CEK PENDAFTARAN BPUM TAHAP 2</h3>
                                
                                    <form class="" action="/cekdata2" method="post">
                                        <div class="row register-form">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control @error('nik') is-invalid @enderror"  id="nik" name="nik" placeholder="Input NIK " aria-label="Inputkan NIK " aria-describedby="basic-addon2" value="{{ old('nik') }}">
                                                    @error('nik')
                                                        <span class="invalid-feedback" role="alert">

                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>                                         
                                                <div class="form-group">
                                                    <input type="submit" class="btnRegister" value="CARI DATA">
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    
                            </div>
                        </div>
                    </div>
                </div>
                <div class='chating' style=' z-index: 99999; width: 150px; padding: 15px; left: 0; bottom: 0; position: fixed; '>
                    <a href="https://abqdev.site/"><img alt='wa' src='../../dist/img/A1.png' style='width: 100px;' /></a> </div>

            </div>	<script type="text/javascript">
        </script>
        <script type="text/javascript">
		
            $("#cari_kelurahan").change(function() {
                if ($("#cari_kelurahan").val() != ""){
                  var kecamatan = $("#cari_kelurahan").val();
                 
                  $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: "POST",
                    url: "/carikelbpum/" +kecamatan,
                    
                    data: {
                        id:kecamatan
                        },    
                   
                    success: function(data) {
                        document.getElementById("kecamatan").value = data.KECAMATAN;
                    }
                  });
                }
              });
        
        </script>
        <script>
            @if(Session::has('message'))
              var type = "{{ Session::get('alert-type', 'info') }}";
              switch(type){
                  case 'info':
                      toastr.info("{{ Session::get('message') }}");
                      break;
                  
                  case 'warning':
                      toastr.warning("{{ Session::get('message') }}");
                      break;
          
                  case 'success':
                      toastr.success("{{ Session::get('message') }}");
                      break;
          
                  case 'error':
                      toastr.error("{{ Session::get('message') }}");
                      break;
              }
            @endif
        </script>
        <script>
            (function () {
                const second = 1000,
                      minute = second * 60,
                      hour = minute * 60,
                      day = hour * 24;
              
                let birthday = "Nov 25, 2020 23:59:59",
                    countDown = new Date(birthday).getTime(),
                    x = setInterval(function() {    
              
                      let now = new Date().getTime(),
                          distance = countDown - now;
              
                      document.getElementById("days").innerText = Math.floor(distance / (day)),
                        document.getElementById("hours").innerText = Math.floor((distance % (day)) / (hour)),
                        document.getElementById("minutes").innerText = Math.floor((distance % (hour)) / (minute)),
                        document.getElementById("seconds").innerText = Math.floor((distance % (minute)) / second);
              
                      //do something later when date is reached
                      if (distance < 0) {
                        let headline = document.getElementById("headline"),
                            countdown = document.getElementById("countdown"),
                            content = document.getElementById("content");
              
                        headline.innerText = "It's my birthday!";
                        countdown.style.display = "none";
                        content.style.display = "block";
              
                        clearInterval(x);
                      }
                      //seconds
                    }, 0)
                }());
        </script>

</body></html>