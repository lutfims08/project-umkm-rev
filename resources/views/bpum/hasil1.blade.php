<html lang="en"><head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex, nofollow">

    <title>DAFTAR BPUM</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <style type="text/css">
    .register{
    background: -webkit-linear-gradient(left, #3931af, #00c6ff);
    margin-top: 2%;
    padding: 3%;
}
.register-left{
    text-align: center;
    color: #fff;
    margin-top: 2%;
}
.register-left input{
    border: none;
    border-radius: 1.5rem;
    padding: 2%;
    width: 60%;
    background: #f8f9fa;
    font-weight: bold;
    color: #383d41;
    margin-top: 30%;
    margin-bottom: 3%;
    cursor: pointer;
}
.register-right{
    background: #f8f9fa;
    border-top-left-radius: 10% 50%;
    border-bottom-left-radius: 10% 50%;
}
.register-right p{
    margin-top: 17%;
    
    margin-bottom: 0%;
}
.register-left img{
    margin-top: 10%;
    margin-bottom: 5%;
    width: 50%;
    -webkit-animation: mover 2s infinite  alternate;
    animation: mover 1s infinite  alternate;
}
@-webkit-keyframes mover {
    0% { transform: translateY(0); }
    100% { transform: translateY(-20px); }
}
@keyframes mover {
    0% { transform: translateY(0); }
    100% { transform: translateY(-20px); }
}
.register-left p{
    font-weight: lighter;
    padding: 12%;
    margin-top: -9%;
}
.register .register-form{
    padding: 8%;
    margin-top: 12%;
}
.btnRegister{
    float: right;
    margin-top: 10%;
    border: none;
    border-radius: 1.5rem;
    padding: 2%;
    background: #0062cc;
    color: #fff;
    font-weight: 600;
    width: 50%;
    cursor: pointer;
}
.register .nav-tabs{
    margin-top: 3%;
    border: none;
    background: #0062cc;
    border-radius: 1.5rem;
    width: 28%;
    float: right;
}
.register .nav-tabs .nav-link{
    padding: 2%;
    height: 34px;
    font-weight: 600;
    color: #fff;
    border-top-right-radius: 1.5rem;
    border-bottom-right-radius: 1.5rem;
}
.register .nav-tabs .nav-link:hover{
    border: none;
}
.register .nav-tabs .nav-link.active{
    width: 100px;
    color: #0062cc;
    border: 2px solid #0062cc;
    border-top-left-radius: 1.5rem;
    border-bottom-left-radius: 1.5rem;
}
.register-heading{
    text-align: center;
    margin-top: 8%;
    margin-bottom: -15%;
    color: #495057;
}    </style>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        window.alert = function(){};
        var defaultCSS = document.getElementById('bootstrap-css');
        function changeCSS(css){
            if(css) $('head > link').filter(':first').replaceWith('<link rel="stylesheet" href="'+ css +'" type="text/css" />'); 
            else $('head > link').filter(':first').replaceWith(defaultCSS); 
        }
        
    </script>
</head>
<body>
    <div class="container register">
                <div class="row">
                    <div class="col-md-3 register-left">
                        <img src="../../dist/img/LOGO-DISKOP2.png" alt="">
                        <h3>PENDAFTARAN BPUM <br> TAHAP 2</h3>
                        <p>Bagi Para Pelaku Usaha Mikro dan Ultra Mikro <br> 
                            <h5>DINAS KOPERASI USAHA MIKRO KECIL DAN MENENGAH</h5><br>KOTA BANDUNG</p>
                        
                    </div>
                    <div class="col-md-9 register-right">
                        
                        <h3 class="register-heading " style="color:red";>Nomor Register telah digunakan, silahkan lakukan Updating Nomor Register di Kelurahan Setempat.</h3>
                        @foreach ($result1 as $data)
                        <p>
                        <div class="row">
                            
                            <dt class="col-sm-4">Nomor Register </dt>
                            <dd class="col-sm-8">:<?php echo $data->no_reg;  ?></dd>
                            <dt class="col-sm-4">Nama </dt>
                            <dd class="col-sm-8">:<?php echo $data->nama_lengkap;  ?></dd>
                            
                            <dt class="col-sm-4">Kelurahan</dt>
                            <dd class="col-sm-8">:<?php echo $data->kelurahan;  ?></dd>
                            <dt class="col-sm-4">Kecamatan</dt>
                            <dd class="col-sm-8">:<?php echo $data->kecamatan;  ?></dd>
                            
                            
                        </div>
                        <a href="/daftarBPUMtahap2"><input type="btn" class="btnRegister" value="Kembali"></a> 
                        </p>
                        @endforeach
                        
                        
                    </div>
                </div>
                
            </div>	<script type="text/javascript">
		</script>
        <div class='chating' style=' z-index: 99999; width: 150px; padding: 15px; left: 0; bottom: 0; position: fixed; '>
            <a href="https://abqdev.site/"><img alt='wa' src='../../dist/img/A1.png' style='width: 100px;' /></a> </div>

</body></html>