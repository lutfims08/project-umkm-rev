<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>MONITORING</title>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<style type="text/css">
.head1 {
	font-weight: bold;
	font-size: 16px;
	font-family: "Bookman Old Style";
	text-align: center;
}
.head2 {
	font-family: "Bookman Old Style";
	text-align: center;
}
.head2 {
	font-size: 14px;
	font-weight: bold;
}
.head3 {
	text-align: center;
	font-family: "Bookman Old Style";
	font-size: 12px;
}
.te1 {
	text-align: center;
	font-family: "Bookman Old Style";
	font-size: 14px;
}
.l1 {
	text-align: center;
}
.l2 {
	text-align: center;
}
.l3 {
	text-align: center;
}
.l4 {
	text-align: center;
}
</style>
</head>

{{--  <body  onload = "JavaScript:AutoRefresh(10000);">  --}}
<body  >
<table width="100%" border="0" cellspacing="1" cellpadding="1">
  <tr>
    <td width="10%" rowspan="3" class="l1"><img src="/dist/img/logo_sk.png" alt="" width="50%"></td>
    <td colspan="3"><div align="center" class="head1">PEMERINTAH KOTA BANDUNG</div></td>
    <td width="12%" rowspan="3" class="l2"><img src="/dist/img/LOGO-DISKOP2.png" alt="" width="75%"></td>
  </tr>
  <tr>
    <td colspan="3" class="head2">DINAS KOPERASI USAHA MIKRO KECIL DAN MENENGAH</td>
  </tr>
  <tr>
    <td colspan="3" class="head3">Jl. Kawaluyaan No. 2 Telp &amp; Fax ( 022 ) 7308358 Bandung</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="3">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="3" class="te1"><u>MONITORING PENDAFTARAN BPUM TAHAP 2</u></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td width="19%">&nbsp;</td>
    <td width="3%">&nbsp;</td>
    <td width="56%">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="l3">&nbsp;</td>
    <td>JUMLAH PENDAFTAR</td>
    <td>:</td>
    <td>
        @foreach ($totaldaftar as $item)
           <strong>{{$item->jumlahnik}}</strong> Pendaftar 
        @endforeach
    </td>
    <td></td>
  </tr>
  <tr>
    <td class="l3">&nbsp;</td>
    <td>JUMLAH PENDAFTAR TERVERIFIKASI</td>
    <td>:</td>
    <td>
        @foreach ($totalvalid as $data)
           <strong style="color:green">{{$data->jumlahstatus}}</strong> Pendaftar || data berhasil di verifikasi
        @endforeach
    </td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="l3">&nbsp;</td>
    <td>JUMLAH PENDAFTAR TIDAK MENDAFTAR ONLINE</td>
    <td>:</td>
    <td>
        @foreach ($totalvalid1 as $data)
           <strong style="color:yellowgreen">{{$data->jumlahcekdata}}</strong> Pendaftar  || rekomendasi : di verifikasi ulang oleh kelurahan.
        @endforeach
    </td>
    <td></td>
  </tr>
  <tr>
    <td class="l3">&nbsp;</td>
    <td>JUMLAH PENDAFTAR MENUNGGU VALIDASI</td>
    <td>:</td>
    <td>
        @foreach ($totalvalid12 as $data)
           <strong style="color:rgb(0, 255, 242)" >{{$data->jumlahcekdata}}</strong> Pendaftar  || keterangan : menunggu data dari kelurahan / dalam proses crosscek data by sistem.
        @endforeach
    </td>
    <td></td>
  </tr>
  <tr>
    <td class="l3">&nbsp;</td>
    <td>JUMLAH PENDAFTAR GAGAL VERIFIKASI</td>
    <td>:</td>
    <td>
        @foreach ($totalvalid123 as $data)
           <strong style="color:red">{{$data->jumlahcekdata}}</strong> Pendaftar  || Keterangan : Tidak diketahui oleh kelurahan.
        @endforeach
    </td>
    <td></td>
  </tr>
  
  <tr>
    <td height="259">&nbsp;</td>
    <td colspan="3"> 
          </td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td> </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="4"><strong>Copyright © 2021 <a href="https://umkmbandung.online/users#">TIM IT BID. UMKM</a>.</strong> All rights reserved by ABQ DEV.</td>
    <td class="l4"><a href="https://abqdev.site/"><img src="/dist/img/A1.png" alt="" width="70%"></a></td>
  </tr>
</table>

<script type = "text/JavaScript">
  <!--
     function AutoRefresh( t ) {
        setTimeout("location.reload(true);", t);
     }
  //-->
</script>
</body>
</html>