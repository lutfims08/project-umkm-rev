@extends('layouts.app')

@section('title', 'REKAP DATA PELAYANAN BPUM')

@section('content')
@if (session('statustambah'))
    <div class="alert alert-success">
        {{ session('statustambah') }}
    </div>
@endif
<section class="content">
      <div class="container-fluid">
        <div class="card card-default">
            <div class="card-header">
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
              </div>
            </div>
            <div class="card-body">
              <table id="viewukm12" class="table table-bordered table-striped">
                <thead class="thead-drak">
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">NIK</th>
                    <th scope="col">Nama Lengkap</th>
                    <th scope="col">alamat</th>
                    <th scope="col">Jenis Usaha</th>
                    <th scope="col">No telp</th>
                    <th scope="col">Keterangan</th>
                    <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                @foreach($result as $m)
                    <tr>
                    <th scope="row">{{$loop->iteration}}</th>
                    <td>{{$m->nik}}</td>
                    <td>{{$m->nama_lengkap}}</td>
                    <td>{{$m->alamat}}</td>
                    <td>{{$m->bidang_usaha}}</td>
                    <td>{{$m->no_telp}}</td>
                    <td>{{$m->keterangan}}</td>
                    <td>
                    {{--  <a href="//{{$m->id_anggota}}" class="badge badge-info">detail</a>  --}}
                    </td>
                    </tr>
                @endforeach    
                </tbody>
                </table>
            </div>
        </div>
      </div>
</section>
        
@endsection

@push('scripts')

<script>
  $(function () {
  
    $('#viewukm12').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true
    });
  });
  
  </script>
@endpush