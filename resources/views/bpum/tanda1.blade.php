<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<style type="text/css">
.head1 {
	font-weight: bold;
	font-size: 16px;
	font-family: "Bookman Old Style";
	text-align: center;
}
.head2 {
	font-family: "Bookman Old Style";
	text-align: center;
}
.head2 {
	font-size: 14px;
	font-weight: bold;
}
.head3 {
	text-align: center;
	font-family: "Bookman Old Style";
	font-size: 12px;
}
.te1 {
	text-align: center;
	font-family: "Bookman Old Style";
	font-size: 14px;
}
.l1 {
	text-align: center;
}
.l2 {
	text-align: center;
}
.l3 {
	text-align: center;
}
.l4 {
	text-align: center;
}
</style>
</head>

<body>
<table width="100%" border="0" cellspacing="1" cellpadding="1">
  <tr>
    <td width="13%" rowspan="3" class="l1"><img src="/dist/img/logo_sk.png" alt="" width="50%"></td>
    <td width="74%"><div align="center" class="head1">PEMERINTAH KOTA BANDUNG</div></td>
    <td width="13%" rowspan="3" class="l2"><img src="/dist/img/LOGO-DISKOP2.png" alt="" width="75%"></td>
  </tr>
  <tr>
    <td class="head2">DINAS KOPERASI USAHA MIKRO KECIL DAN MENENGAH</td>
  </tr>
  <tr>
    <td class="head3">Jl. Kawaluyaan No. 2 Telp &amp; Fax ( 022 ) 7308358 Bandung</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="te1"><u>TANDA TERIMA PENDAFTARAN BPUM TAHAP 2</u></td>
    <td>&nbsp;</td>
  </tr>
  @foreach ($result as $item => $data)
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><strong>{{$data->no_reg}}</strong></td>
  </tr>
  
  <tr>
    <td rowspan="5" class="l3">{!! QrCode::size(200)->generate( "umkmbandung.online/tanda/terima/0/1/bpum/".$data->nik);!!}</td>
    <td>Bahwa nama yang tercantum dibawah ini, telah terdaftar dalam usulan calon Penerima BPUM pada tahap 2 :</td>
    <td>&nbsp;</td>
  </tr>
  
  <tr>
    <td>&nbsp; Nama : {{$data->nama_lengkap}}</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp; Alamat : {{$data->alamat}}</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp; Nomer HP : {{$data->no_telp}}</td>
    <td>&nbsp;</td>
  </tr>
  @endforeach
  <tr>
    <td> 
      @if ($data->status == "invalid")
     <h2 style="color:red";>
      {{$data->keterangan}}
      </h2>
      @endif
      @if($data->status == "valid")
      <h2 style="color:green";>
        Bukti yang Sah telah terdaftar Pada BPUM yang diusulkan Dinas KUMKM Kota Bandung dan Telah Terverifikasi oleh Kelurahan
      </h2>  
      @endif
      @if($data->status == "validasi")
      Semua Berkas Pendaftaran Harap di serahkan kepada pihak Kelurahan Setempat sesuai dengan KTP, dan Mencetak bukti Pendaftaran ini.
      <br> 
      apabila bukti ini lupa menyimpan bisa akses di <a href="/daftarBPUM">Cek data</a>
      <br> Bukti yang Sah telah terdaftar Pada BPUM yang diusulkan Dinas KUMKM Kota Bandung
      <h4>PERSYARATAN AKAN DI VERIFIKASI ULANG SETELAH BERKAS DIKEMBALIKAN KE KELURAHAN SETEMPAT
        <br>APABILA TIDAK DIKEMBALIKAN DAN TIDAK SESUAI DENGAN PROSEDUR MAKA PENDAFTARAN DINYATAKAN GAGAL DAN TIDAK AKAN DIUSULKAN
      </h4>
      @endif 
      @if($data->status == null)
      Semua Berkas Pendaftaran Harap di serahkan kepada pihak Kelurahan Setempat sesuai dengan KTP, dan Mencetak bukti Pendaftaran ini.
      <br> 
      apabila bukti ini lupa menyimpan bisa akses di <a href="/daftarBPUM">Cek data</a>
      <br> Bukti yang Sah telah terdaftar Pada BPUM yang diusulkan Dinas KUMKM Kota Bandung
      <h4>PERSYARATAN AKAN DI VERIFIKASI ULANG SETELAH BERKAS DIKEMBALIKAN KE KELURAHAN SETEMPAT
        <br>APABILA TIDAK DIKEMBALIKAN DAN TIDAK SESUAI DENGAN PROSEDUR MAKA PENDAFTARAN DINYATAKAN GAGAL DAN TIDAK AKAN DIUSULKAN
      </h4>
      @endif 
      
    </td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td rowspan="2" class="l4"> <a href="https://abqdev.site/"><img src="/dist/img/A1.png" alt="" width="70%"></a>  </td>
  </tr>
  <tr>
    <td colspan="2"><strong>Copyright © 2021 <a href="https://abqdev.site/">TIM IT BID. UMKM</a>.</strong> All rights reserved by ABQ DEV.</td>
  </tr>
</table>
</body>
</html>