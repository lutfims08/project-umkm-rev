@extends('layouts.app')

@section('content')
@if (session('statustambah'))
    <div class="alert alert-success">
        {{ session('statustambah') }}
    </div>
@endif
<div class="container">
    <div class="card card-info">
        <div class="card-header">
          <h3 class="card-title">VERIFIKASI BERKAS PENDAFTAR BPUM TAHAP 2</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
              </button>
          </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    
                    <form class="form-horizontal" action="/verifikasit2" method="post">
                        <div class="form-group row ">
                            <label for="inputEmail3" class="col-sm-3 col-form-label" align="right">NIK</label>
                            <div class="col-sm-8">
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control @error('nik') is-invalid @enderror"  id="nik" name="nik" placeholder="Input NIK " aria-label="Inputkan NIK " aria-describedby="basic-addon2" value="{{ old('nik') }}">
                                    
                                    
                                  </div>
                                  @error('nik')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                  <span id="result"></span>
                                {{--  <input type="text" class="form-control @error('nik') is-invalid @enderror"  id="nik" name="nik" placeholder="Inputkan NIK" ><span id="result"></span>  --}}
                            </div>	
                        </div>                     
                        <div class="form-group row ">
                            <label for="inputEmail3" class="col-sm-3 col-form-label" align="right">Nama Lengkap</label>
                            <div class="col-sm-8">
                            
                                <input type="text" class="form-control @error('nama_dpn') is-invalid @enderror"  id="nama_dpn" name="nama_dpn"  placeholder="Isikan Nama Lengkap" value="{{ old('nama_dpn') }}" readonly ><span id="nama_dpn"></span>
                                
                                @error('nama_dpn')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            	
                        </div>
                        
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label" align="right">Alamat</label>

                            <div class="col-sm-8">
                                <textarea name="alamat" id="alamat" class="@error('alamat') is-invalid @enderror" rows="4" cols="45" placeholder=" Isikan Alamat Lengkap" readonly >{{ old('alamat') }}</textarea>
                                @error('alamat')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label" align="right">Kelurahan</label>

                            <div class="col-sm-6">
                            <select class="form-control select2 @error('kelurahan') is-invalid @enderror" id="cari_kelurahan" name="kelurahan" style="width: 100%;" >
                                <option selected disabled value="" >-- Pilih Kelurahan --</option>
                                <?php
                                $kec = DB::table('db_kelurahan_1')
                                
                                ->orderBy('kelurahan')
                                ->get();
                                foreach($kec as $k1){
                                    ?>
                                    <option  disabled <?php 
                                        if (old('kelurahan') == $k1->kelurahan){
                                        echo "selected";	
                                        }
                                        else{
                                        }
                                        ?> 
                                    value = "<?php echo $k1->kelurahan?>"><?php echo $k1->kelurahan; ?>
                                    </option>
                                   <?php } ?>
                                
                                </select>
                                @error('kelurahan')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            	
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label" align="right">Kecamatan</label>

                            <div class="col-sm-6">
                                <input type="text" class="form-control @error('kecamatan') is-invalid @enderror"  id="kecamatan" name="kecamatan" placeholder="Input Kecamatan " aria-label="Inputkan Kecamatan " aria-describedby="basic-addon2" value="{{ old('kecamatan') }}" readonly>
                                @error('kecamatan')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            
                        </div>
                        
                        

                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label" align="right">No. Telp / Hp</label>

                            <div class="col-sm-8">
                                <input type="text" class="form-control @error('hp') is-invalid @enderror" data-inputmask="'mask': ['9999-9999-9999']" data-mask
                                id="hp" name="hp" placeholder="No. Handphone / Telepon" value="{{ old('hp') }}" readonly >
                                @error('hp')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                
                            </div>
                            
                        </div>

                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label" align="right"  >No. Register</label>

                            <div class="col-sm-8">
                                <input type="text" class="form-control @error('no_reg') is-invalid @enderror"
                                id="no_reg" name="no_reg" placeholder="No. Register" value="{{ old('no_reg') }}" readonly >
                                @error('no_reg')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                
                            </div>
                            
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label" align="right"> JENIS USAHA</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control @error('bidang_usaha') is-invalid @enderror"
                                id="bidang_usaha" name="bidang_usaha" placeholder="No. Handphone / Telepon" value="{{ old('bidang_usaha') }}" readonly >
                                @error('bidang_usaha')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>      
                        </div>

                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label" align="right"> Validasi</label>
                            <div class="col-sm-8">
                                <div class="col-sm-1 icheck-success d-inline">
                                    <input type="radio" name="aktivasi" id="aktivasi1" value="valid" disabled>
                                    <label for="aktivasi1">Valid
                                    </label>
                                </div>
                                <div class="col-sm-1 icheck-success d-inline">
                                    <input type="radio" name="aktivasi" id="aktivasi2" value="invalid" disabled>
                                    <label for="aktivasi2">
                                    Invalid
                                    </label>
                                </div>
                                <div class="col-sm-1 icheck-success d-inline">
                                    <input type="radio" name="aktivasi" id="aktivasi3" value="resend_data" checked>
                                    <label for="aktivasi3">
                                        resend_data
                                    </label>
                                </div>
                            </div>      
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label" align="right">Keterangan Verifikasi</label>

                            <div class="col-sm-8">
                                <textarea name="keterangan" id="keterangan" class="@error('keterangan') is-invalid @enderror" rows="4" cols="45" placeholder=" Isikan keterangan Lengkap KARENA ANDA KENA FRANK BRI>> SELAMAT"  >{{ old('keterangan') }}</textarea>
                                @error('keterangan')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            
                        </div>

                        <div class="card-footer" >
                            <button type="submit" class="btn btn-info float-right swalDefaultSuccess"><i class="fa fa-save">  VERIFIKASI DATA</i></button>
                        </div>
                    
                    </form>       
                </div>
                <div class="col-md-6">
                    <div class="callout callout-warning">
                        {{--  <h5>Tata Cara Perubahan Data :</h5>      
                        
                        <ol>
                            <li>Inputkan NIK terlebih dahulu Kemudian TAB</li>
                            <li>Tunggu Hingga Data Muncul Sesuai dengan data Pendaftaran</li>
                            <li>Lakukan Perubahan data Terhadap Data yang salah</li>
                            <li>Lalu Klik Rubah Data.</li>
                            <li>Lakukan Pengecekan Ulang pada tanda terima.</li>
                        </ol>                --}}
                        
                    </div>
                    <div class="callout callout-info">
                        {{--  <img src="../../dist/img/20200827_114722.gif" alt="AdminLTE Logo" width="200">               --}}
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer" >
            
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

       

   
</div>
@endsection


@push('scripts')
<script>
   
	$(document).ready(function()
    {    
      $("#nik").change(function()
      {   
        var name = $(this).val() 
        
        if(name.length > 0)
        {   
          $("#result").html('checking...');       
          $.ajax({
            headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            type : 'POST',
            url  : '/cariniktahap2/' + name,
            data : 'json',
            
            success : function(data)
                  {
                    
                    if(data.nama_lengkap == null){
                        
                        $("#result").html("DATA TIDAK DITEMUKAN");
                        
                    }
                    else{
                        $("#result").html("LENGKAPI DATA");
                        document.getElementById("nama_dpn").value = data.nama_lengkap;
                        
                        document.getElementById("alamat").value = data.alamat;
                        document.getElementById("kecamatan").value = data.kecamatan;
                        document.getElementById("cari_kelurahan").value = data.kelurahan;
                        document.getElementById("hp").value = data.no_telp;
                        document.getElementById("bidang_usaha").value = data.bidang_usaha;
                        document.getElementById("no_reg").value = data.no_reg;
                        
                    }
                    }
            });
            return false;
          
        }
        else
        {
            
            document.getElementById("nama_dpn").value = "";
                        
            document.getElementById("alamat").value = "";
            document.getElementById("kecamatan").value = "";
            document.getElementById("cari_kelurahan").value = "";
            document.getElementById("hp").value = "";
            document.getElementById("bidang_usaha").value = "";
            $("#result").html("INPUTKAN NIK");
        }
              
      });
      
    });

</script>
<script type="text/javascript">
		
    $("#cari_kelurahan").change(function() {
        if ($("#cari_kelurahan").val() != ""){
          var kecamatan = $("#cari_kelurahan").val();
         
          $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "POST",
            url: "/carikelbansos/" +kecamatan,
            
            data: {
                id:kecamatan
                },    
           
            success: function(data) {
                document.getElementById("kecamatan").value = data.KECAMATAN;
            }
          });
        }
      });

</script>


@endpush