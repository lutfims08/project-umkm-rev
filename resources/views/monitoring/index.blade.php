@extends('layouts.app')

@section('title', 'CARI DATA MONITORING UMKM ')

@section('content')

@if (session('statustambah'))
    <div class="alert alert-success">
        {{ session('statustambah') }}
    </div>
@endif

<section class="content">
   <div class="container-fluid">
        <div class="form-horizontal">
           <div class="box box-default">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title"></h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fas fa-minus"></i></button>
                            </button>
                        </div>
                    </div>
                    <form action="/carimonitoring" method="post">
                        <div class="row">
                            <div class="col-md-6">                           
                                <div class="card-body">
                                    <div class="form-group">
                                    <label for="exampleInputEmail1">Nama Pemilik</label>
                                    <input type="text" class="form-control" id="nama_dpn" name="nama_dpn" placeholder="Nama Pemilik">
                                    </div>
                                    <div class="form-group">
                                    <label for="exampleInputPassword1">Nama Usaha</label>
                                    <input type="text" class="form-control" id="nama_usaha" name="nama_usaha" placeholder="Nama Usaha">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Kecamatan</label>
                                        <select class="form-control select2  @error('kecamatan')== null is-invalid @enderror" id="kecamatan" name="kecamatan" style="width: 100%;" >
                                            <option selected disabled value="">-- Pilih Kecamatan --</option>
                                            @foreach($kec as $k1)
                                                
                                                <option  <?php 
                                                    if (old('kecamatan') == $k1->id_kecamatan){
                                                    echo "selected";	
                                                    }
                                                    else{
                                                    }
                                                    ?> 
                                                value = "<?php echo $k1->id_kecamatan?>"><?php echo $k1->kecamatan; ?>
                                                </option>
                                                @endforeach
                                        </select>
                                    </div>
                                    <div class="card-footer">
                                        <button type="button" id="caridata" class="btn btn-primary detail" data-toggle="modal" data-target="#modal-default">Submit</button>
                                        
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="col-md-6">                           
                                <div class="card-body">
                                    <div class="callout callout-info">
                                        <h5>Hal yang harus diperhatikan :</h5>      
                                        
                                        <ol>
                                            <li>Isikan Nama Pemilik, Nama Usaha dan Kecamatan</li>
                                            <li>Apabila di dalam data hanya ada Nama Pemilik maka nama usaha di biarkan kosong</li>
                                            <li>didalam pencarian data di harapkan disesuaikan dengan data yang terlah tersedia</li>
                                        </ol>              
                                        
                                      </div> 
                                </div>
                            </div>
                        </div>
                        
                    </form>
                     
                    
                    

                </div>
           </div>
        </div>
   </div>
   <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Data UMKM</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              
              
            </div>
            
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
</section>



@endsection

@push('scripts')
<script type="text/javascript">
	

      $(document).on('click','.detail',function(e){
        var kecamatan = $("#kecamatan").val();
        var depan = $("#nama_dpn").val();
        var usaha = $("#nama_usaha").val();
        e.preventDefault();
        $("#modal-default").modal('show');
        $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
          });
       
        $.post("/tampildata",
            {
            nama_dpn: depan,
            nama_usaha: usaha,
            kecamatan:kecamatan
            },
            function(data,html){
              $(".modal-body").html(data);
            });
        
    });
</script>

@endpush