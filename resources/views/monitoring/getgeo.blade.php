@extends('layouts.app')

@section('title', 'DATA SEBARAN UMKM KOTA BANDUNG')

@section('content')
<?php
$kecamatan = [
			"Andir"=>["#888888",304],
			 "Antapani"=>["#ff0099",236], 
			 "Arcamanik"=>["#009900",352], 
			 "Astanaanyar"=>["#880088",6352],
			 "Babakan_Ciparay"=>["#ff9900",4647],
			 "Bandung_Kulon"=>["#ff00ff",2424],
			 "Bandung_Wetan"=>["#900900",242],
			 "Bandung_Kidul"=>["#225588",635],
			 "Batununggal"=>["#900900",645],
			 "Bojongloa_Kaler"=>["#009900",242],
			 "Bojongloa_Kidul"=>["#ff0000",524],
			 "Buahbatu"=>["#ff0000",2525],
			 "Cibeunying_Kidul"=>["#00ff00", 242],
			 "Cibeunying_Kaler"=>["#ff9900",245], 
			 "Cidadap"=>["#880088",424],
			 "Coblong"=>["#ff0000",422],
			 "Cinambo"=>["#ff00ff",2423],
			 "Cicendo"=>["#00ff00",4242],
			 "Cibiru"=>["#225588",411],
			 "Gedebage"=>["#900900",435],
			 "Kiaracondong"=>["#888888",2524],
			 "Lengkong"=>["#345500",3252],
			 "Mandalajati"=>["#ff0000",12451],
			 "Panyileukan"=>["#00ff00", 3426],
			 "Rancasari"=>["#ff9900", 2432],
			 "Regol"=>["#880088",342],
			 "Sukasari"=>["#ff9900",543],
			 "Sukajadi"=>["#ff00ff",224],
			 "Sumur_Bandung"=>["#ff0099",283],
			 "Ujungberung"=>["#ff9900",290]
			 
			];

?>
<style type="text/css">
	#mapid { height: 500px; }
	.icon {
	display: inline-block;
	margin: 2px;
	height: 16px;
	width: 16px;
	background-color: #ccc;
}
.icon-drinking_water {
	background: url('dist/js/leaflet-panel-layers-master/examples/images/icons/drinking_water.png') center center no-repeat;
}
.info1a { padding: 6px 8px; font: 14px/16px Arial, Helvetica, sans-serif; background: white; background: rgba(255,255,255,0.8); box-shadow: 0 0 15px rgba(0,0,0,0.2); border-radius: 5px; } .info1a h4 { margin: 0 0 5px; color: #777; } 
.info1a b { margin: 0 0 5px; color: #777; }
.legend { text-align: left; line-height: 18px; color: #555; } .legend i { width: 18px; height: 18px; float: left; margin-right: 8px; opacity: 0.7; }

</style>

<section class="col-lg-12 connectedSortable">

    <!-- Map card -->
    <div class="card bg-gradient-primary">
      <div class="card-header border-0">
        <h3 class="card-title">
          <i class="fas fa-map-marker-alt mr-1"></i>
          
        </h3>
        <!-- card tools -->
        <div class="card-tools">
          {{-- <button type="button"
                  class="btn btn-primary btn-sm daterange"
                  data-toggle="tooltip"
                  title="Date range">
            <i class="far fa-calendar-alt"></i>
		  </button> --}}
		  <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
          <button type="button"
                  class="btn btn-primary btn-sm"
                  data-card-widget="collapse"
                  data-toggle="tooltip"
                  title="Collapse">
            <i class="fas fa-minus"></i>
          </button>
        </div>
        <!-- /.card-tools -->
      </div>
      <div class="card-body">
        <div id="mapid"></div>        
      </div>
      <!-- /.card-body-->
      <div class="card-footer bg-transparent">
       
      </div>
    </div>
    <!-- /.card -->

   

   
  </section>

@endsection





@push('scripts')


<script type="text/javascript">
    <?php
    foreach($kec_out as $kec_tol){
        
        $datakec1[str_replace('_', ' ', $kec_tol->kecamatan)]=$kec_tol->TOTAL;
        
    }
    
       
        
     echo 'var JUMLAHUKM='.json_encode($datakec1);
    
    ?>

    


 
 	var map = L.map('mapid').setView([-6.9136727,107.6417273], 12);

 	var LayerKita = L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw'
});

map.addLayer(LayerKita);

    var info1a = L.control({position: 'bottomleft'});
    
    	info1a.onAdd = function (map) {
    		this._div = L.DomUtil.create('div', 'info1a');
    		this.update();
    		return this._div;
    	};
    
    	info1a.update = function (props) {
    		this._div.innerHTML = '<h4>Sebaran UMKM di KOTA BANDUNG</h4>' +  (props ?
			'<b>Kecamatan : ' + props.nama_kecamatan + '<br /><br />Jumlah : ' + JUMLAHUKM[props.nama_kecamatan] + ' Unit Usaha</b>'
    			: '<b>Arahkan Mouse</b>');
    	};
        
    	info1a.addTo(map);

    function getColor(d) {
    		return d > 500 ? '#FF4500' :
    				// d > 475  ? '#40E0D0' :
    				d > 450  ? '#6A5ACD' :
    				// d > 425  ? '#D8BFD8' :
    				d > 400   ? '#708090' :
    				// d > 375  ? '#D2B48C' :
    				d > 350   ? '#00FF7F' :
    				// d > 325   ? '#FED976' :
    				d > 300   ? '#4682B4' :
    				// d > 275   ? '#FED976' :
    				d > 250   ? '#D2B48C' :
    				// d > 225   ? '#FED976' :
    				d > 200   ? '#008080' :
    				// d > 175   ? '#FED976' :
    				d > 150   ? '#FF6347' :
    				// d > 125   ? '#FED976' :
    				d > 100   ? '#40E0D0' :
    				// d > 75   ? '#FED976' :
    				d > 50   ? '#EE82EE' :
    				// d > 25   ? '#FED976' :
    							'#F5DEB3';
    	}

function style(feature) {
		return {
			weight: 2,
			opacity: 1,
			color: 'white',
			dashArray: '3',
			fillOpacity: 0.7,
			fillColor: getColor(JUMLAHUKM[feature.properties.nama_kecamatan])

		};
	}

	function highlightFeature(e) {
		var layer = e.target;

		layer.setStyle({
			weight: 5,
			color: '#666',
			dashArray: '',
			fillOpacity: 0.7
		});

		if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
			layer.bringToFront();
		}

		info1a.update(layer.feature.properties);
	}
	
	var geojson;

	function resetHighlight(e) {
	    var layer = e.target;

		layer.setStyle({
			weight: 2,
			opacity: 1,
			color: 'white',
			dashArray: '3',
			fillOpacity: 0.7
		});
		info1a.update();
	}

	function zoomToFeature(e) {
		map.fitBounds(e.target.getBounds());
	}

	function onEachFeature(feature, layer) {
		layer.on({
			mouseover: highlightFeature,
			mouseout: resetHighlight,
			click: zoomToFeature
		});
	}
	
	var legend = L.control({position: 'bottomleft'});

	legend.onAdd = function (map) {

		var div = L.DomUtil.create('div', 'info1a legend'),
			grades = [0, 50, 100, 150, 200, 250, 300, 350, 400, 450, 500],
			labels = [],
			from, to;

		for (var i = 0; i < grades.length; i++) {
			from = grades[i];
			to = grades[i + 1];

			labels.push(
				'<i style="background:' + getColor(from + 1) + '"></i> ' +
				from + (to ? '&ndash;' + to : '+'));
		}

		div.innerHTML = labels.join('<br>');
		return div;
		
	};

	legend.addTo(map);


    var myStyle = {
        "color": "#ff7800",
        "weight": 1.5,
        "opacity": 0.65
    };
    
   function popUp(f,l){
        var out = [];
        if (f.properties){
            // for(key in f.properties){
                
            // }
            out.push("Kecamatan : "+f.properties["nama_kecamatan"]);
            l.bindPopup(out.join("<br />"));
        }
    }
    
        function iconByName(name) {
        	return '<i class="icon icon-'+name+'"></i>';
        }
        
        function featureToMarker(feature, latlng) {
        	return L.marker(latlng, {
        		icon: L.divIcon({
        			className: 'marker-'+feature.properties.amenity,
        			html: iconByName(feature.properties.amenity),
        			iconUrl: '../images/markers/'+feature.properties.amenity+'.png',
        			iconSize: [25, 41],
        			iconAnchor: [12, 41],
        			popupAnchor: [1, -34],
        			shadowSize: [41, 41]
        		})
        	});
        }
        
        var baseLayers = [
        	{
        		name: "OpenStreetMap",
        		layer: LayerKita
        	},
        	{
        		name: "OpenCycleMap",
        		layer: L.tileLayer('http://{s}.tile.opencyclemap.org/cycle/{z}/{x}/{y}.png')
        	},
        	{
        		name: "Outdoors",
        		layer: L.tileLayer('http://{s}.tile.thunderforest.com/outdoors/{z}/{x}/{y}.png')
        	}
        ];
        <?php
		foreach ($kecamatan as $key => $value) {
            
			?>
            
			var myStyle{{$key}} = {
			    "color": "Array",
			    "weight": 1,
			    "opacity": 0.9
			};

			<?php
			$arrayKec[]='{
			name: "'.str_replace('_', ' ', $key).'",
			icon: iconByName("drinking_water"),
			layer: new L.GeoJSON.AJAX(["../../dist/geojson/'.strtolower(str_replace('_', '', $key)).'.geojson"],{
			   style: style,
		        onEachFeature: onEachFeature 
			    }).addTo(map)
			}';
		}
	?>


	var overLayers = [
        	{
        		group: "GeoJSON Layers",
        		layers: [
        				<?=implode(',', $arrayKec);?>
        		]
        	}
        ];

var panelLayers = new L.Control.PanelLayers(baseLayers, overLayers, {
	//compact: true,
	//collapsed: true,
	collapsibleGroups: true,
// 	position:'bottomleft'
});
    map.addControl(panelLayers);

// marker
var markers = L.markerClusterGroup();

var myIcon = L.icon({
     iconUrl: '../../dist/img/icons8-marker-96.png',
    iconSize: [18, 25],
});

<?php 

		if((auth()->user()->role == 'superadmin') or (auth()->user()->role == 'Bid_UMKM')){
			foreach ($point as $point_out) { 
			?>
			var marker  = L.marker([<?php echo($point_out->Latitude);?> , <?php echo($point_out->Longitude);?>],{icon: myIcon})
				.bindPopup("<p><b>Nama Pemilik :</b> <?php echo($point_out->nama_depan); ?> <br> <b>Nama Usaha :</b> <?php echo($point_out->nama_usaha); ?> <br> <b>Jenis Usaha :</b> <?php echo($point_out->jenis_usaha); ?> <br> <b>Alamat :</b> <?php echo($point_out->alamat); ?></p>" + '<a href="/monev/<?php echo($point_out->id_anggota); ?>" class="badge badge-info">detail</a>'); 
				markers.addLayer(marker);
			<?php
			}
		}
		else{
			foreach ($point as $point_out) { 
				?>
				var marker  = L.marker([<?php echo($point_out->Latitude);?> , <?php echo($point_out->Longitude);?>],{icon: myIcon})
					.bindPopup("<p><b>Nama Pemilik :</b> <?php echo($point_out->nama_depan); ?> <br> <b>Nama Usaha :</b> <?php echo($point_out->nama_usaha); ?> <br> <b>Jenis Usaha :</b> <?php echo($point_out->jenis_usaha); ?> <br> <b>Alamat :</b> <?php echo($point_out->alamat); ?></p>" ); 
					markers.addLayer(marker);
				<?php
				}
		}
      ?>
    
   map.addLayer(markers);

</script>



@endpush