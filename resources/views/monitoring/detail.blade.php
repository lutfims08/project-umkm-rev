@extends('layouts.app')

@section('title', 'DETAIL PROFILE UMKM')

@section('content')


<section class="content">
    <div class="container-fluid">
    @foreach($v1 as $result1 => $result )
        <div class="row">
            <div class="col-md-3">
                <div class="card card-primary card-outline">
                    <?php
                    if(($v2[$result1]->aktifasi) =='Aktif'){ ?>
                        <div class="ribbon-wrapper ribbon-lg">
                            <div class="ribbon bg-success text-lg">
                            {{$v2[$result1]->aktifasi}}
                            </div>
                        </div>

                    <?php } else{ ?>
                        <div class="ribbon-wrapper ribbon-lg">
                            <div class="ribbon bg-danger text-lg">
                            {{$v2[$result1]->aktifasi}}
                            </div>
                        </div>
                    <?php } ?>
                    
                    <div class="card-body box-profile">
                        <div class="text-center">
                        <img class="profile-user-img img-fluid img-circle"
                            src="../../dist/img/user4-128x128.jpg"
                            alt="User profile picture">
                        </div>

                        <h3 class="profile-username text-center">{{$result->nama_depan}}</h3>

                        <p class="text-muted text-center">{{$result->email}}</p>

                        <ul class="list-group list-group-unbordered mb-3">
                        <li class="list-group-item">
                            <b>NIK</b> <a class="float-right">{{$result->nik}}</a>
                        </li>
                        <li class="list-group-item">
                            <b>NO HP</b> <a class="float-right">{{$result->no_handphone}}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Jens Kelamin</b> <a class="float-right">{{$result->jenis_kelamin}}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Alamat</b> <a class="float-right">{{$result->alamat}}</a> <br>
                            <b></b> <a class="float-right">{{$v2[$result1]->alamat}}</a>
                        </li>
                        <li class="list-group-item">
                            <b>RT / RT</b> <a class="float-right">{{$result->rt}} / {{$result->rw}}</a> <br>
                            <b></b> <a class="float-right">{{$v2[$result1]->rt}} / {{$v2[$result1]->rw}}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Kelurahan</b> <a class="float-right">{{$result->kelurahan}}</a> <br>
                            <b></b> <a class="float-right">{{$v2[$result1]->kelurahan}}</a> 
                        </li>
                        <li class="list-group-item">
                            <b>Kecamatan</b> <a class="float-right">{{$result->kecamatan}}</a> <br>
                            <b></b> <a class="float-right">{{$v2[$result1]->kecamatan}}</a> 
                        </li>
                        <li class="list-group-item">
                            <b>Kota</b> <a class="float-right">{{$result->kota}}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Kode Pos</b> <a class="float-right">{{$result->kode_pos}}</a> <br>
                            <b></b> <a class="float-right">{{$v2[$result1]->kode_pos}}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Pendidikan</b> <a class="float-right">{{$result->pendidikan}}</a>
                        </li>
                        </ul>
                        @if ((auth()->user()->role == 'superadmin') or (auth()->user()->role == 'Bid_UMKM'))
                        <a href="{{$result->id_anggota}}/edit" class="btn btn-block btn-primary swalDefaultInfo">
                        
                        <i class="far fa-save"> UPDATE</i>
                        </a>
                        <hr>
                        <form action="/monev/{{$result->id_anggota}}" method="POST">
                        @method('delete')
                        @csrf
                            <button type="button" class="btn btn-block btn-danger" data-toggle="modal" data-target="#modal-default"> 
                            <!-- <button type="submit" class="btn btn-block btn-danger" onclick="return confirm('Apakah Anda Yakin Akan Menghapus Data ini')"> -->
                            <i class="fas fa-trash-alt">
                                DELETE
                                </i>
                            </button>
                        </form>
                        @else
                        @endif
                        <hr>
                        <a href="/monev" class="btn btn-block btn-default">
                        <i class="fa fa-reply"> BACK</i>
                        </a>
                        

                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
            <div class="col-md-9">
                <div class="card">
                    <div class="card-header p-2">
                        <ul class="nav nav-pills">
                            
                            <li class="nav-item"><a class="nav-link active" href="#usaha" data-toggle="tab">Data Usaha</a></li>
                            <li class="nav-item"><a class="nav-link" href="#timeline" data-toggle="tab">Legalitas Usaha</a></li>
                        </ul>
                    </div>
                    <div class="card-body">
                        <div class="tab-content">
                            <div class="active tab-pane" id="usaha">
                                <!-- Pribadi -->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="callout callout-info">
                                            <p class="text-muted text-center">Profile Usaha</p>
                                            <ul class="list-group list-group-unbordered mb-3">
                                                <li class="list-group-item">
                                                    <b>Nama Usaha</b> <a class="float-right">{{$result->nama_usaha}}</a> <br>
                                                    <b></b> <a class="float-right">{{$v2[$result1]->nama_usaha}}</a> <br>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Alamat Usaha</b> <a class="float-right">{{$result->alamat_usaha}}</a> <br>
                                                    <b></b> <a class="float-right">{{$v2[$result1]->alamat_usaha}}</a>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>RT /RW </b> <a class="float-right">{{$result->rt}} / {{$result->rw}}</a> <br>
                                                    <b> </b> <a class="float-right">{{$v2[$result1]->rt}} / {{$v2[$result1]->rw}}</a> <br>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>No Telp</b> <a class="float-right">{{$result->no_telepon}}</a> <br>
                                                    <b></b> <a class="float-right">{{$v2[$result1]->no_telepon}}</a> <br>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Tahun Pendaftaran</b> <a class="float-right">{{$result->thn_daftar}}</a>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Tahun Berdiri</b> <a class="float-right">{{$result->thn_berdiri}}</a>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Bentuk Usaha</b> <a class="float-right">{{$result->bentuk_usaha}}</a> <br>
                                                    <b></b> <a class="float-right">{{$v2[$result1]->bentuk_usaha}}</a> <br>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Jenis Usaha</b> <a class="float-right">{{$result->jenis_usaha}}</a> <br>
                                                    <b></b> <a class="float-right">{{$v2[$result1]->jenis_usaha}}</a> <br>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Produk</b> <a class="float-right">{{$result->produk_1}}, {{$result->produk_2}}, {{$result->produk_3}} </a> <br>
                                                    <b></b> <a class="float-right">{{$v2[$result1]->produk_1}}, {{$v2[$result1]->produk_2}}, {{$v2[$result1]->produk_3}} </a>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Jumlah Karyawan</b> <a class="float-right">{{$result->jumlah_karyawan}} </a> <br>
                                                    <b></b> <a class="float-right">{{$v2[$result1]->jumlah_karyawan}} </a>
                                                </li>
                                            </ul>
                                            <p class="text-muted text-center">Bahan Baku Produk</p>
                                            <ul class="list-group list-group-unbordered mb-3">
                                                <li class="list-group-item">
                                                    <b>Bahan Baku Utama</b> <a class="float-right">{{$result->bahan_utama}}</a> <br>
                                                    <b></b> <a class="float-right">{{$v2[$result1]->bahan_utama}}</a> <br>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Perolehan Bahan Baku Utama</b> <a class="float-right">{{$result->perolehan}}</a> <br>
                                                    <b></b> <a class="float-right">{{$v2[$result1]->perolehan}}</a>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Detail Perolehan</b> <a class="float-right">{{$result->detail_perolehan}}</a> <br>
                                                    <b></b> <a class="float-right">{{$v2[$result1]->detail_perolehan}}</a> <br>
                                                </li>
                                                
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="callout callout-info">
                                                                                
                                            <p class="text-muted text-center">Manajemen Keuangan</p>
                                            <ul class="list-group list-group-unbordered mb-3">
                                                <li class="list-group-item">
                                                    <b>Manajemen Keuangan</b> <a class="float-right">{{$result->manajemen_keuangan}}</a> <br>
                                                    <b></b> <a class="float-right">{{$v2[$result1]->manajemen_keuangan}}</a>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Detail Manajemen</b> <a class="float-right">{{$result->detail_manajemen}}</a> <br>
                                                    <b></b> <a class="float-right">{{$v2[$result1]->detail_manajemen}}</a>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Modal Sendiri</b> <a class="float-right">{{$result->modal_sendiri}}</a> <br>
                                                    <b></b> <a class="float-right">{{$v2[$result1]->modal_sendiri}}</a>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Modal Luar</b> <a class="float-right">{{$result->modal_luar}}</a> <br>
                                                    <b></b> <a class="float-right">{{$v2[$result1]->modal_luar}}</a>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Penjamin Keuangan</b> <a class="float-right">{{$result->penjamin}}</a> <br>
                                                    <b></b> <a class="float-right">{{$v2[$result1]->penjamin}}</a> 
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Detail Penjamin</b> <a class="float-right">{{$result->detail_penjamin}}</a> <br>
                                                    <b></b> <a class="float-right">{{$v2[$result1]->detail_penjamin}}</a> 
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Nominal Pinjaman</b> <a class="float-right">{{$result->nominal}}</a> <br>
                                                    <b></b> <a class="float-right">{{$v2[$result1]->nominal}}</a> <br>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Jenis Omset</b> <a class="float-right">{{$result->jenis_omset}}</a> <br>
                                                    <b></b> <a class="float-right">{{$v2[$result1]->jenis_omset}}</a> <br>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Omset/Thn</b> <a class="float-right">Rp. {{$result->jumlah_omset}}</a> <br>
                                                    <b></b> <a class="float-right">Rp. {{$v2[$result1]->jumlah_omset}}</a> <br>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Jenis Aset</b> <a class="float-right">{{$result->jenis_aset}}</a> <br>
                                                    <b></b> <a class="float-right">{{$v2[$result1]->jenis_aset}}</a> <br>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Jumlah Aset</b> <a class="float-right">Rp. {{$result->jumlah_aset}}</a> <br>
                                                    <b></b> <a class="float-right">Rp. {{$v2[$result1]->jumlah_aset}}</a> <br>
                                                </li>
                                                                                            
                                            </ul>
                                        
                                            <p class="text-muted text-center">Promosi dan Pemasaran</p>
                                            <ul class="list-group list-group-unbordered mb-3">
                                                <li class="list-group-item">
                                                    <b>Pasar</b> <a class="float-right">{{$result->pemasaran}}</a> <br>
                                                    <b></b> <a class="float-right">{{$v2[$result1]->pemasaran}}</a> <br>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Detail Pasar</b> <a class="float-right">{{$result->detailpemasaran}}</a> <br>
                                                    <b></b> <a class="float-right">{{$v2[$result1]->detailpemasaran}}</a> <br>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Kerjasama</b> <a class="float-right">{{$result->kerjasama}}</a> <br>
                                                    <b></b> <a class="float-right">{{$v2[$result1]->kerjasama}}</a> 
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Detail Kerjasama</b> <a class="float-right">{{$result->detailkerjasama}}</a>
                                                    <b>Detail Kerjasama</b> <a class="float-right">{{$v2[$result1]->detailkerjasama}}</a>
                                                </li>                                               
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12"> 
                                    <div class="callout callout-success">
                                        <p class="text-muted text-center">Keterangan</p>
                                        <ul class="list-group list-group-unbordered mb-9">
                                        <a class="float-center">{{$result->pelatihan}}</a> <br>
                                        <a class="float-center">{{$v2[$result1]->pelatihan}}</a> <br>
                                        </ul>
                                    </div>
                                </div>
                                <!-- /.Pribadi -->
                            </div>

                            <div class="tab-pane" id="timeline">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="callout callout-info">
                                            <p class="text-muted text-center">Legalitas Usaha</p>
                                            <ul class="list-group list-group-unbordered mb-3">
                                                <li class="list-group-item">
                                                    <b>No. NIB + IUMK</b> <a class="float-right">{{$result->nib}}</a> <br>
                                                    <b></b> <a class="float-right">{{$v2[$result1]->nib}}</a> <br>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>No. NPWP</b> <a class="float-right">{{$result->npwp}}</a> <br>
                                                    <b></b> <a class="float-right">{{$v2[$result1]->npwp}}</a> <br>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>No. HAKI</b> <a class="float-right">{{$result->haki}}</a> <br>
                                                    <b></b> <a class="float-right">{{$v2[$result1]->haki}}</a> 
                                                </li>
                                                <li class="list-group-item">
                                                    <b>No. HALAL</b> <a class="float-right">{{$result->halal}}</a> <br>
                                                    <b></b> <a class="float-right">{{$v2[$result1]->halal}}</a> <br>
                                                </li>                                               
                                                <li class="list-group-item">
                                                    <b>No. P-IRT</b> <a class="float-right">{{$result->pirt}}</a> <br>
                                                    <b></b> <a class="float-right">{{$v2[$result1]->pirt}}</a> <br>
                                                </li>                                               
                                                <li class="list-group-item">
                                                    <b>No. BPOM</b> <a class="float-right">{{$result->bpom}}</a> <br>
                                                    <b></b> <a class="float-right">{{$v2[$result1]->bpom}}</a> <br>
                                                </li>                                               
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                                            <ol class="carousel-indicators">
                                                <?php
                                                for($i=0; $i<$countgambar;$i++){
                                                    echo '<li data-target="#carouselExampleIndicators" data-slide-to="'.$i.'"'; if($i==0){ echo 'class="active"'; } echo '></li>';
                                                }
                                                ?>
                                            </ol>
                                            <div class="carousel-inner">
                                                @foreach($gambar as $key => $slider)
                                                    <div class="carousel-item {{$key == 0 ? 'active' : '' }}">
                                                        <img src="{{url('/data_file/', $slider->file)}}" class="d-block w-100"  alt="...">
                                                        <div class="carousel-caption">
                                                            <h1 class="carousel-caption-header">{{$slider->file}}</h1>
                                                            
                                                        </div> 
                                                    </div>
                                                @endforeach 
                                            </div>
                                            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
    </div>
    <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
          <div class="modal-content bg-danger">
            <div class="modal-header">
              <h4 class="modal-title">Konfirmasi Hapus Data !!</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Apakah anda yakin ingin menghapus data ini :</p>
              <p>Nama : {{$result->nama_depan}} </p>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <form action="/monev/{{$result->id_anggota}}" method="POST">
                        @method('delete')
                        @csrf
                           <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                            <button type="submit" class="btn btn-block btn-info" onclick="return confirm('Apakah Anda Yakin Akan Menghapus Data ini')">
                            <i class="fas fa-trash-alt">
                                DELETE
                                </i>
                            </button>
                        </form>
              
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
</section>

@endsection

@push('scripts')
<script>
function myFunction() {
  confirm("Apakah Anda Yakin Akan Menghapus Data ini");
}
</script>

@endpush