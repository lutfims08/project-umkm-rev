@extends('layouts.app')

@section('title', 'UPDATE DATA UMKM')

@section('content')

<section class="content">
	<div class="form-horizontal">
		<div class="box box-default">
			<div class="card card-primary">
				<div class="card-header">
					<h3 class="card-title"></h3>

					<div class="card-tools">
						<button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
						<i class="fas fa-minus"></i></button>
					</div>
				</div>
				@foreach($v1 as $result)
				<div class="card-body">
					<div class="row">
						<div class="col-md-12">
							
								<div class="card">
									<div class="card-header p-2">
										<ul class="nav nav-pills">
										<li class="nav-item"><a class="nav-link active" href="#pribadi" data-toggle="tab">Data Pribadi</a></li>
										<li class="nav-item"><a class="nav-link" href="#usaha" data-toggle="tab">Data Usaha</a></li>
										<li class="nav-item"><a class="nav-link" href="#lainya" data-toggle="tab">Data Lainnya</a></li>
										{{--  <li class="nav-item"><a class="nav-link" href="#upload" data-toggle="tab">Upload Dokumen</a></li>  --}}
										</ul>
									</div><!-- /.card-header -->
									<form class="form-horizontal" method="POST" action="/monitoring/monev">
									
									@csrf
									<div class="card-body">
										<div class="tab-content">
											<div class="active tab-pane" id="pribadi">
												<div class="form-group clearfix">
													<label for="inputEmail3" class="col-sm-2 control-label" align="right">AKTIVASI USAHA</label>
														<div class="col-sm-1 icheck-success d-inline">
															<input type="radio" name="aktivasi" checked id="aktivasi1" value="Aktif">
															<label for="aktivasi1">Aktif
															</label>
														</div>
														<div class="col-sm-1 icheck-success d-inline">
															<input type="radio" name="aktivasi" disabled id="aktivasi2" value="Non Aktif">
															<label for="aktivasi2">
															Non Aktif
															</label>
														</div>	
												</div>

												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label" for="nik" align="right">NIK</label>

													<div class="col-sm-3">
														<input type="hidden" class="form-control" id="id_anggota" name="id_anggota" 
														value="{{$result->id_anggota}}">
														<input type="text" class="form-control @error('nik') is-invalid @enderror" id="nik" 
														name="nik" placeholder="Isikan NIK" value="{{$result->nik}}"> <span id="result"></span>
														<div class="invalid-tooltip">
															Silihkan Isikan NIK
														</div>
													</div>
												</div>

												<div class="form-group row">
												  	<label for="luti" class="col-sm-2 col-form-label" align="right">Latitude</label>
													<div class="col-sm-2">
														<input type="text" name="luti1" id="luti1" class="form-control" placeholder="" aria-describedby="helpId">
														
													</div>
													<label for="long" class="col-sm-2 col-form-label" align="right">Latitude</label>
													<div class="col-sm-2">
														<input type="text" name="long1" id="long1" class="form-control" placeholder="" aria-describedby="helpId">
													</div>
													
													
													
												</div>
												<div class="form-group row">
													<label for="long" class="col-sm-2 col-form-label" align="right"></label>
													<p id="tampil"></p>
													<p id="view_err"></p>
													<div class="col-sm-2">
														
														{{-- <i  class="btnRegister1" align="center" onclick="getLocation() ">Get Lokasi</i> --}}
														<a><button type="button" name="loc" id="loc" class="btn btn-info float-right" onclick="getLocation() ">
															<i class="far fa-hand-point-right">Get Lokasi</i></button></a>
													</div>
												</div>

												<div class="form-group row ">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right">Nama Pemilik</label>
													<div class="col-sm-6">
														<input type="text" class="form-control @error('nama_dpn') is-invalid @enderror"  
														id="nama_dpn" name="nama_dpn"  placeholder="Nama Depan" value="{{$result->nama_depan}}">
														<div class="invalid-tooltip">
															Silihkan Isikan Nama Pemilik
														</div>
													</div>	
												</div>
												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right">Alamat</label>

													<div class="col-sm-10">
														<textarea name="alamat" id="alamat" class="@error('alamat') is-invalid @enderror" 
														rows="4" cols="63" placeholder=" Isikan alamat lengkap" >{{$result->alamat}}</textarea>
														<div class="invalid-tooltip">
															Silihkan Isikan Alamat
														</div>
													</div>
												</div>

												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label " align="right">RT</label>

													<div class="col-sm-1">
														<input type="text" style="text-align:right" id="rt" name="rt" 
														class="form-control @error('rt') is-invalid @enderror"  placeholder="" value="{{$result->rt}}">
														
													</div>
													
													
													<label for="inputEmail3" class="col-sm-1 col-form-label" align="right">RW</label>
													<div class="col-sm-1">
														<input type="text" style="text-align:right" class="form-control @error('rw') is-invalid @enderror" 
														id="rw" name="rw" placeholder="" value="{{$result->rw}}">
														
													</div>
													

													<label for="inputEmail3" class="col-sm-1 col-form-label" align="right">No. Telp</label>
													<div class="col-sm-2">
														<input type="text" class="form-control @error('telp') is-invalid @enderror" id="telp" 
														name="telp" placeholder="No. Telepon" value="{{ $result->no_telepon}}">
														
													</div>
													

												</div>

												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right">No. Handphone</label>

													<div class="col-sm-2">
														<input type="text"  class="form-control @error('hp') is-invalid @enderror" 
														data-inputmask="'mask': ['9999-9999-9999']" data-mask
														id="hp" name="hp" placeholder="No. Handphone" value="{{$result->no_handphone}}" >
														<div class="invalid-tooltip">
															Silihkan Isikan No Handphone
														</div>
													</div>
													

													<label for="inputEmail3" class="col-sm-1 col-form-label" align="right">Email</label>

													<div class="col-sm-3">
														<input type="email"  class="form-control @error('email') is-invalid @enderror" 
														id="email" name="email" placeholder="Email" value="{{ $result->email }}">
														<div class="invalid-tooltip">
															Silihkan Isikan Email
														</div>
													</div>
													
												</div>

												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label " align="right">Jenis Kelamin</label>

													<div class="col-sm-6">
														<select class="form-control select2 @error('jeniskelamin')== null is-invalid @enderror" 
														id="jeniskelamin" name="jeniskelamin" style="width: 100%;" value="{{ old('jeniskelamin') }}">
															<option selected  value="">-- Pilih Jenis Kelamin --</option>
															<option value ="Laki - laki "<?php if($result->jenis_kelamin=="Laki - laki"){ echo "selected";}?>>Laki - Laki</option>
															<option value ="Perempuan"<?php if($result->jenis_kelamin =="Perempuan"){ echo "selected";}?>>Perempuan</option>
														</select>
														<div class="invalid-tooltip">
															Silihkan Pilih Jenis Kelamin
														</div>
														
													</div>
												</div>

												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right">Kecamatan</label>

													<div class="col-sm-6">
													<select class="form-control select2  @error('kecamatan')== null is-invalid @enderror" id="kecamatan" name="kecamatan" style="width: 100%;" >
													<option selected disabled value="">-- Pilih Kecamatan --</option>
													@foreach($kec as $k1)
														
														<option  <?php 
																if ($k1->id_kecamatan == $result->id_kecamatan){
																echo "selected";	
																}
																else{
																}
																?> 
															value = "<?php echo $k1->id_kecamatan?>"><?php echo $k1->kecamatan; ?>
														</option>
														@endforeach
														</select>
														<div class="invalid-tooltip">
															Silihkan Pilih Kecamatan
														</div>
													</div>
												</div>

												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right">Kelurahan</label>

													<div class="col-sm-6">
													<select class="form-control select2  @error('kelurahan')==null is-invalid @enderror" id="cari_kelurahan" name="kelurahan" style="width: 100%;" >
													@foreach($kel as $kel1)
														<option <?php 
																if ($result->id_kelurahan == $kel1->id_kelurahan){
																echo "selected";	
																}
																else{
																}
																?>
																value = "<?php echo $kel1->id_kelurahan?>"><?php echo $kel1->kelurahan; ?>

														@endforeach
														
														</select>
													</div>	
												</div>

												<div class="form-group row ">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right">Kota</label>

													<div class="col-sm-3">
														<input type="text"  class="form-control" id="kota" name="kota" value="Bandung" readonly="" >
													</div>

													<label for="inputEmail3" class="col-sm-1 col-form-label" align="right">Kode Pos</label>

													<div class="col-sm-2">
														<input type="text"  class="form-control" id="kode_pos" name="kode_pos" value="{{ $result->kode_pos }}" >
													</div>
												</div>

												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right"> Pendidikan Terakhir</label>
														<div class="col-sm-1 icheck-success d-inline">
															<input type="radio" name="pendidikan" id="pendidikan1"  value="SD"
															<?php if($result->pendidikan=="SD"){ echo "checked";}?>>	
																<label  for="pendidikan1">SD</label>
														</div>
														<div class="col-sm-1 icheck-success d-inline">
															<input type="radio" name="pendidikan" id="pendidikan2" class="col-sm-40 d-inline" value="SMP"
															<?php if($result->pendidikan=="SMP"){ echo "checked";}?>>
																<label for="pendidikan2">SMP</label>
														</div>
														<div class="col-sm-1 icheck-success d-inline">
															<input type="radio" name="pendidikan" id="pendidikan7" class="col-sm-40 d-inline" value="SMA"
															<?php if($result->pendidikan=="SMA"){ echo "checked";}?>>
																<label for="pendidikan7">SMA</label>
														</div>
														<div class="col-sm-1 icheck-success d-inline">
															<input type="radio" name="pendidikan" id="pendidikan3" class="col-sm-40 d-inline" value="D3"
															<?php if($result->pendidikan=="D3"){ echo "checked";}?>>
																<label for="pendidikan3">D3</label>
														</div>
														<div class="col-sm-1 icheck-success d-inline">
														<input type="radio" name="pendidikan" id="pendidikan4" value="S1"
														<?php if($result->pendidikan=="S1"){ echo "checked";}?>>
																<label for="pendidikan4">S1</label>
														</div>
														<div class="col-sm-1 icheck-success d-inline">
															<input type="radio" name="pendidikan" id="pendidikan5" value="S2"
															<?php if($result->pendidikan=="S2"){ echo "checked";}?>>
																<label for="pendidikan5">S2</label>
														</div>
														<div class="col-sm-1 icheck-success d-inline">
															<input type="radio" name="pendidikan" id="pendidikan6" value="S3"
															<?php if($result->pendidikan=="S3"){ echo "checked";}?>>
																<label for="pendidikan6">S3</label>
														</div>
													</div>
													<div class="card-footer" >
														<a href="#usaha" ><button type="button" name="next1" id="next1" class="btn btn-info float-right "><i class="far fa-hand-point-right">Lanjut</i></button></a>
													</div>
											</div>

											<div class="tab-pane" id="usaha">
												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right">Nama Usaha</label>

													<div class="col-sm-6">
														<input type="text" id="nama_usaha" name="nama_usaha" class="form-control @error('nama_usaha') is-invalid @enderror"  placeholder="Isikan Nama Usaha" value="{{ $result->nama_usaha}}">
														<div class="invalid-tooltip">
															Silihkan Isikan Nama Usaha
														</div>
													</div>
												</div>
												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right">Alamat</label>

													<div class="col-sm-6">
														<textarea name="alamat_usaha" id="alamat_usaha" class="@error('alamat_usaha') is-invalid @enderror" rows="4" cols="63" placeholder=" Isikan alamat lengkap" value="{{ old('alamat_usaha') }}">{{ $result->alamat_usaha }}</textarea>
														<div class="invalid-tooltip">
															Silihkan Isikan Alamat Usaha
														</div>
													</div>

													
												</div>
												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right">RT</label>

													<div class="col-sm-1">
														<input type="text" style="text-align:right" class="form-control" id="rt_usaha" name="rt_usaha" placeholder="" value="{{ $result->rt_usaha }}">
													</div>

													<label for="inputEmail3" class="col-sm-1 col-form-label" align="right">RW</label>
													<div class="col-sm-1">
														<input type="text" style="text-align:right" class="form-control" id="rw_usaha" name="rw_usaha" placeholder="" value="{{ $result->rw_usaha }}">
													</div>

													<label for="inputEmail3" class="col-sm-1 col-form-label" align="right">No. Telp</label>
													<div class="col-sm-2">
														<input type="text" class="form-control" id="telp_usaha" name="telp_usaha" placeholder="Isikan No. Telepon" value="{{ $result->no_telepon_usaha }}">
													</div>

												</div>
												
												<div class="form-group row">
													<label  class="col-sm-2 col-form-label" align="right">Tahun Pendaftaran</label>
														<div class="col-sm-2">
															<div class="input-group date " id="reservationdate" data-target-input="nearest">
																<input type="text" name="thn_pendaftaran" class="form-control datetimepicker-input @error('thn_pendaftaran') is-invalid @enderror" 
																	data-target="#reservationdate" placeholder="Pendaftaran " 
																	id="reservationdate"   value="{{$result->thn_daftar}}" /> 
																<div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
																	<div class="input-group-text"><i class="fa fa-calendar"></i></div>
																</div>
																<div class="invalid-tooltip">
															Silihkan Pilih Tahun 
														</div>
															</div>
														</div>
												<!-- </div>
												<div class="form-group row"> -->
													<label  class="col-sm-2 col-form-label" align="right">Tahun Berdiri</label>
														<div class="col-sm-2">
															<div class="input-group" id="reservationdate1" data-target-input="nearest">
																<input type="text" name="thn_berdiri" class="form-control datetimepicker-input @error('thn_berdiri') is-invalid @enderror" 
																	data-target="#reservationdate1" placeholder="Berdiri " 
																	id="reservationdate1" value="{{$result->thn_berdiri}}"/> 
																<div class="input-group-append" data-target="#reservationdate1" data-toggle="datetimepicker">
																	<div class="input-group-text"><i class="fa fa-calendar"></i></div>
																</div>
																<div class="invalid-tooltip">
															Silihkan Pilih Tahun
														</div>	
															</div>
														</div>	
												</div>

												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right">Bentuk Usaha</label>

													<div class="col-sm-2">
													<select class="form-control select2 @error('bentuk_usaha') is-invalid @enderror" name="bentuk_usaha" style="width: 100%;" >
														<option selected disabled value="" selected="selected">-- Pilih Bentuk Usaha--</option>
														<option value="PD" <?php if($result->bentuk_usaha=="PD"){ echo "selected";}?>>PD</option>                                    	
														<option value="Perseorangan" <?php if($result->bentuk_usaha=="Perseorangan"){ echo "selected";}?>>Perorangan</option>
														</select>
														<div class="invalid-tooltip">
															Silihkan Pilih Bentuk Usaha
														</div>
													</div>
													
												<!-- </div>
												<div class="form-group row"> -->
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right">Jenis Usaha</label>

													<div class="col-sm-2">
													<select class="form-control select2 @error('jenis_usaha') is-invalid @enderror" name="jenis_usaha" style="width: 100%;" >
													<option selected disabled value="" value="" selected="selected">-- Pilih Jenis Usaha--</option>
															<option value="Fashion" <?php if($result->jenis_usaha=="Fashion"){ echo "selected";}?>>Fashion</option>
															<option value="Kuliner" <?php if($result->jenis_usaha=="Kuliner"){ echo "selected";}?>>Kuliner</option>
															<option value="Jasa" <?php if($result->jenis_usaha=="Jasa"){ echo "selected";}?>>Jasa</option>
															<option value="Perdagangan"<?php if($result->jenis_usaha=="Perdagangan"){ echo "selected";}?>>Perdagangan</option>
															<option value="Handicraft"<?php if($result->jenis_usaha=="Handicraft"){ echo "selected";}?>>Handicraft</option>
															<option value="lain-lain" <?php if($result->jenis_usaha=="lain-lain"){ echo "selected";}?>>Lainnya</option>
														</select>
														<div class="invalid-tooltip">
															Silihkan Pilih Jenis Usaha
														</div>
													</div>
													
												</div>
												<hr class="col-sm-12 ">			
												<div class="input-group">
													<label for="inputEmail3" class="col-sm-5 col-form-label" align="right">Spesifikasi Produk </label>
												</div>
												<hr class="col-sm-12">
												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right">Jenis Produk</label>
													<div class="col-sm-2">
													<input type="text" name="spesifikasi_produk1" class="form-control @error('spesifikasi_produk1') is-invalid @enderror" 
													placeholder="Contoh produk 1" value="{{ $result->produk_1 }}"> 
													<div class="invalid-tooltip">
															Silihkan Isikan Produk 1
														</div>
													</div>
													<div class="col-sm-2">
													<input type="text" name="spesifikasi_produk2" class="form-control" placeholder="Contoh produk 2" value="{{ $result->produk_2 }}"> 
													</div>
													<div class="col-sm-2">
													<input type="text" name="spesifikasi_produk3" class="form-control" placeholder="Contoh produk 3" value="{{ $result->produk_3 }}"> 
													</div>
												</div>

												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right">Bahan Baku Utama</label>
													<div class="col-sm-6">
													<input type="text" name="bahanbakuutama" class="form-control" placeholder="Bahan Baku Utama" value="{{ $result->bahan_utama }}"> 
													</div>
												</div>

												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right">Perolehan Bahan Baku</label>
													<?php 
													$bb = $result->bahan_baku;
													$ba = explode(",",$bb);
													?>	
													<div class="col-sm-6">
														<div class="col-sm-2 icheck-primary d-inline">
															<input type="checkbox" name="bahan_baku[]" class=" col-sm-1 " id="checkboxPrimary1" value="Lokal"
															<?php if(in_array('Lokal',$ba)) echo 'checked="checked"'?>>
															<label for="checkboxPrimary1">Lokal
															</label>
														</div>
														<div class="col-sm-2 icheck-primary d-inline">
															<input type="checkbox" name="bahan_baku[]" class=" col-sm-1 " id="checkboxPrimary2" value="Regional"
															<?php if(in_array('Regional',$ba)) echo 'checked="checked"'?>>
															<label for="checkboxPrimary2">Regional
															</label>
														</div>
														<div class="col-sm-2 icheck-primary d-inline">
														<input type="checkbox" name="bahan_baku[]" class=" col-sm-1 " id="checkboxPrimary3" value="Nasional"
														<?php if(in_array('Nasional',$ba)) echo 'checked="checked"'?>>
															<label for="checkboxPrimary3">Nasional
															</label>
														</div>
														<div class="col-sm-2 icheck-primary d-inline">
														<input type="checkbox" name="bahan_baku[]" class=" col-sm-1 " id="checkboxPrimary4" value="Import"
														<?php if(in_array('Import',$ba)) echo 'checked="checked"'?>>
															<label for="checkboxPrimary4">Import
															</label>
														</div>
														<label for="inputEmail3" class="col-sm-2 col-form-label" align="right"></label>
														<div class="col-sm-12">
														<input type="text" name="perolehan" class="form-control" placeholder="detailkan perolehan bahan baku" 
														value="{{ $result->detail_perolehan }}">
														</div>
													</div>  
												</div>
												<hr class="col-sm-12 ">			
												<div class="input-group">
													<label for="inputEmail3" class="col-sm-5 col-form-label" align="right">Manajemen Usaha </label>
												</div>
												<hr class="col-sm-12">
												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right">Jumlah Karyawan</label>
													<div class="col-sm-6">
													<input type="text" name="jumlah_karyawan" class="form-control" placeholder="Jumlah Karyawan" value="{{$result->jumlah_karyawan }}"> 
													</div>
												</div>
												
												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right">Sendiri </label>
													<div class="col-sm-2">
														<div class="input-group">
															<div class="input-group-prepend">
																<span class="input-group-text">Rp</span>
															</div>
															<input type="text" class="form-control" id="mdl_sendiri" name="mdl_sendiri" placeholder="" value="{{ $result->modal_sendiri }}" >
														</div>
													</div>
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right"> Modal Luar</label>
													<div class="col-sm-2">
														<div class="input-group">
															<div class="input-group-prepend">
																<span class="input-group-text">Rp</span>
															</div>
															<input type="text" class="form-control" id="mdl_luar" name="mdl_luar" placeholder="" value="{{ $result->modal_luar }}">
															
														</div>
													</div>
												</div>

												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right">Penjamin Keuangan</label>
													<?php 
													$cc = $result->penjamin;
													$ca = explode(",",$cc);
													?>	
													<div class="col-sm-2 icheck-primary d-inline">
													<input type="checkbox" name="penjamin[]" id="penjamin1"   value="Perbankan"
													<?php if(in_array('Perbankan',$ca)) echo 'checked="checked"'?>>
														<label for="penjamin1">Perbankan
														</label>
													</div>
													<div class="col-sm-1 icheck-primary d-inline">
													<input type="checkbox" name="penjamin[]" id="penjamin2" value="PKBL"
													<?php if(in_array('PKBL',$ca)) echo 'checked="checked"'?>>
														<label for="penjamin2">PKBL
														</label>
													</div>
													<div class="col-sm-1 icheck-primary d-inline">
													<input type="checkbox" name="penjamin[]" id="penjamin3"  value="CSR"
													<?php if(in_array('CSR',$ca)) echo 'checked="checked"'?>>
														<label for="penjamin3">CSR
														</label>
													</div>
													<div class="col-sm-1 icheck-primary d-inline">
													<input type="checkbox" name="penjamin[]" id="penjamin4"  value="HIBAH"
													<?php if(in_array('HIBAH',$ca)) echo 'checked="checked"'?>>
														<label for="penjamin4">HIBAH
														</label>
													</div>
													<div class="col-sm-1 icheck-primary d-inline">
													<input type="checkbox" name="penjamin[]" id="penjamin5"  value="Investor"
													<?php if(in_array('Investor',$ca)) echo 'checked="checked"'?>>
														<label for="penjamin5">Investor
														</label>
													</div>
													<div class="col-sm-1 icheck-primary d-inline">
														<input type="checkbox" name="penjamin[]" id="penjamin6"  value="Tidak Ada"
														<?php if(in_array('Tidak Ada',$ca)) echo 'checked="checked"'?>>
															<label for="penjamin6">Tidak Ada
															</label>
														</div>
													
												</div>
												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right"></label>
													<div class="col-sm-6">
													<input type="text" name="penjamindetail" class="form-control" placeholder="detailkan penjamin keuangan" value="{{ $result->detail_penjamin }}">
													</div>
												</div>
												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right">Nominal Pinjaman</label>
													<div class="col-sm-6">
														<div class="input-group">
															<div class="input-group-prepend">
																<span class="input-group-text">Rp</span>
															</div>
															<input type="Text" class="form-control" id="nominalpinjaman" name="nominalpinjaman" placeholder="" value="{{ $result->nominal }}">
														</div>
													</div>
												</div>

												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right">Omset</label>

													<div class="col-sm-6">
														<div class="input-group">
														<div class="input-group-prepend">
																<span class="input-group-text">Rp</span>
															</div>
														<input type="Text" class="form-control @error('omset') is-invalid @enderror" id="omset" name="omset" placeholder=""value="{{ $result->jumlah_omset }}">
														<div class="input-group-append">
															<span class="input-group-text">/Tahun</span>
														</div>
														<div class="invalid-tooltip">
															Silihkan Isikan Omset
														</div>
														</div>
													</div>
												</div>	
												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right">Aset</label>

													<div class="col-sm-6">
														<div class="input-group">
														<div class="input-group-prepend">
																<span class="input-group-text">Rp</span>
															</div>
														<input type="Text" class="form-control @error('aset') is-invalid @enderror" id="aset" name="aset" placeholder="" value="{{ $result->jumlah_aset }}">
														<div class="input-group-append">
															<span class="input-group-text">/Tahun</span>
														</div>
														<div class="invalid-tooltip">
															Silihkan Isikan Aset
														</div>
														</div>
													</div>
												</div>

												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-3 col-form-label" align="right">Manajemen Keuangan</label>
													<div class="col-sm-2 icheck-primary d-inline">
														<input type="radio" name="keuangan" id="keuangan1"  value="Manual"
														<?php if($result->manajemen_keuangan=="Manual"){ echo "checked";}?> >
															<label for="keuangan1">Manual
															</label>
													</div>
													<div class="col-sm-2 icheck-primary d-inline">
														<input type="radio" name="keuangan" id="keuangan2"  value="By Sistem"
														<?php if($result->manajemen_keuangan=="By Sistem"){ echo "checked";}?>>
															<label for="keuangan2"> By Sistem
															</label>
													</div>
												</div>
												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right"></label>
													<div class="col-sm-6">
													<input type="text" name="keuangandetail" class="form-control" placeholder="Jelaskan" value="{{ $result->detail_manajemen }}"> 
													</div>
												</div>
												<div class="card-footer" >
													<a href="#pribadi" ><button type="button" name="back1" id="back1" class="btn btn-default"><i class="far fa-hand-point-left">Kembali</i></button></a>
													<a href="#lainya" ><button type="button" name="next2" id="next2" class="btn btn-info float-right"><i class="far fa-hand-point-right">Lanjut</i></button></a>
												</div>
													
											</div>

											<div class="tab-pane" id="lainya">
												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right">Media Penjualan</label>
													<?php 
													$dd = $result->media_penjualan;
													$da = explode(",",$dd);
													?>	
													<div class="col-sm-2 icheck-primary d-inline">
													<input type="checkbox" name="mediapenjualan[]" id="media1" value="Market Place"
													<?php if(in_array('Market Place',$da)) echo 'checked="checked"'?>>
														<label for="media1">Market Place
														</label>
													</div>
													<div class="col-sm-2 icheck-primary d-inline">
													<input type="checkbox" name="mediapenjualan[]" id="media2" value="Sistem During"
													<?php if(in_array('Sistem During',$da)) echo 'checked="checked"'?>>
														<label for="media2">Sistem During
														</label>
													</div>
													<div class="col-sm-2 icheck-primary d-inline">
													<input type="checkbox" name="mediapenjualan[]" id="media3" value="Konsinyasi"
													<?php if(in_array('Konsinyasi',$da)) echo 'checked="checked"'?>>
														<label for="media3">Konsinyasi
														</label>
													</div>
													<div class="col-sm-2 icheck-primary d-inline">
													<input type="checkbox" name="mediapenjualan[]" id="media4" value="Toko"
													<?php if(in_array('toko',$da)) echo 'checked="checked"'?>>
														<label for="media4">Toko
														</label>
													</div>
												</div>

												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right"></label>
													<div class="col-sm-6">
													<input type="text" name="detailmediapenjualan" class="form-control" placeholder="Jelaskan" value="{{ $result->detail_media }}"> 
													</div>
												</div>

												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right">Daerah Pemasaran</label>
													<?php 
													$ff = $result->pemasaran;
													$fa = explode(",",$ff);
													?>
													<div class="col-sm-2 icheck-primary d-inline">
													<input type="checkbox" name="pasar[]" id="pasar1" value="Bandung Raya"
													<?php if(in_array('Bandung Raya',$fa)) echo 'checked="checked"'?>>
														<label for="pasar1">Bandung Raya
														</label>
													</div>
													<div class="col-sm-2 icheck-primary d-inline">
													<input type="checkbox" name="pasar[]" id="pasar2" value="Regional"
													<?php if(in_array('Regional',$fa)) echo 'checked="checked"'?>>
														<label for="pasar2"> Regional
														</label>
													</div>
													<div class="col-sm-2 icheck-primary d-inline">
													<input type="checkbox" name="pasar[]" id="pasar3" value="Nasional"
													<?php if(in_array('Nasional',$fa)) echo 'checked="checked"'?>>
														<label for="pasar3">Nasional
														</label>
													</div>
													<div class="col-sm-2 icheck-primary d-inline">
													<input type="checkbox" name="pasar[]" id="pasar4" value="Ekspor"
													<?php if(in_array('Ekspor',$fa)) echo 'checked="checked"'?>>
														<label for="pasar4">Ekspor
														</label>
													</div>
												</div>

												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right"></label>
													<div class="col-sm-6">
													<input type="text" name="detailpasar" class="form-control" placeholder="Jelaskan" value="{{ $result->detailpemasaran }}"> 
													</div>
												</div>

												<div class="form-group">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right">Kerjasama Bisnis</label>
													<?php 
													$gg = $result->kerjasama;
													$ga = explode(",",$gg);
													?>
													<div class="col-sm-2 icheck-primary d-inline">
													<input type="checkbox" name="kerjasama[]" id="kerjasama1" value="Pernah"
													<?php if(in_array('Pernah',$ga)) echo 'checked="checked"'?>>
														<label for="kerjasama1">Pernah
														</label>
													</div>
													<div class="col-sm-2 icheck-primary d-inline">
													<input type="checkbox" name="kerjasama[]" id="kerjasama2" value="Tidak Pernah"
													<?php if(in_array('Tidak Pernah',$ga)) echo 'checked="checked"'?>>
														<label for="kerjasama2">Tidak Pernah
														</label>
													</div>
												</div>

												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right"></label>
													<div class="col-sm-6">
													<input type="text" name="detailkerjasama" class="form-control" placeholder="Jelaskan" value="{{ $result->detailkerjasama }}"> 
													</div>
												</div>
												<hr class="col-sm-12 ">			
												<div class="input-group">
													<label for="inputEmail3" class="col-sm-5 col-form-label" align="right">PERIZINAN </label>
												</div>
												<hr class="col-sm-12">
												<div class="form-group row">
												<?php 
												$hh = $result->perijinan;
												$ha = explode(",",$hh);
												?>													
													<div class="col-sm-2 icheck-primary d-inline">
													<input type="checkbox" name="ijin[]" id="izin1" value="NIB + IUMK"
													<?php if(in_array('NIB + IUMK',$ha)) echo 'checked="checked"'?>>
														<label for="izin1">NIB + IUMK
														</label>
														<input type="text" name="nib" class="form-control @error('nib') is-invalid @enderror" class=" col-sm-3" placeholder="NOMOR NIB + IUMK" value="{{$result->nib }}">
														<div class="invalid-tooltip">
															Silihkan Masukan  NO NIB
														</div>
													</div>		
													<div class="col-sm-2 icheck-primary d-inline">
													<input type="checkbox" name="ijin[]" id="izin2" value="NPWP"
													<?php if(in_array('NPWP',$ha)) echo 'checked="checked"'?>>
														<label for="izin2">NPWP
														</label>
														<input type="text" name="npwp" class="form-control @error('npwp') is-invalid @enderror" class=" col-sm-3" placeholder="NOMOR NPWP" value="{{ $result->npwp }}">
														<div class="invalid-tooltip">
															Silihkan Masukan No NPWP
														</div>
													</div>		
													<div class="col-sm-2 icheck-primary d-inline">
													<input type="checkbox" name="ijin[]" id="izin3" value="HAKI"
													<?php if(in_array('HAKI',$ha)) echo 'checked="checked"'?>>
														<label for="izin3">HAKI
														</label>
														<input type="text" name="haki" class="form-control" class=" col-sm-3" placeholder="NOMOR HAKI" value="{{ $result->haki }}">
													</div>		
													<div class="col-sm-2 icheck-primary d-inline">
													<input type="checkbox" name="ijin[]" id="izin4" value="HALAL"
													<?php if(in_array('HALAL',$ha)) echo 'checked="checked"'?>>
														<label for="izin4">HALAL
														</label>
														<input type="text" name="halal" class="form-control" class=" col-sm-3" placeholder="NOMOR HALAL" value="{{ $result->halal }}">
													</div>		
													<div class="col-sm-2 icheck-primary d-inline">
													<input type="checkbox" name="ijin[]" id="izin5" value="P-IRT"
													<?php if(in_array('P-IRT',$ha)) echo 'checked="checked"'?>>
														<label for="izin5">P-IRT
														</label>
														<input type="text" name="pirt" class="form-control" class=" col-sm-3" placeholder="NOMOR P-IRT" value="{{ $result->pirt }}">
													</div>		
													<div class="col-sm-2 icheck-primary d-inline">
													<input type="checkbox" name="ijin[]" id="izin6" value="BPOM"
													<?php if(in_array('BPOM',$ha)) echo 'checked="checked"'?>>
														<label for="izin6">BPOM
														</label>
														<input type="text" name="bpom" id="bpom" class="form-control " class=" col-sm-3" placeholder="NOMOR BPOM" value="{{ $result->bpom }}">
													</div>		
												</div>

												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right">KETERANGAN</label>
																		<div class="col-sm-6">
														<textarea name="keterangan" id="keterangan" rows="4" cols="63" placeholder="">{{ $result->pelatihan }}</textarea>
													</div>
												</div>

												<div class="card-footer" >
														<a href="#usaha" ><button type="button" name="back2" id="back2" class="btn btn-default"><i class="far fa-hand-point-left">  Kembali</i></button></a>
														<button type="submit" class="btn btn-info float-right"><i class="fa fa-save">  Simpan</i></button>
													</div>

											</div>
											{{--  <div class="tab-pane" id="upload">
												
													<div class="form-group row">
														<label for="inputEmail3" class="col-sm-2 col-form-label " align="right">File </label>
														<div class="col-sm-6">
															<input type="file" name="file">
														</div>
													</div>
							 
													<div class="form-group row">
														<label for="inputEmail3" class="col-sm-2 col-form-label " align="right">Jenis Kelamin</label>

														<div class="col-sm-6">
															<select class="form-control select2 @error('jenisfile')== null is-invalid @enderror" 
															id="jenisfile" name="jenisfile" style="width: 100%;" value="{{ old('jenisfile') }}">
																<option selected  value="">-- Pilih file --</option>
																<option value ="ktp">KTP</option>
																<option value ="npwp">NPWP</option>
																<option value ="nib">NIB + IUMK</option>
																<option value ="produk">FOTO PRODUK</option>
															</select>
															<div class="invalid-tooltip">
																Silihkan Pilih Jenis Kelamin
															</div>
															
														</div>
													</div>
													
											</div>  --}}

										</div>
									</div>
									<from>
								</div>
							
						</div>
					</div>

				</div>
				@endforeach
			</div>
		</div>
	</div>
      
</section>

@endsection

@push('scripts')
<script>
$(document).ready(function()
    {    
      $("#nik").keyup(function()
      {   
        var name = $(this).val(); 
        
        if(name.length > 0)
        {   
          $("#result").html('checking...');       
          $.ajax({
            headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            type : 'POST',
            url  : '/carinik/' + name,
            
            success : function(data)
                  {
                       $("#result").html(data);
                    }
            });
            return false;
          
        }
        else
        {
          $("#result").html('');
        }
      });
      
    });
</script>
<script type="text/javascript">
        $("#kecamatan").on('change',function() {
            if ($("#kecamatan").val() != ""){
              var kecamatan = $("#kecamatan").val();
             
              $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: "/carikel/" +kecamatan,
                
                data: {
                    id:kecamatan
                    },    
               
                success: function(html) {
                  $("#cari_kelurahan").html(html);
                }
              });
            }
          });

</script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservationdate').datetimepicker({
        format: 'YYYY'
	});

	$('#reservationdate1').datetimepicker({
        format: 'YYYY'
	});
	
	$('#datepicker').datepicker({
      minViewMode: 2,
       format: 'yyyy',
       autoclose: true
    })
    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY hh:mm A'
      }
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Timepicker
    $('#timepicker').datetimepicker({
      format: 'LT'
    })
    
    //Bootstrap Duallistbox
    $('.duallistbox').bootstrapDualListbox()

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    $('.my-colorpicker2').on('colorpickerChange', function(event) {
      $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    });

    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });

  })
</script>
<script>
$(function () {
    $('#pribadi a').click(function (e) {
        e.preventDefault();
        $('a[href="' + $(this).attr('href') + '"]').tab('show');
    })

     $('#usaha a').click(function (e) {
        e.preventDefault();
        $('a[href="' + $(this).attr('href') + '"]').tab('show');
    })

      $('#lainya a').click(function (e) {
        e.preventDefault();
        $('a[href="' + $(this).attr('href') + '"]').tab('show');
    })
});
 </script>
 <script type="text/javascript">
	var sukses = document.getElementById("tampil");
	var view = document.getElementById("luti1");
	var view1 = document.getElementById("long1");
	var view2 = document.getElementById("luti");
	var view11 = document.getElementById("long");
	var view_err = document.getElementById("view_err")
	function getLocation() {
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(showPosition, showError);
		} else {
			view.innerHTML = "Yah browsernya ngga support Geolocation bro!";
		}
	}
	 function showPosition(position) {
		view.value =  position.coords.latitude.toFixed(6);
		view1.value =  position.coords.longitude.toFixed(6);
		{{-- view2.value =  position.coords.latitude.toFixed(6); --}}
		{{-- view11.value =  position.coords.longitude.toFixed(6); --}}
		sukses.innerHTML = "Sukses, Lokasi Sudah Ditambahkan";
	 }
	 
	 
	 function showError(error) {
		switch(error.code) {
			case error.PERMISSION_DENIED:
				view_err.innerHTML = "Yah, mau deteksi lokasi tapi ga boleh :("
				break;
			case error.POSITION_UNAVAILABLE:
				view_err.innerHTML = "Yah, Info lokasimu nggak bisa ditemukan nih"
				break;
			case error.TIMEOUT:
				view_err.innerHTML = "Requestnya timeout bro"
				break;
			case error.UNKNOWN_ERROR:
				view_err.innerHTML = "An unknown error occurred."
				break;
		}
	 }
	 
	 
	</script>


@endpush