@extends('layouts.app')

@section('title', 'HASIL MONITORING DATA UMKM KOTA BANDUNG')

@section('content')
@if (session('statustambah'))
    <div class="alert alert-success">
        {{ session('statustambah') }}
    </div>
@endif
<section class="content">
      <div class="container-fluid">
        <div class="card card-default">
            <div class="card-header">
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
              </div>
            </div>
            <div class="card-body">
              <table id="viewukm1" class="table table-bordered table-striped">
                <thead class="thead-drak">
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">ID Anggota</th>
                    <th scope="col">Nama Pemilik</th>
                    <th scope="col">Nama Usaha</th>
                    <th scope="col">No HP/WA</th>
                    <th scope="col">Aktifitas Usaha</th>
                    <th scope="col">Kecamatan</th>
                    <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($member as $m)
                    <tr>
                    <th scope="row">{{$loop->iteration}}</th>
                    <td>{{$m->id_anggota}}</td>
                    <td>{{$m->nama_depan}}</td>
                    <td>{{$m->nama_usaha}}</td>
                    <td>{{$m->no_handphone}}</td>
                    <td>{{$m->aktifasi}}</td>
                    <td>{{$m->kecamatan}}</td>
                    <td>
                    <a href="/monev/{{$m->id_anggota}}" class="badge badge-info">detail</a>
                    </td>
                    </tr>
                @endforeach    
                </tbody>
                </table>
            </div>
        </div>
      </div>
</section>
        
@endsection

@push('scripts')

<script>
  $(function () {
  
    $('#viewukm1').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true
    });
  });
  
  </script>
@endpush