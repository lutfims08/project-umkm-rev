@extends('layouts.app')

@section('title', 'CARI DATA MONITORING UMKM ')

@section('content')

@if (session('statustambah'))
    <div class="alert alert-success">
        {{ session('statustambah') }}
    </div>
@endif

<section class="content">
   <div class="container-fluid">
        <div class="form-horizontal">
           <div class="box box-default">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title"></h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fas fa-minus"></i></button>
                            </button>
                        </div>
                    </div>
                    <form action="/upload/proses" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">                           
                                <div class="card-body">
                                    <div class="form-group row">
                                        <label for="inputEmail3" class="col-sm-3 col-form-label " align="right">File</label>
                                        <div class="col-sm-6">
                                        <input type="file" name="file">
                                        <input type="hidden" name="id_anggota" value="{{$id}}">
                                        </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                        <label for="inputEmail3" class="col-sm-3 col-form-label " align="right">DOKUMEN</label>

                                        <div class="col-sm-6">
                                            <select class="form-control select2 @error('jenisdokumen')== null is-invalid @enderror" 
                                            id="jenisdokumen" name="jenisdokumen" style="width: 100%;" value="{{ old('jenisdokumen') }}">
                                                <option selected  value="">-- Pilih Jenis Dokumen --</option>
                                                <option value ="ktp">KTP</option>
                                                <option value ="npwp">NPWP</option>
                                                <option value ="nib">NIB + IUMK</option>
                                                <option value ="produk">Foto Produk 1</option>
                                                <option value ="produk2">Foto Produk 2</option>
                                                <option value ="produk3">Foto Produk 3</option>
                                            </select>
                                            <div class="invalid-tooltip">
                                                Silihkan Pilih Jenis Kelamin
                                            </div>
                                            
                                        </div>
                                    </div>


                                    <div class="card-footer">
                                        <input type="submit" value="Upload" class="btn btn-primary">
                                        
                                    </div>
                                </form>
                                    <h4 class="my-5">Data</h4>
 
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th width="1%">File</th>
                                                <th>Keterangan</th>
                                                <th width="1%">OPSI</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($gambar as $g)
                                            <tr>
                                                <td><img width="150px" src="{{ url('/data_file/'.$g->file) }}"></td>
                                                <td>{{$g->keterangan}}</td>
                                                
                                                        <td>
                                                            <form action="/upload/hapus/{{$g->keterangan}}" method="POST">
                                                                @method('delete')
                                                                @csrf
                                                                <button type="submit" class="btn btn-block btn-danger" onclick="return confirm('Apakah Anda Yakin Akan Menghapus Data ini')">
                                                                    <i class="fas fa-trash-alt">
                                                                        DELETE
                                                                        </i>
                                                                </button>
                                                            </form>
                                                        </td>
                                                
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-6">                           
                                <div class="card-body">
                                    <div class="callout callout-info">
                                        <h5>Hal yang harus diperhatikan :</h5>      
                                        
                                        <ol>
                                            <li>Isikan Nama Pemilik, Nama Usaha dan Kecamatan</li>
                                            <li>Apabila di dalam data hanya ada Nama Pemilik maka nama usaha di biarkan kosong</li>
                                            <li>didalam pencarian data di harapkan disesuaikan dengan data yang terlah tersedia</li>
                                        </ol>              
                                        
                                      </div> 
                                </div>
                            </div>
                            
                        </div>
                        
                        <div class="card-footer">
                            <a href="/monitoring" class="btn btn-block btn-default">
                                <i class="fa fa-reply"> BACK</i>
                                </a>
                            
                        </div>
                     
                    
                    

                </div>
           </div>
        </div>
   </div>
   
</section>



@endsection

@push('scripts')
<script type="text/javascript">
	

      $(document).on('click','.detail',function(e){
        var kecamatan = $("#kecamatan").val();
        var depan = $("#nama_dpn").val();
        var usaha = $("#nama_usaha").val();
        e.preventDefault();
        $("#modal-default").modal('show');
        $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
          });
       
        $.post("/tampildata",
            {
            nama_dpn: depan,
            nama_usaha: usaha,
            kecamatan:kecamatan
            },
            function(data,html){
              $(".modal-body").html(data);
            });
        
    });
</script>

@endpush