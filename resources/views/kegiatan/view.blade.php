@extends('layouts.app')

@section('title', 'Detail Kegiatan')
@section('content')
@if (session('statustambah'))
    <div class="alert alert-success">
        {{ session('statustambah') }}
    </div>
@endif
<section class="content">
    <div class="form-horizontal">
		<div class="box box-default">
			<div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title"></h3>

					<div class="card-tools">
						<button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
						<i class="fas fa-minus"></i></button>
					</div>
                </div>
                <div class="card-body">
                    
                    <div class="row">
                        <div class="col-md-6">
                            <table class="table table-striped table-bordered table-hover ">
                                @foreach ($kegiatandata as $item)
                                <tr>
                                    <td>Nama Kegiatan </td>
                                    <td> :</td>
                                    <td align="left">{{$item->nama_kegiatan}}</td>
                                </tr>

                                <tr>
                                    <td>Deskripsi </td>
                                    <td> :</td>
                                    <td align="left">{{$item->deskripsi}}</td>
                                </tr>

                                <tr>
                                    <td>Jumlah Peserta </td>
                                    <td> :</td>
                                    <td align="left">{{$item->jumlah_peserta}}</td>
                                </tr>

                                <tr>
                                    <td>Tempat Kegiatan </td>
                                    <td> :</td>
                                    <td align="left">{{$item->tempat_kegiatan}}</td>
                                </tr>

                                <tr>
                                    <td>Tanggal Kegiatan </td>
                                    <td> :</td>
                                    <td align="left">{{$item->tanggal_kegiatan}}</td>
                                    
                                </tr>
                                @endforeach
                            </table>
                        </div>
                        <div class="col-md-6">
                            <form class="form-horizontal" method="POST" action="/addpeserta">
                                @csrf
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Masukan Anggota</label>
                                    <input type="hidden" class="form-control" id="id" name="id" value="{{$idkegiatan}}">
                                    <select name="anggota" id="anggota" class="form-control select2 selectpicker" style="width: 90%;">
                                    <option>-- PILIH ANGGOTA -- </option>
                                    @foreach ($member as $itemhasil)
                                        <option value="{{$itemhasil->id_anggota}}">{{$itemhasil->id_anggota}}| {{$itemhasil->nik}}| {{$itemhasil->nama_depan}} | {{$itemhasil->nama_usaha}}</option>  
                                    @endforeach
                                    
                                    
                                    </select>
                                </div>
                                <button type="submit" class="btn btn-info">Tambah</button>
                            </form> 
                        </div>
                        </div>
                        <div class="col-md-12">
                            <div class="box-body table-responsive no-padding">
                                <table id="example1" class="table responsive table-bordered table-hover">
                                    <thead>
                                   <tr>
                                     <th>#</th>
                                     <th>ID Anggota</th>
                                     <th>Nik</th>
                                     <th>Nama Anggota</th>
                                     
                                     <th>Nama Usaha</th>
                                    
                                     <th>Jenis Usaha</th>
                                     <th></th>
                                    
                                   </tr>
                                    </thead>
                                  <tbody>
                                        @foreach ($kegiatan as $hasil)
                                            
                                        
                                        <tr>
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{$hasil->id_anggota}}</td>
                                            <td>{{$hasil->nik}}</td>
                                            <td>{{$hasil->nama_depan}}</td>
                                            <td>{{$hasil->nama_usaha}}</td>
                                            <td>{{$hasil->jenis_usaha}}</td>
                                            <td align='center'>
                                             {{--  <a href="action.php?module=peserta&act=delete&id=<?php echo $data['id_kegiatan'];?>&id2=<?php echo $data['id_anggota'];?>" onclick="return confirm('Apakah Anda Yakin Akan Menghapus Data ini')"><i class="fa fa-trash"></i></a>  --}}
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                    </div> 
                    
					
            </div>
        </div>
    </div>
</section>

@endsection
@push('scripts')
<script>
    $(function () {
        $('.select2').select2()
        $('.select2bs4').select2({
            theme: 'bootstrap4'
          })
    })
</script>

@endpush
