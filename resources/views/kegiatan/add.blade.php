@extends('layouts.app')

@section('title', 'INPUT KEGIATAN')
@section('content')
<section class="content">
    <div class="form-horizontal">
		<div class="box box-default">
			<div class="card card-primary">
				<div class="card-header">
                    <h3 class="card-title"></h3>
                    <a href="/addkegiatan">
                    <button class="btn btn-warning">Tambah Kegiatan</button> </a>
					<div class="card-tools">
						<button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
						<i class="fas fa-minus"></i></button>
					</div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form class="form-horizontal" method="POST" action="/savekegiatan">
                                @csrf
                                <div class="form-group">
                                  <label for="inputEmail3" class="col-sm-2 control-label">Nama Kegiatan</label>

                                  <div class="col-sm-5">
                                  <input type="hidden" class="form-control" id="id_kegiatan" name="id_kegiatan">
                                    <input type="text" class="form-control" id="nama_kegiatan" name="nama_kegiatan" placeholder="Nama Kegiatan">
                                  </div>
                                </div>  

                                <div class="form-group">
                                  <label for="inputEmail3" class="col-sm-2 control-label">Deskripsi Kegiatan</label>

                                   <div class="col-sm-6">
                                        <textarea name="deskripsi" id="deskripsi" rows="4" cols="63" placeholder=" Deskripsi kegiatan"></textarea>
                                      </div>
                                </div>  

                                <div class="form-group">
                                  <label for="inputEmail3" class="col-sm-2 control-label">Jumlah Peserta</label>

                                  <div class="col-sm-3">
                                    <input type="text" class="form-control" id="jumlah_peserta" name="jumlah_peserta" placeholder="Jumlah Peserta">
                                  </div>
                                </div> 


                                <div class="form-group">
                                  <label for="inputEmail3" class="col-sm-2 control-label">Tempat Kegiatan</label>

                                  <div class="col-sm-5">
                                    <input type="text" class="form-control" id="tempat_kegiatan" name="tempat_kegiatan" placeholder="Tempat Kegiatan">
                                  </div>
                                </div>  

                                <div class="form-group">
                                  <label for="inputEmail3" class="col-sm-2 control-label">Tanggal Kegiatan</label>

                                  <div class="col-sm-5">
                                    <input type="text" class="form-control" id="datepicker3" name="tgl_kegiatan" placeholder="Jadwal Kegiatan"  data-date-format="dd-mm-yyyy">
                                  </div>
                                </div>  

                                <div class="box-footer">
                                    <button type="submit" class="btn btn-info">Simpan</button>
                                    {{--  //<a href="index.php?page=view-kegiatan"><button type="button" class="btn btn-default">Kembali</button></a>  --}}
                                </div>
                            <!-- /.box-footer -->

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('scripts')
<script>
    $(function () {
        $('.select2').select2()
        $('.select2bs4').select2({
            theme: 'bootstrap4'
          })
        
          //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservationdate').datetimepicker({
        format: 'YYYY'
	});

	$('#reservationdate1').datetimepicker({
        format: 'YYYY'
	});
	
	$('#datepicker3').datepicker({
      minViewMode: 2,
       format: 'yyyy',
       autoclose: true
    })
    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY hh:mm A'
      }
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Timepicker
    $('#timepicker').datetimepicker({
      format: 'LT'
    })
    
    //Bootstrap Duallistbox
    $('.duallistbox').bootstrapDualListbox()

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    $('.my-colorpicker2').on('colorpickerChange', function(event) {
      $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    });

    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });
    })
</script>

@endpush