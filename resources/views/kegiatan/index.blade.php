@extends('layouts.app')

@section('title', 'DAFTAR KEGIATAN')
@section('content')

<section class="content">
    <div class="form-horizontal">
		<div class="box box-default">
			<div class="card card-primary">
				<div class="card-header">
                    <h3 class="card-title"></h3>
                    <a href="/addkegiatan">
                    <button class="btn btn-warning">Tambah Kegiatan</button> </a>
					<div class="card-tools">
						<button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
						<i class="fas fa-minus"></i></button>
					</div>
                </div>
                <div class="card-body">
                    
					<div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <table id="example1" class="table responsive table-bordered table-hover">
                                    <thead>
                                       <tr>
                                         <th>#</th>
                                         <th>Nama Kegiatan</th>
                                         <th>Deskripsi</th>
                                         <th>Jumlah Peserta</th>
                                         
                                         <th>Tempat Kegiatan</th>
                                         <th>Tanggal Kegiatan</th>
                                         <th></th>
                                         
                                        
                                       </tr>
                                    </thead>
                                      <tbody>
                                            @foreach ($kegiatan as $hasil)
                                                <tr>
                                                    <th>{{$loop->iteration}}</th>
                                                    <th>{{$hasil->nama_kegiatan}}</th>
                                                    <th>{{$hasil->deskripsi}}</th>
                                                    <th>{{$hasil->jumlah_peserta}}</th>
                                                    <th>{{$hasil->tempat_kegiatan}}</th>
                                                    <th>{{$hasil->tanggal_kegiatan}}</th>
                                                    <th><a href="/kegiatan/{{$hasil->id_kegiatan}}" class="badge badge-info">detail</a></th>
                                                </tr>
                                            @endforeach
                                      </tbody>
                                     <tfoot>
                                       <tr>
                                         <th>#</th>
                                         <th>Nama Kegiatan</th>
                                         <th>Deskripsi</th>
                                         <th>Jumlah Peserta</th>
                                         
                                         <th>Tempat Kegiatan</th>
                                         <th>Tanggal Kegiatan</th>
                                         <th></th>
                                         
                                       </tr>
                                   </tfoot>
                               </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
