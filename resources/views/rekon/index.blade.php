@extends('layouts.app')

@section('title', 'CARI DATA MONITORING UMKM ')

@section('content')
@section('content')
<div class="container">
    <div class="card card-info">
        <div class="card-header">
          <h3 class="card-title">Jumlah Pendaftar Perkecamatan</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
              </button>
          </div>
        </div>
        <div class="card-body">
            <table id="binaan-table" class="table table-bordered" >
                <thead class="thead-drak">
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">Kecamatan</th>
                    <th scope="col">jumlah</th>
                    <th scope="col">Detail Per Kelurahan</th>
                    
                    </tr>
                </thead>
                <tbody>
                    @foreach($rekon as $result => $total)
                    <tr>
                    <th scope="row">{{$loop->iteration}}</th>
                    <td>{{$total->KELURAHAN}}</td>
                    {{-- <td>{{$total->jumlahnik}}</td> --}}
                    
                    <td>
                        <a href="/rekon/{{$total->KELURAHAN}}" class="badge badge-info">detail</a>
                    </td>
                    </tr>
                @endforeach 
               
                <tr>
                    <td></td>
                    <td> <strong>TOTAL</strong> </td>
                    {{-- <td> <strong>{{$grand}} </strong> </td> --}}
                    <td>  </td>
                </tr>
                
                </tbody>
                </table>
        </div>
        <div class="card-footer" >
            <h4>Updating data dilakukan setiap jam 00.00</h4>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

       

   
</div>

@endsection


@push('scripts')
<script type="text/javascript">
        $.noConflict();
        $(document).ready( function ($) {
            $('#binaan-table').dataTable();
        } );
    </script>
@endpush