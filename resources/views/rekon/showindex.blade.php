@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card card-info">
        <div class="card-header">
          {{-- <h3 class="card-title">MASTER DATA BANPRES TAHUN 2020</h3> --}}

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
              </button>
          </div>
        </div>
        <div class="card-body">
            
                <table id="binaan-table" class="display wrap" style="width:100%">
                    
                    
                    <thead>
                        
                        <tr> 
                            <th>NO</th> 
                            <th>NIK</th> 
                            <th>NAMA</th> 
                            <th>ALAMAT</th>
                            <th>Kelurahan</th>
                            <th>Kecamatan</th>
                            <th>NO_TELP</th>
                            
                             
                        </tr> 
                    </thead> 
                    <tbody> 
                        @foreach($rekon as $result => $total)
                        <tr> 
                            <td>{{$loop->iteration}}</td> 
                            <td>{{$total->NIK}}</td>
                            <td>{{$total->NAMA_LENGKAP}}</td>
                            <td>{{$total->ALAMAT_LENGKAP}}</td>
                            <td>{{$total->KELURAHAN}}</td>
                            <td>{{$total->KECAMATAN}}</td>
                            <td>{{$total->NO_TELP}}</td> 
                            
                        </tr> 
                        @endforeach
                    </tbody>
                </table>
        </div>
        <div class="card-footer" >
            <h4>Updating data dilakukan setiap jam 00.00</h4>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

       

   
</div>

@endsection


@push('scripts')
{{-- <script type="text/javascript">
    $(document).ready(function() {
        $('#binaan-table').DataTable( {
            dom: 'Bfrtip',
            buttons: [
            {
                'copy', 'csv', 'excel', 'pdf', 'print'
                //extend: 'print',
                //header: 'test',
                //messageTop: "<h3>DINAS KOPERASI USAHA MIKRO KECIL DAN MENENGAH KOTA BANDUNG</h3> <br> Kelurahan : {{$total->KELURAHAN}}  Kecamatan : {{$total->KECAMATAN}}"
            }
        ]
        } );
    } );
    
</script> --}}
<script type="text/javascript">
    $(document).ready(function() {
        $('#binaan-table').DataTable( {
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'excelHtml5',
                    title: '<?php echo($total->KELURAHAN); ?>'
                },
                {
                    extend:  'csvHtml5',
                    title: '<?php echo($total->KELURAHAN); ?>'
                }
            ]
        } );
    } );
</script>

@endpush


{{-- @extends('layouts.app')

@section('content')
    {{$dataTable->table()}}
@endsection

@push('scripts')
    {{$dataTable->scripts()}}
    
@endpush --}}