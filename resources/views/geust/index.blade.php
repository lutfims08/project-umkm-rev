@extends('layouts.app')

@section('title', 'Selamat Datang di Sistem Informasi Data UMKM Dinas KUMKM Kota Bandung')

@section('content')
<section class="content">
    <div class="card card-info">
        <div class="card-header">
          <h3 class="card-title">Pendaftaran Penerima Fasilitasi</h3>

          <div class="card-tools">
            {{--  <button type="button" class="btn btn-tool" data-card-widget="card-refresh" data-source="widgets.html" data-source-selector="#card-refresh-content" data-load-on-init="false"><i class="fas fa-sync-alt"></i></button>  --}}
            <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            {{--  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>  --}}
          </div>
          <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <div class="row">
                <div class="col-sm-6">
                    <div class="callout callout-success">
                    <form class="form-horizontal" id="quickForm" method="POST" action="/cekbinaan" > 
                    @csrf
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-2 col-form-label" for="nik" align="right">NIK</label>

                        <div class="col-sm-6">
                            
                            <input type="text" class="form-control @error('nik') is-invalid @enderror" id="nik" name="nik" placeholder="Isikan NIK" value="{{ old('nik') }}"> 
                            
                        </div>
                        
                    </div>
                    <span id="result"></span>
                        {{--  <button type="submit" class="btn btn-info"><i class="far fa-hand-point-right ">Lanjut</i></button>  --}}
                    </form>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="callout callout-info">
                        <h5>KETERANGAN :</h5>
      
                        <p> Pencarian NIK yang Terdaftar sebagai UMKM binaan. </p>
                        <p>Apabila NIK tidak ditemukan Silahkan Lanjutkan untuk Mendaftar UMKM Binaan</p>
                        
                      </div>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
      </div>
</section>

@endsection

@push('scripts')
<script>
	$(document).ready(function()
    {    
      $("#nik").keyup(function()
      {   
        var name = $(this).val(); 
        
        if(name.length > 0)
        {   
          $("#result").html('checking...');       
          $.ajax({
            headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            type : 'POST',
            url  : '/carinik1/' + name,
            
            success : function(data)
                  {
                       $("#result").html(data);
                    }
            });
            return false;
          
        }
        else
        {
          $("#result").html('');
        }
      });
      
    });
</script>
@endpush