@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-10">
      <h1 class="mt-3" >Data UMKM Terdaftar di Dinas KUMKM Kota Bandung</h1>

        <table id="binaan-table" class="table table-bordered" >
        <thead class="thead-drak">
            <tr>
            <th scope="col">#</th>
            <th scope="col">ID</th>
            <th scope="col">NIK</th>
            <th scope="col">Nama Pemilik</th>
            <th scope="col">Nama Usahan</th>
            <th scope="col">No HP/WA</th>
            <th scope="col">Alamat</th>
            <th scope="col">Kecamatan</th>
            <th scope="col">Aksi</th>
            </tr>
        </thead>
        <tbody>
        @foreach($list_binaan as $result => $hasil)
            <tr>
            <th scope="row">{{$loop->iteration}}</th>
            <td>{{$hasil->id_anggota}}</td>
            <td>{{$hasil->nik}}</td>
            <td>{{$hasil->nama_depan}}</td>
            <td>
           
            {{$hasil->nama_usaha}}</td>
           
            <td>{{$hasil->no_handphone}}</td>
            <td>{{$hasil->alamat}}</td>
            <td>{{$hasil->id_kecamatan}}</td>
            <td>
            
            </td>
            </tr>
        @endforeach 
        </tbody>
        </table>

    </div>
  </div>
</div>

@endsection


@push('scripts')
<script type="text/javascript">
        $.noConflict();
        $(document).ready( function ($) {
            $('#binaan-table').dataTable();
        } );
    </script>
@endpush
