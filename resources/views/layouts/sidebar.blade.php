@include('layouts.navbar')
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{url('/dashboard')}}" class="brand-link">
      <img src="../../dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">DINAS KUMKM</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      @guest
      <nav class="mt-2">
        {{--  <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-book-alt"></i>
              <p>
                DATA SEBARAN
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{url('/add')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>MAPING</p>
                </a>
              </li>
              
            </ul>
          </li>
        </ul>  --}}
        
      </nav>
      @else
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="../../dist/img/user1-128x128.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="/users/{{ Auth::user()->nik }}" class="d-block">{{ Auth::user()->name }}</a>
        
          <a href="/users/{{ Auth::user()->nik }}" class="d-block"><strong>{{ Auth::user()->role }}</strong></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        @if (auth()->user()->role == 'superadmin')
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
                with font-awesome or any other icon font library -->
            <li class="nav-item has-treeview">
              <a href="#" class="nav-link active">
                <i class="nav-icon fas fa-plus-square"></i>
                <p>
                  INPUT DATA UMKM
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="{{url('/add')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Input Data UMKM</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{url('/monitoring')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Monitoring</p>
                  </a>
                </li>
              </ul>
            </li>
            <li class="nav-item has-treeview">
              <a href="#" class="nav-link active">
                <i class="nav-icon fas fa-id-card-alt"></i> 
                <p>
                  SURAT KETERANGAN
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="{{url('/skumkm')}}" class="nav-link">
                    <i class="far fa-id-card"></i>
                    <p>Proses Keterangan</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{url('/skumkmview')}}" class="nav-link">
                    <i class="far fa-id-card"></i>
                    <p>Daftar Keterangan</p>
                  </a>
                </li>
              </ul>
            </li>
            <li class="nav-item has-treeview">
              <a href="#" class="nav-link active">
                <i class="nav-icon fas fa-book"></i>
                <p>
                  MASTER DATA
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="{{url('/binaans')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Data UMKM</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{url('/monev')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Monitoring Data UMKM</p>
                  </a>
                </li>
                {{--  <li class="nav-item">
                  <a href="{{url('/kegiatan')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Daftar Kegiatan</p>
                  </a>
                </li>  --}}
                <li class="nav-item">
                  <a href="{{url('/kegiatan')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Data Kegiatan</p>
                  </a>
                </li>
              </ul>
            </li>
            <li class="nav-item has-treeview">
              <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-newspaper"></i>
                <p>
                  LAPORAN DATA UMKM
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="{{url('/reportthn')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>per Tahun</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{url('/reportjenis')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Klasifikasi Usaha</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{url('/reportusaha')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Per Bidang Usaha</p>
                  </a>
                </li>
              </ul>
            </li>
            <li class="nav-item has-treeview">
              <a href="#" class="nav-link active">
                <i class="fas fa-money-check-alt"></i>
                <p>
                  BANPRES Tahap 1 Tahun 2020
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="{{url('/kecamatan')}}" class="nav-link">
                    <i class="fas fa-thumbtack"></i>
                    <p>Jumlah Per Kecamatan</p>
                  </a>
                </li>
                
                <li class="nav-item">
                  <a href="{{url('/caridatabansos')}}" class="nav-link">
                    <i class="fas fa-search-location"></i>
                    <p>Cari Data</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{url('/masterbansos')}}" class="nav-link">
                    <i class="fas fa-search-location"></i>
                    <p>Master Data BANPRES</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{url('/inputbansos')}}" class="nav-link">
                    <i class="fas fa-user-plus"></i>
                    <p>Input Data</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{url('/editbansos')}}" class="nav-link">
                    <i class="fas fa-user-plus"></i>
                    <p>Edit Data</p>
                  </a>
                </li>
                
              </ul>
            </li>
            <li class="nav-item has-treeview">
              <a href="#" class="nav-link active">
                <i class="fas fa-money-check-alt"></i>
                <p>
                  BANPRES Tahap 2 Tahun 2020
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="{{url('/kecamatantahap2')}}" class="nav-link">
                    <i class="fas fa-thumbtack"></i>
                    <p>Jumlah Per Kecamatan</p>
                  </a>
                </li>
                
                <li class="nav-item">
                  <a href="{{url('/masterbansostahap2')}}" class="nav-link">
                    <i class="fas fa-search-location"></i>
                    <p>Master Data BANPRES</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{url('/updatedata2')}}" class="nav-link">
                    <i class="fas fa-thumbtack"></i>
                    <p>Perbaikan Data</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{url('/validasibpum2')}}" class="nav-link">
                    <i class="fas fa-thumbtack"></i>
                    <p>Verifikasi Pendaftaran</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{url('/resenddata')}}" class="nav-link">
                    <i class="fas fa-thumbtack"></i>
                    <p>DATA PERBAIKAN </p>
                  </a>
                </li>
                
                
              </ul>
            </li>
            <li class="nav-item has-treeview">
              <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-map-marked-alt"></i>
                <p>
                  DATA SEBARAN
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="{{url('/datasebaran')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>MAPING</p>
                  </a>
            </li>
                
              </ul>
            </li>
            <li class="nav-item">
              <a href="{{url('/users')}}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Data User</p>
              </a>
            </li>  
          </ul>    
        @endif
        @if (auth()->user()->role == 'Bid_UMKM')
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
                with font-awesome or any other icon font library -->
            <li class="nav-item has-treeview">
              <a href="#" class="nav-link active">
                <i class="nav-icon fas fa-plus-square"></i>
                <p>
                  INPUT DATA UMKM
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="{{url('/add')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Input Data UMKM</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{url('/monitoring')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Monitoring</p>
                  </a>
                </li>
              </ul>
            </li>
            <li class="nav-item has-treeview">
              <a href="#" class="nav-link active">
                <i class="nav-icon fas fa-id-card-alt"></i> 
                <p>
                  SURAT KETERANGAN
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="{{url('/skumkm')}}" class="nav-link">
                    <i class="far fa-id-card"></i>
                    <p>Proses Keterangan</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{url('/skumkmview')}}" class="nav-link">
                    <i class="far fa-id-card"></i>
                    <p>Daftar Keterangan</p>
                  </a>
                </li>
              </ul>
            </li>
            <li class="nav-item has-treeview">
              <a href="#" class="nav-link active">
                <i class="nav-icon fas fa-book"></i>
                <p>
                  MASTER DATA
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="{{url('/binaans')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Data UMKM</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{url('/monev')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Monitoring Data UMKM</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{url('/kegiatan')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Data Kegiatan</p>
                  </a>
                </li>
              </ul>
            </li>
            <li class="nav-item has-treeview">
              <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-newspaper"></i>
                <p>
                  LAPORAN DATA UMKM
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="{{url('/reportthn')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>per Tahun</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{url('/reportjenis')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Klasifikasi Usaha</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{url('/reportusaha')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Per Bidang Usaha</p>
                  </a>
                </li>
              </ul>
            </li>
            <li class="nav-item has-treeview">
              <a href="#" class="nav-link active">
                <i class="fas fa-money-check-alt"></i>
                <p>
                  BANPRES Tahun 2020
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="{{url('/kecamatan')}}" class="nav-link">
                    <i class="fas fa-thumbtack"></i>
                    <p>Jumlah Per Kecamatan</p>
                  </a>
                </li>
                
                <li class="nav-item">
                  <a href="{{url('/caridatabansos')}}" class="nav-link">
                    <i class="fas fa-search-location"></i>
                    <p>Cari Data</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{url('/masterbansos')}}" class="nav-link">
                    <i class="fas fa-search-location"></i>
                    <p>Master Data BANPRES</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{url('/inputbansos')}}" class="nav-link">
                    <i class="fas fa-user-plus"></i>
                    <p>Input Data</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{url('/editbansos')}}" class="nav-link">
                    <i class="fas fa-user-plus"></i>
                    <p>Edit Data</p>
                  </a>
                </li>
                
              </ul>
            </li>
            <li class="nav-item has-treeview">
              <a href="#" class="nav-link active">
                <i class="fas fa-money-check-alt"></i>
                <p>
                  BANPRES Tahap 2 Tahun 2020
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="{{url('/kecamatantahap2')}}" class="nav-link">
                    <i class="fas fa-thumbtack"></i>
                    <p>Jumlah Per Kecamatan</p>
                  </a>
                </li>
                
                <li class="nav-item">
                  <a href="{{url('/masterbansostahap2')}}" class="nav-link">
                    <i class="fas fa-search-location"></i>
                    <p>Master Data BANPRES</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{url('/updatedata2')}}" class="nav-link">
                    <i class="fas fa-thumbtack"></i>
                    <p>Perbaikan Data</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{url('/validasibpum2')}}" class="nav-link">
                    <i class="fas fa-thumbtack"></i>
                    <p>Verifikasi Pendaftaran</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{url('/resenddata')}}" class="nav-link">
                    <i class="fas fa-thumbtack"></i>
                    <p>DATA PERBAIKAN </p>
                  </a>
                </li>
                
                
              </ul>
            </li>
            <li class="nav-item has-treeview">
              <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-map-marked-alt"></i>
                <p>
                  DATA SEBARAN
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="{{url('/datasebaran')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>MAPING</p>
                  </a>
                </li>
                
              </ul>
            </li>
            
          </ul>    
        @endif
        @if (auth()->user()->role == 'visitor')
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
                with font-awesome or any other icon font library -->
            <li class="nav-item has-treeview">
              <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-map-marked-alt"></i>
                <p>
                  DATA SEBARAN
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="{{url('/datasebaran')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>MAPING</p>
                  </a>
                </li>
              </ul>
            </li>
            
           
          </ul>    
        @endif
        @if (auth()->user()->role == 'surveyor')
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
                with font-awesome or any other icon font library -->
                <li class="nav-item has-treeview">
                  <a href="#" class="nav-link active">
                    <i class="nav-icon fas fa-plus-square"></i>
                    <p>
                      MONITORING
                      <i class="right fas fa-angle-left"></i>
                    </p>
                  </a>
                  <ul class="nav nav-treeview">
                    <li class="nav-item">
                      <a href="{{url('/monitoring')}}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Input Monitoring</p>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a href="{{url('/monev')}}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Monitoring Data UMKM</p>
                      </a>
                    </li>
                  </ul>
                </li>
            <li class="nav-item has-treeview">
              <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-map-marked-alt"></i>
                <p>
                  DATA SEBARAN
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="{{url('/datasebaran')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>MAPING</p>
                  </a>
                </li>
              </ul>
            </li>
             
           
          </ul>    
        @endif
        @if (auth()->user()->role == 'TimInputData')
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-item has-treeview">
              <a href="#" class="nav-link active">
                <i class="fas fa-money-check-alt"></i>
                <p>
                  BANPRES Tahun 2020
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="{{url('/kecamatan')}}" class="nav-link">
                    <i class="fas fa-thumbtack"></i>
                    <p>Jumlah Per Kecamatan</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{url('/caridatabansos')}}" class="nav-link" disabled>
                    <i class="fas fa-search-location"></i>
                    <p>Cari Data</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{url('/inputbansos')}}" class="nav-link">
                    <i class="fas fa-user-plus"></i>
                    <p>Input Data</p>
                  </a>
                </li>
                
                
              </ul>
            </li>
          </ul>
        @endif
        @if (auth()->user()->role == 'advis1')
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-item has-treeview">
              <a href="#" class="nav-link active">
                <i class="fas fa-money-check-alt"></i>
                <p>
                  BANPRES Tahap 2 Tahun 2020
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                
                <li class="nav-item">
                  <a href="{{url('/updatedata2')}}" class="nav-link">
                    <i class="fas fa-thumbtack"></i>
                    <p>Perbaikan Data</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{url('/validasibpum2')}}" class="nav-link">
                    <i class="fas fa-thumbtack"></i>
                    <p>Verifikasi Pendaftaran</p>
                  </a>
                </li> 
              </ul>
            </li>
          </ul>
        @endif

      </nav>
      <!-- /.sidebar-menu -->
      @endguest
    </div>
    <!-- /.sidebar -->
  </aside>