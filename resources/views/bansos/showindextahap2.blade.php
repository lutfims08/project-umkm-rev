@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card card-info">
        <div class="card-header">
          <h3 class="card-title">MASTER DATA BANPRES TAHUN 2020</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
              </button>
          </div>
        </div>
        <div class="card-body">
            {{-- <table id="binaan-table" class="table table-bordered" >
                <thead class="thead-drak">
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">NIK</th>
                    <th scope="col">NAMA</th>
                    <th scope="col">ALAMAT</th>
                    <th scope="col">KELURAHAN</th>
                    <th scope="col">KECAMATAN</th>
                    <th scope="col">NO TELP / HP</th>
                    <th scope="col">NAMA USAHA</th>
                    
                    <th scope="col">JENIS USAHA</th>
                    
                    
                    </tr>
                </thead>
                <tbody>
                    @foreach($totaldaftar as $result => $total)
                    <tr>
                    <th scope="row">{{$loop->iteration}}</th>
                    <td>{{$total->NIK}}</td>
                    <td>{{$total->NAMA_LENGKAP}}</td>
                    <td>{{$total->ALAMAT_LENGKAP}}</td>
                    <td>{{$total->KELURAHAN}}</td>
                    <td>{{$total->KECAMATAN}}</td>
                    <td>{{$total->NO_TELP}}</td>
                    <td>{{$total->NAMA_USAHA}}</td>
                    
                    <td>{{$total->JENIS_USAHA}}</td>
                    </tr>
                @endforeach 
               
                
                
                </tbody>
                </table> --}}
                <table id="binaan-table" class="display wrap" style="width:100%">
                    
                    
                    <thead>
                        
                        <tr> 
                            <th>NO</th> 
                            <th>NIK</th> 
                            <th>NAMA</th> 
                            <th>ALAMAT</th>
                            <th>JENIS USAHA</th>
                            <th>NO TELP / HP</th>
                            <th>NOMOR REGISTER</th>
                            
                             
                        </tr> 
                    </thead> 
                    <tbody> 
                        @foreach($totaldaftar as $result => $total)
                        <tr> 
                            <td>{{$loop->iteration}}</td> 
                            <td>{{$total->nik}}</td>
                            <td>{{$total->nama_lengkap}}</td>
                            <td>{{$total->alamat}}</td>
                            <td>{{$total->bidang_usaha}}</td>
                            <td>{{$total->no_telp}}</td> 
                            <td>{{$total->no_reg}}</td> 
                            
                        </tr> 
                        @endforeach
                    </tbody>
                </table>
        </div>
        <div class="card-footer" >
            <h4>Updating data dilakukan setiap jam 00.00</h4>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

       

   
</div>

@endsection


@push('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('#binaan-table').DataTable( {
            dom: 'Bfrtip',
            buttons: [
            {
                extend: 'print',
                header: 'test',
                messageTop: "<h3>DINAS KOPERASI USAHA MIKRO KECIL DAN MENENGAH KOTA BANDUNG</h3> <br> Kelurahan : {{$total->kelurahan}}  Kecamatan : {{$total->kecamatan}}"
            }
        ]
        } );
    } );
    </script>
@endpush


{{-- @extends('layouts.app')

@section('content')
    {{$dataTable->table()}}
@endsection

@push('scripts')
    {{$dataTable->scripts()}}
    
@endpush --}}