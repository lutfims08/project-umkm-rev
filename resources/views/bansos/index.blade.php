@extends('layouts.app')

@section('content')
<section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-6">
      
          <!-- PIE CHART -->
          <div class="card card-info">
            <div class="card-header">
              <h3 class="card-title">Data Grafik Pendaftar Bansos Usaha Mikro dan Ultra Mikro</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
              </div>
            </div>
            <div class="card-body">
              <canvas id="pieChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
            </div>
            <div class="card-footer" >
                
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <div class="col-md-6">
      
          <div class="card card-info">
            <div class="card-header">
              <h3 class="card-title">Perbandingan Data Input(Verifikasi) dan Data Import(Belum Verifikasi)</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
              </div>
            </div>
            <div class="card-body">
              <canvas id="pieChart1" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
            </div>
            <div class="card-footer" >
                
            </div>
            <!-- /.card-body -->
          </div>

          
        </div>

      </div> 
      <div class="card card-success">
        <div class="card-header">
          <h3 class="card-title">KONDISI DATA IMPORT DARI SELURUH KELURAHAN</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
          </div>
        </div>
        <div class="card-body">
          <h6 class="card-title">Sumber Data Berasal dari 118 kelurahan se Kota Bandung</h6>
          <br>
          <hr>
          <br>

          <div class="chart">
            <canvas id="barChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
          </div>
        </div>
        <!-- /.card-body -->
      </div> 
      <div class="card card-success">
        <div class="card-header">
          <h3 class="card-title">KONDISI DATA TERVERIFIKASI</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
          </div>
        </div>
        <div class="card-body">
          <h6 class="card-title">Sumber Data Berasal seluruh media pendaftaran</h6>
          <br>
          <hr>
          <br>

          <div class="chart">
            <canvas id="barChart1" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
          </div>
        </div>
        <!-- /.card-body -->
      </div> 
    </div><!-- /.container-fluid -->
  </section>

@endsection


@push('scripts')
<script>
    $(function () {
        
        //-------------
        //- DONUT CHART -
        //-------------
        // Get context with jQuery - using jQuery's .get() method.
        //var donutChartCanvas = $('#donutChart').get(0).getContext('2d')
        var donutData        = {
          labels: [
              'Jumlah Pendaftar', 
              'Data Belum Terverifikasi',
              
              'Pendaftar diterima',
              'Pendaftar ditolak',
              
               
               
          ],
          datasets: [
            {
              //data: [50400, <?php foreach($totaldaftar  as $dataT) { echo '"' . $dataT->jumlahnik . '",';} ?> , <?php foreach($totalbandung as $dataT) { echo '"' . $dataT->jumlahnik . '",';} ?> , <?php foreach($totalluar as $dataT) { echo '"' . $dataT->jumlahnik . '",';} ?>  ],
              data: [ '130000' , <?php foreach($totalver  as $dataT) { echo '"' . $dataT->jumlahnik . '"';} ?>,  <?php foreach($totalbandung as $dataT) { echo '"' . $dataT->jumlahnik . '"';} ?> , <?php foreach($totalluar as $dataT) { echo '"' . $dataT->jumlahnik . '"';} ?>],
              backgroundColor : ['#d2d6de', '#FFFF00','#00FF00', 	'#FF0000'],
            }
          ]
        }
        
        var donutOptions     = {
          maintainAspectRatio : false,
          responsive : true,
        }
        
        //Create pie or douhnut chart
        // You can switch between pie and douhnut using the method below.
       
    
        //-------------
        //- PIE CHART -
        //-------------
        // Get context with jQuery - using jQuery's .get() method.
        
        var pieChartCanvas = $('#pieChart').get(0).getContext('2d')
        var pieData        = donutData;
        var pieOptions     = {
          maintainAspectRatio : false,
          responsive : true,
        }
        //Create pie or douhnut chart
        // You can switch between pie and douhnut using the method below.
        var pieChart = new Chart(pieChartCanvas, {
          type: 'pie',
          data: pieData,
          options: pieOptions      
        })
        
        

        var donutData1        = {
          labels: [
              'TERVERIFIKASI', 
              'BELUM TERVERIFIKASI',
              
               
               
          ],
          datasets: [
            {
              //data: [50400, <?php foreach($totaldaftar  as $dataT) { echo '"' . $dataT->jumlahnik . '",';} ?> , <?php foreach($totalbandung as $dataT) { echo '"' . $dataT->jumlahnik . '",';} ?> , <?php foreach($totalluar as $dataT) { echo '"' . $dataT->jumlahnik . '",';} ?>  ],
              data: [<?php foreach($totalbandung as $dataT) { echo '"' . $dataT->jumlahnik . '"';} ?>, <?php foreach($totalver  as $dataT) { echo '"' . $dataT->jumlahnik . '"';} ?>],
              backgroundColor : ['#00FF00', 	'#FF0000'],
            }
          ]
        }

        var donutOptions1     = {
          maintainAspectRatio : false,
          responsive : true,
        }

        var pieChartCanvas1 = $('#pieChart1').get(0).getContext('2d')
        var pieData1        = donutData1;
        var pieOptions1     = {
          maintainAspectRatio : false,
          responsive : true,
        }

        var pieChart1 = new Chart(pieChartCanvas1, {
          type: 'pie',
          data: pieData1,
          options: pieOptions1      
        })

        //-------------
        //- BAR CHART -
        //-------------
       
        var areaChartData = { 
          labels  : ['NIK','NAMA', 'ALAMAT', 'BIDANG_USAHA' , 'NO_TELP',],
          datasets: [
            {
              label               : 'TERSEDIA',
              backgroundColor     : 'rgba(60,141,188,0.9)',
              borderColor         : 'rgba(60,141,188,0.8)',
              pointRadius          : false,
              pointColor          : '#3b8bba',
              pointStrokeColor    : 'rgba(60,141,188,1)',
              pointHighlightFill  : '#fff',
              pointHighlightStroke: 'rgba(60,141,188,1)',
              data                : [<?php foreach($total as $dataT) { echo '"' . $dataT->jumlahnik . '"';} ?>,
              <?php foreach($total as $dataT) { echo '"' . $dataT->jumlahnama . '"';} ?>,
              <?php foreach($total as $dataT) { echo '"' . $dataT->jumlahalamat . '"';} ?>,
              <?php foreach($total as $dataT) { echo '"' . $dataT->jumlahjenis . '"';} ?>,
              <?php foreach($total as $dataT) { echo '"' . $dataT->jumlahhp . '"';} ?>]
            },
            {
              label               : 'TIDAK TERSEDIA',
              backgroundColor     : 'rgba(210, 214, 222, 1)',
              borderColor         : 'rgba(210, 214, 222, 1)',
              pointRadius         : false,
              pointColor          : 'rgba(210, 214, 222, 1)',
              pointStrokeColor    : '#c1c7d1',
              pointHighlightFill  : '#fff',
              pointHighlightStroke: 'rgba(220,220,220,1)',
              data                : [ <?php echo '"' . $niknull . '"'; ?>,<?php echo '"' . $namanull . '"'; ?>, <?php echo '"' . $alamatnull . '"'; ?>, <?php echo '"' . $jenisnull . '"'; ?>, <?php echo '"' . $hpnull . '"'; ?>]
            },
          ]
        }

        var barChartCanvas = $('#barChart').get(0).getContext('2d')
        var barChartData = jQuery.extend(true, {}, areaChartData)
        var temp0 = areaChartData.datasets[0]
        var temp1 = areaChartData.datasets[1]
        barChartData.datasets[0] = temp1
        barChartData.datasets[1] = temp0
        var barChartOptions = {
          responsive              : true,
          maintainAspectRatio     : false,
          datasetFill             : false
        }
    
        var barChart = new Chart(barChartCanvas, {
          type: 'bar', 
          data: barChartData,
          options: barChartOptions
        })
       

        var areaChartData1 = { 
          labels  : ['NIK','NAMA', 'ALAMAT', 'BIDANG_USAHA' , 'NO_TELP',],
          datasets: [
            {
              label               : 'TERSEDIA',
              backgroundColor     : 'rgba(60,141,188,0.9)',
              borderColor         : 'rgba(60,141,188,0.8)',
              pointRadius          : false,
              pointColor          : '#3b8bba',
              pointStrokeColor    : 'rgba(60,141,188,1)',
              pointHighlightFill  : '#fff',
              pointHighlightStroke: 'rgba(60,141,188,1)',
              data                : [<?php foreach($totalya as $dataT) { echo '"' . $dataT->jumlahnik . '"';} ?>,
              <?php foreach($totalya as $dataT) { echo '"' . $dataT->jumlahnama . '"';} ?>,
              <?php foreach($totalya as $dataT) { echo '"' . $dataT->jumlahalamat . '"';} ?>,
              <?php foreach($totalya as $dataT) { echo '"' . $dataT->jumlahjenis . '"';} ?>,
              <?php foreach($totalya as $dataT) { echo '"' . $dataT->jumlahhp . '"';} ?>]
            },
            {
              label               : 'TIDAK TERSEDIA',
              backgroundColor     : 'rgba(210, 214, 222, 1)',
              borderColor         : 'rgba(210, 214, 222, 1)',
              pointRadius         : false,
              pointColor          : 'rgba(210, 214, 222, 1)',
              pointStrokeColor    : '#c1c7d1',
              pointHighlightFill  : '#fff',
              pointHighlightStroke: 'rgba(220,220,220,1)',
              data                : [ <?php echo '"' . $niknull1 . '"'; ?>,<?php echo '"' . $namanull1 . '"'; ?>, <?php echo '"' . $alamatnull1 . '"'; ?>, <?php echo '"' . $jenisnull1 . '"'; ?>, <?php echo '"' . $hpnull1 . '"'; ?>]
            },
          ]
        }
        var barChartCanvas1 = $('#barChart1').get(0).getContext('2d')
        var barChartData1 = jQuery.extend(true, {}, areaChartData1)
        var temp2 = areaChartData1.datasets[0]
        var temp3 = areaChartData1.datasets[1]
        barChartData1.datasets[0] = temp2
        barChartData1.datasets[1] = temp3
        var barChartOptions1 = {
          responsive              : true,
          maintainAspectRatio     : false,
          datasetFill             : false
        }
    
        var barChart1 = new Chart(barChartCanvas1, {
          type: 'bar', 
          data: barChartData1,
          options: barChartOptions1
        })
    
        })
  </script>
@endpush
