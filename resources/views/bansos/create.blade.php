@extends('layouts.app')

@section('content')
@if (session('statustambah'))
    <div class="alert alert-success">
        {{ session('statustambah') }}
    </div>
@endif
<div class="container">
    <div class="card card-info">
        <div class="card-header">
          <h3 class="card-title">INPUT DATA BANPRES TAHUN 2020</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
              </button>
          </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label" for="nik1" align="right">NIK</label>

                            <div class="col-sm-6">
                                
                                <input type="text" class="form-control @error('nik1') is-invalid @enderror" id="nik1" name="nik1" placeholder="Isikan NIK" value="{{ old('nik1') }} "> 
                                @error('nik1')
                                <div class="invalid-tooltip">
                                    {{ $message }}
                                </div>
                                
                                @enderror
                            </div>
                            <span id="result"></span>
                           

                        </div>
                        
                        
                </div>
                <div class="col-md-6">
                    <div class="callout callout-warning">
                      <h3>SEDANG DALAM PERBAIKAN</h3>
                      <h4>Dimohon Tetap Melanjutkan Menginput</h4>
                        <h5>Tata Cara Penginputan :</h5>      
                        
                        <ol>
                            <li>Inputkan NIK terlebih dahulu</li>
                            <li>Apabila Diinputkan NIK, Data Nama dan lain-lain masih kosong silahkan melengkapi Data yang kosong sesuai Surat Pernyataan</li>
                            <li>Apabila terdapat kesalahan dalam penulisan NIK maka akan ada peringatan dan lakukan penginputan ulang</li>
                        </ol>              
                        
                    </div>
                    <div class="callout callout-info">
                        {{--  <img src="../../dist/img/20200827_114722.gif" alt="AdminLTE Logo" width="200">               --}}
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer" >
            
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

       

   
</div>
@endsection


@push('scripts')
<script type="text/javascript">
		
    $("#kecamatan").on('change',function() {
        if ($("#kecamatan").val() != ""){
          var kecamatan = $("#kecamatan").val();
         
          $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "POST",
            url: "/carikelbansos/" +kecamatan,
            
            data: {
                id:kecamatan
                },    
           
            success: function(html) {
              $("#cari_kelurahan").html(html);
            }
          });
        }
      });

</script>
<script>
	$(document).ready(function()
    {    
      $("#nik1").keyup(function()
      {   
        var name = $(this).val(); 
        
        if(name.length > 0)
        {   
          $("#result").html('checking...');       
          $.ajax({
            headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            type : 'POST',
            url  : '/carinik/' + name,
            
            success : function(data)
                  {
                       $("#result").html(data);
                    }
            });
            return false;
          
        }
        else
        {
          $("#result").html('');
        }
      });
      
    });
</script>

@endpush