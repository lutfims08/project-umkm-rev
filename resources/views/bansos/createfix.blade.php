@extends('layouts.app')

@section('content')
@if (session('statustambah'))
    <div class="alert alert-success">
        {{ session('statustambah') }}
    </div>
@endif
<div class="container">
    <div class="card card-info">
        <div class="card-header">
          <h3 class="card-title">INPUT DATA BANPRES TAHUN 2020</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
              </button>
          </div>
        </div>
        <div class="card-body">
            
               
                    <div class="callout callout-danger">
                        <h5>PROSES PENGINPUTAN DATA TELAH SELESAI.</h5>      
                        
                        <ol>
                           <p>Kami mengucapkan terimakasih atas partisipasinya, semoga segala perbuatan Allah SWT membalas berlipat ganda Amin Ya Rob.</p>
                        </ol>              
                        
                    </div>
                    
                
           
        </div>
        <div class="card-footer" >
            
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

       

   
</div>
@endsection


@push('scripts')
<script>
   
	$(document).ready(function()
    {    
      $("#nik").change(function()
      {   
        var name = $(this).val() 
        
        if(name.length > 0)
        {   
          $("#result").html('checking...');       
          $.ajax({
            headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            type : 'POST',
            url  : '/carinik12/' + name,
            data : 'json',
            
            success : function(data)
                  {
                    
                    if(data.NAMA_LENGKAP == null){
                        
                        $("#result").html("SILAHKAN INPUTKAN DATA");
                        
                    }
                    else{
                        $("#result").html("LENGKAPI DATA");
                        document.getElementById("nama_dpn").value = data.NAMA_LENGKAP;
                        
                        document.getElementById("alamat").value = data.ALAMAT_LENGKAP;
                        document.getElementById("kecamatan").value = data.KECAMATAN;
                        document.getElementById("cari_kelurahan").value = data.KELURAHAN;
                        document.getElementById("hp").value = data.NO_TELP;
                        document.getElementById("pendidikan").value = data.JENIS_USAHA;
                        
                    }
                    }
            });
            return false;
          
        }
        else
        {
            
            document.getElementById("nama_dpn").value = "";
                        
            document.getElementById("alamat").value = "";
            document.getElementById("kecamatan").value = "";
            document.getElementById("cari_kelurahan").value = "";
            document.getElementById("hp").value = "";
            document.getElementById("pendidikan").value = "";
            $("#result").html("INPUTKAN NIK");
        }
              
      });
      
    });

</script>
<script type="text/javascript">
		
    $("#cari_kelurahan").change(function() {
        if ($("#cari_kelurahan").val() != ""){
          var kecamatan = $("#cari_kelurahan").val();
         
          $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "POST",
            url: "/carikelbansos/" +kecamatan,
            
            data: {
                id:kecamatan
                },    
           
            success: function(data) {
                document.getElementById("kecamatan").value = data.KECAMATAN;
            }
          });
        }
      });

</script>


@endpush