@extends('layouts.app')

@section('content')
@if (session('statustambah'))
    <div class="alert alert-success">
        {{ session('statustambah') }}
    </div>
@endif
<div class="container">
    <div class="card card-info">
        <div class="card-header">
          <h3 class="card-title">INPUT DATA BANPRES TAHUN 2020</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
              </button>
          </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    
                    <form class="form-horizontal" action="/createbansos" method="post">
                        <div class="form-group row ">
                            <label for="inputEmail3" class="col-sm-3 col-form-label" align="right">NIK</label>
                            <div class="col-sm-8">
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control @error('nik') is-invalid @enderror"  id="nik" name="nik" placeholder="Input NIK " aria-label="Inputkan NIK " aria-describedby="basic-addon2" value="{{ old('nik') }}">
                                    
                                    
                                  </div>
                                  @error('nik')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                  <span id="result"></span>
                                {{--  <input type="text" class="form-control @error('nik') is-invalid @enderror"  id="nik" name="nik" placeholder="Inputkan NIK" ><span id="result"></span>  --}}
                            </div>	
                        </div>                     
                        <div class="form-group row ">
                            <label for="inputEmail3" class="col-sm-3 col-form-label" align="right">Nama Lengkap</label>
                            <div class="col-sm-8">
                            
                                <input type="text" class="form-control @error('nama_dpn') is-invalid @enderror"  id="nama_dpn" name="nama_dpn"  placeholder="Isikan Nama Lengkap" value="{{ old('nama_dpn') }}"  ><span id="nama_dpn"></span>
                                
                                @error('nama_dpn')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            	
                        </div>
                        
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label" align="right">Alamat</label>

                            <div class="col-sm-8">
                                <textarea name="alamat" id="alamat" class="@error('alamat') is-invalid @enderror" rows="4" cols="45" placeholder=" Isikan Alamat Lengkap"  >{{ old('alamat') }}</textarea>
                                @error('alamat')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label" align="right">Kelurahan</label>

                            <div class="col-sm-6">
                            <select class="form-control select2 @error('kelurahan') is-invalid @enderror" id="cari_kelurahan" name="kelurahan" style="width: 100%;" >
                                <option selected disabled value="" >-- Pilih Kelurahan --</option>
                                <?php
                                $kec = DB::table('db_kelurahan_1')
                                
                                ->orderBy('kelurahan')
                                ->get();
                                foreach($kec as $k1){
                                    ?>
                                    <option  <?php 
                                        if (old('kelurahan') == $k1->kelurahan){
                                        echo "selected";	
                                        }
                                        else{
                                        }
                                        ?> 
                                    value = "<?php echo $k1->kelurahan?>"><?php echo $k1->kelurahan; ?>
                                    </option>
                                   <?php } ?>
                                
                                </select>
                                @error('kelurahan')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            	
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label" align="right">Kecamatan</label>

                            <div class="col-sm-6">
                                <input type="text" class="form-control @error('kecamatan') is-invalid @enderror"  id="kecamatan" name="kecamatan" placeholder="Input Kecamatan " aria-label="Inputkan Kecamatan " aria-describedby="basic-addon2" value="{{ old('kecamatan') }}" readonly>
                                @error('kecamatan')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            
                        </div>
                        
                        

                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label" align="right">No. Telp / Hp</label>

                            <div class="col-sm-8">
                                <input type="text" class="form-control @error('hp') is-invalid @enderror" data-inputmask="'mask': ['9999-9999-9999']" data-mask
                                id="hp" name="hp" placeholder="No. Handphone / Telepon" value="{{ old('hp') }}" >
                                @error('hp')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                
                            </div>
                            
                        </div>
                        
                        
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label" align="right"> JENIS USAHA</label>
                                <div class="col-sm-3 icheck-success d-inline">
                                    <input type="radio" name="pendidikan" id="pendidikan1"  value="KULINER"
                                        <?php if(old('pendidikan')=="KULINER"){ echo "checked";}?>>	
                                        <label  for="pendidikan1">KULINER</label>
                                </div>
                                
                                <div class="col-sm-3 icheck-success d-inline">
                                    <input type="radio" name="pendidikan" id="pendidikan2" class="col-sm-40 d-inline" value="FASHION"
                                        <?php if(old('pendidikan')=="FASHION"){ echo "checked";}?>>
                                        <label for="pendidikan2">FASHION</label>
                                </div>
                                <div class="col-sm-3 icheck-success d-inline">
                                    <input type="radio" name="pendidikan" id="pendidikan3" class="col-sm-40 d-inline" value="CRAFT"
                                        <?php if(old('pendidikan')=="CRAFT"){ echo "checked";}?>>
                                        <label for="pendidikan3">CRAFT</label>
                                </div>
                                <label for="inputEmail3" class="col-sm-3 col-form-label" align="right"></label>
                                <div class="col-sm-3 icheck-success d-inline">
                                    <input type="radio" name="pendidikan" id="pendidikan4" class="col-sm-40 d-inline" value="JASA"
                                        <?php if(old('pendidikan')=="JASA"){ echo "checked";}?>>
                                        <label for="pendidikan4">JASA</label>
                                </div>
                                <div class="col-sm-4 icheck-success d-inline">
                                    <input type="radio" name="pendidikan" id="pendidikan5" class="col-sm-40 d-inline" value="PERDAGANGAN"
                                        <?php if(old('pendidikan')=="PERDAGANGAN"){ echo "checked";}?>>
                                        <label for="pendidikan5">PERDAGANGAN</label>
                                </div>
                                <label for="inputEmail3" class="col-sm-3 col-form-label" align="right"></label>
                                <div class="col-sm-3 icheck-success d-inline">
                                    <input type="radio" name="pendidikan" id="pendidikan6" class="col-sm-40 d-inline" value="ARGOBISNIS"
                                        <?php if(old('pendidikan')=="ARGOBISNIS"){ echo "checked";}?>>
                                        <label for="pendidikan6">AGROBISNIS</label>
                                </div>
                                @error('pendidikan')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror       
                        </div>
                        <div class="card-footer" >
                            <button type="submit" class="btn btn-info float-right swalDefaultSuccess"><i class="fa fa-save">  SIMPAN</i></button>
                        </div>
                    
                    </form>       
                </div>
                <div class="col-md-6">
                    <div class="callout callout-warning">
                        <h5>Tata Cara Penginputan :</h5>      
                        
                        <ol>
                            <li>Inputkan NIK terlebih dahulu</li>
                            <li>Apabila Diinputkan NIK, Data Nama dan lain-lain masih kosong silahkan melengkapi Data yang kosong sesuai Surat Pernyataan</li>
                            <li>Apabila terdapat kesalahan dalam penulisan NIK maka akan ada peringatan dan lakukan penginputan ulang</li>
                        </ol>              
                        
                    </div>
                    <div class="callout callout-info">
                        {{--  <img src="../../dist/img/20200827_114722.gif" alt="AdminLTE Logo" width="200">               --}}
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer" >
            
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

       

   
</div>
@endsection


@push('scripts')
<script>
   
	$(document).ready(function()
    {    
      $("#nik").change(function()
      {   
        var name = $(this).val() 
        
        if(name.length > 0)
        {   
          $("#result").html('checking...');       
          $.ajax({
            headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            type : 'POST',
            url  : '/carinik12/' + name,
            data : 'json',
            
            success : function(data)
                  {
                    
                    if(data.NAMA_LENGKAP == null){
                        
                        $("#result").html("SILAHKAN INPUTKAN DATA");
                        
                    }
                    else{
                        $("#result").html("LENGKAPI DATA");
                        document.getElementById("nama_dpn").value = data.NAMA_LENGKAP;
                        
                        document.getElementById("alamat").value = data.ALAMAT_LENGKAP;
                        document.getElementById("kecamatan").value = data.KECAMATAN;
                        document.getElementById("cari_kelurahan").value = data.KELURAHAN;
                        document.getElementById("hp").value = data.NO_TELP;
                        document.getElementById("pendidikan").value = data.JENIS_USAHA;
                        
                    }
                    }
            });
            return false;
          
        }
        else
        {
            
            document.getElementById("nama_dpn").value = "";
                        
            document.getElementById("alamat").value = "";
            document.getElementById("kecamatan").value = "";
            document.getElementById("cari_kelurahan").value = "";
            document.getElementById("hp").value = "";
            document.getElementById("pendidikan").value = "";
            $("#result").html("INPUTKAN NIK");
        }
              
      });
      
    });

</script>
<script type="text/javascript">
		
    $("#cari_kelurahan").change(function() {
        if ($("#cari_kelurahan").val() != ""){
          var kecamatan = $("#cari_kelurahan").val();
         
          $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "POST",
            url: "/carikelbansos/" +kecamatan,
            
            data: {
                id:kecamatan
                },    
           
            success: function(data) {
                document.getElementById("kecamatan").value = data.KECAMATAN;
            }
          });
        }
      });

</script>


@endpush