@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card card-info">
        <div class="card-header">
          <h3 class="card-title">Pencarian Pendaftaran</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
              </button>
          </div>
        </div>
        <div class="card-body">
            <div class="callout callout-info">
                <h5>Hal yang harus diperhatikan :</h5>      
                
                <ol>
                    <li>Isikan NIK</li>
                    <li>Isikan Nama Berdasarkan NIK</li>
                    
                </ol>              
                
              </div>
              <div class="form-group row ">
                <label for="inputEmail3" class="col-sm-6 col-form-label" align="center">Bisa diinputkan Nama / NIK  dan / Kedua nya</label>
              </div>
            <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label" for="nik" align="right">NIK</label>

                <div class="col-sm-3">
                    
                    <input type="text" class="form-control @error('nik') is-invalid @enderror" id="nik" name="nik" placeholder="Isikan NIK" value="{{ old('nik') }}"> <span id="result"></span>
                    <div class="invalid-tooltip">
                        Silihkan Isikan NIK
                    </div>
                </div>
            </div>
            
            <div class="form-group row ">
                <label for="inputEmail3" class="col-sm-2 col-form-label" align="right">Nama Pemilik</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control @error('nama_dpn') is-invalid @enderror"  id="nama_dpn" name="nama_dpn"  placeholder="Nama Depan" value="{{ old('nama_dpn') }}">
                    <div class="invalid-tooltip">
                        Silihkan Isikan Nama Pemilik
                    </div>
                </div>	
            </div>
            <div class="card-footer" >
                <a href="/bansos" ><button type="button" name="back3" id="back3" class="btn btn-default  d-inline"><i class="far fa-hand-point-left">  Kembali</i></button></a>
                {{--  <button type="submit" class="btn btn-info swalDefaultSuccess d-inline"><i class="fa fa-save">  Simpan</i></button>  --}}
                <button type="button" id="caridata" class="btn btn-primary detail" data-toggle="modal" data-target="#modal-default"><i class="fas fa-search">  Cari</i></button>
            </div>
        </div>
        <div class="card-footer" >
            <h4>Updating data dilakukan setiap jam 00.00</h4>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

       

   
</div>
<div class="modal fade" id="modal-default">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Data Pendaftar Bansos Modal Usaha Produktif</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          
          
        </div>
        
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
@endsection


@push('scripts')
<script type="text/javascript">
	

    $(document).on('click','.detail',function(e){
      
      var depan = $("#nama_dpn").val();
      var usaha = $("#nik").val();
      e.preventDefault();
      $("#modal-default").modal('show');
      $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
        });
     
      $.post("/caridatabansos1",
          {
          nama_dpn: depan,
          nik: usaha
          },
          function(data,html){
            $(".modal-body").html(data);
          });
      
  });
</script>
@endpush
