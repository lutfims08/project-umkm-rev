@extends('layouts.app')

@section('content')

    {{$dataTable->table()}}
@endsection

@push('scripts')
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css">
<script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
<script src="/vendor/datatables/buttons.server-side.js"></script>
{{$dataTable->scripts()}}
{{--  <script type="text/javascript">
$(function() {
    $('#bansos-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '/ms'
    });
});
</script  --}}
    {{--  {{$dataTable->scripts()}}  --}}

{{--  <script type="text/javascript">
    $(document).ready(function() {
        $('#binaan-table').DataTable( {
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv','pdf', 'print'
            ]
        } );
    } );
    </script>  --}}
@endpush


{{-- @extends('layouts.app')

@section('content')
    {{$dataTable->table()}}
@endsection

@push('scripts')
    {{$dataTable->scripts()}}
    
@endpush --}}