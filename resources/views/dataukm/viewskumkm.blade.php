@extends('layouts.app')

@section('title', 'HASIL MONITORING DATA UMKM KOTA BANDUNG')

@section('content')
@if (session('statustambah'))
    <div class="alert alert-success">
        {{ session('statustambah') }}
    </div>
@endif
<section class="content">
      <div class="container-fluid">
        <div class="card card-default">
            <div class="card-header">
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
              </div>
            </div>
            <div class="card-body">
              <table id="viewukm1" class="table table-bordered table-striped">
                <thead class="thead-drak">
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">ID Anggota</th>
                    <th scope="col">NIK</th>
                    <th scope="col">NAMA PEMILIK</th>
                    <th scope="col">NAMA USAHA</th>
                    <th scope="col">TANGGAL PERMOHONAN</th>
                    <th scope="col">TANGGAL PEMBUATAN</th>
                    <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($v1 as $m)
                    <tr>
                    <th scope="row">{{$loop->iteration}}</th>
                    <td>{{$m->id_anggota}}</td>
                    <td>{{$m->nik}}</td>
                    <td>{{$m->nama_depan}}</td>
                    <td>{{$m->nama_usaha}}</td>
                    <td>{{$m->tgl_permohonan}}</td>
                    <td>{{$m->tgl_pembuatan}}</td>
                    <td>
                    <a href="/printsk/{{$m->id_anggota}}" class="badge badge-info">PRINT</a>
                    @if ((auth()->user()->role == 'superadmin') or (auth()->user()->role == 'Bid_UMKM'))
                    <button type="button" id="caridata" class="btn btn-primary detail" data-toggle="modal" data-id="<?php echo $m->id_anggota;?>" data-target="#modal-default">Proses</button>
                    <form action="/skumkmview/{{$m->id_anggota}}" method="POST">
                      @method('delete')
                      @csrf
                          
                          <button type="submit" class="btn btn-block btn-danger" onclick="return confirm('Apakah Anda Yakin Akan Menghapus Data ini')">
                          <i class="fas fa-trash-alt">
                              DELETE
                              </i>
                          </button>
                      </form>
                    @else
                    @endif
                    </td>
                    </tr>
                @endforeach    
                </tbody>
                </table>
            </div>
        </div>
      </div>
      <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Data Surat Keterangan</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              
              
            </div>
            
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
</section>
        
@endsection

@push('scripts')

<script>
  $(function () {
  
    $('#viewukm1').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true
    });
  });
  
  </script>

  <script type="text/javascript">
  

    $(document).on('click','.detail',function(e){
      var nik = $("#nik").val();
      var depan = $("#nama_dpn").val();
      var usaha = $("#nama_usaha").val();
      var permohonan = $("#tgl_permohonan").val();
      var pembuatan = $("#tgl_pembuatan").val();
      e.preventDefault();
      $("#modal-default").modal('show');
      $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
        });
     
      $.post("/skdata1",
          {
            id:$(this).attr('data-id'),
          },
          function(data,html){
            $(".modal-body").html(data);
          });
      
  });
</script>
@endpush