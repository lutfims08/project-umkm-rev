
<section class="content">
    <div class="container-fluid">
    @foreach($v1 as $result)
        <div class="row">
            <div class="col-md-3">
                <div class="card card-primary card-outline">
                    <?php
                    if(($result->aktifasi) =='Aktif'){ ?>
                        <div class="ribbon-wrapper ribbon-lg">
                            <div class="ribbon bg-success text-lg">
                            {{$result->aktifasi}}
                            </div>
                        </div>

                    <?php } else{ ?>
                        <div class="ribbon-wrapper ribbon-lg">
                            <div class="ribbon bg-danger text-lg">
                            {{$result->aktifasi}}
                            </div>
                        </div>
                    <?php } ?>
                    
                    <div class="card-body box-profile">
                        <div class="text-center">
                        <img class="profile-user-img img-fluid img-circle"
                            src="../../dist/img/user4-128x128.jpg"
                            alt="User profile picture">
                        </div>

                        <h3 class="profile-username text-center">{{$result->nama_depan}}</h3>

                        <p class="text-muted text-center">{{$result->email}}</p>

                        <ul class="list-group list-group-unbordered mb-3">
                        <li class="list-group-item">
                            <b>NIK</b> <a class="float-right">{{$result->nik}}</a>
                        </li>
                        <li class="list-group-item">
                            <b>NO HP</b> <a class="float-right">{{$result->no_handphone}}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Jens Kelamin</b> <a class="float-right">{{$result->jenis_kelamin}}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Alamat</b> <a class="float-right">{{$result->alamat}}</a>
                        </li>
                        <li class="list-group-item">
                            <b>RT / RT</b> <a class="float-right">{{$result->rt}} / {{$result->rw}}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Kelurahan</b> <a class="float-right">{{$result->kelurahan}}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Kecamatan</b> <a class="float-right">{{$result->kecamatan}}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Kota</b> <a class="float-right">{{$result->kota}}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Kode Pos</b> <a class="float-right">{{$result->kode_pos}}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Pendidikan</b> <a class="float-right">{{$result->pendidikan}}</a>
                        </li>
                        </ul>

                       
                        
                        
                        <i class="fa fa-reply"> BACK</i>
                        </a>
                        

                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
            <div class="col-md-9">
                <div class="card">
                    <div class="card-header p-2">
                        <ul class="nav nav-pills">
                            
                            <li class="nav-item"><a class="nav-link active" href="#usaha" data-toggle="tab">Data Usaha</a></li>
                            <li class="nav-item"><a class="nav-link" href="#timeline" data-toggle="tab">Legalitas Usaha</a></li>
                        </ul>
                    </div>
                    <div class="card-body">
                        <div class="tab-content">
                            <div class="active tab-pane" id="usaha">
                                <!-- Pribadi -->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="callout callout-info">
                                            <p class="text-muted text-center">Profile Usaha</p>
                                            <ul class="list-group list-group-unbordered mb-3">
                                                <li class="list-group-item">
                                                    <b>Nama Usaha</b> <a class="float-right">{{$result->nama_usaha}}</a>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Alamat Usaha</b> <a class="float-right">{{$result->alamat_usaha}}</a>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>RT /RW </b> <a class="float-right">{{$result->rt}} / {{$result->rw}}</a>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>No Telp</b> <a class="float-right">{{$result->no_telepon}}</a>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Tahun Pendaftaran</b> <a class="float-right">{{$result->thn_daftar}}</a>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Tahun Berdiri</b> <a class="float-right">{{$result->thn_berdiri}}</a>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Bentuk Usaha</b> <a class="float-right">{{$result->bentuk_usaha}}</a>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Jenis Usaha</b> <a class="float-right">{{$result->jenis_usaha}}</a>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Produk</b> <a class="float-right">{{$result->produk_1}}, {{$result->produk_2}}, {{$result->produk_3}} </a>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Jumlah Karyawan</b> <a class="float-right">{{$result->jumlah_karyawan}} </a>
                                                </li>
                                            </ul>
                                            <p class="text-muted text-center">Bahan Baku Produk</p>
                                            <ul class="list-group list-group-unbordered mb-3">
                                                <li class="list-group-item">
                                                    <b>Bahan Baku Utama</b> <a class="float-right">{{$result->bahan_utama}}</a>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Perolehan Bahan Baku Utama</b> <a class="float-right">{{$result->perolehan}}</a>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Detail Perolehan</b> <a class="float-right">{{$result->detail_perolehan}}</a>
                                                </li>
                                                
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="callout callout-info">
                                                                                
                                            <p class="text-muted text-center">Manajemen Keuangan</p>
                                            <ul class="list-group list-group-unbordered mb-3">
                                                <li class="list-group-item">
                                                    <b>Manajemen Keuangan</b> <a class="float-right">{{$result->manajemen_keuangan}}</a>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Detail Manajemen</b> <a class="float-right">{{$result->detail_manajemen}}</a>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Modal Sendiri</b> <a class="float-right">{{$result->modal_sendiri}}</a>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Modal Luar</b> <a class="float-right">{{$result->modal_luar}}</a>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Penjamin Keuangan</b> <a class="float-right">{{$result->penjamin}}</a>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Detail Penjamin</b> <a class="float-right">{{$result->detail_penjamin}}</a>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Nominal Pinjaman</b> <a class="float-right">{{$result->nominal}}</a>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Jenis Omset</b> <a class="float-right">{{$result->jenis_omset}}</a>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Omset/Thn</b> <a class="float-right">Rp. {{$result->jumlah_omset}}</a>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Jenis Aset</b> <a class="float-right">{{$result->jenis_aset}}</a>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Jumlah Aset</b> <a class="float-right">Rp. {{$result->jumlah_aset}}</a>
                                                </li>
                                                                                            
                                            </ul>
                                        
                                            <p class="text-muted text-center">Promosi dan Pemasaran</p>
                                            <ul class="list-group list-group-unbordered mb-3">
                                                <li class="list-group-item">
                                                    <b>Pasar</b> <a class="float-right">{{$result->pemasaran}}</a>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Detail Pasar</b> <a class="float-right">{{$result->detailpemasaran}}</a>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Kerjasama</b> <a class="float-right">{{$result->kerjasama}}</a>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Detail Kerjasama</b> <a class="float-right">{{$result->detailkerjasama}}</a>
                                                </li>                                               
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12"> 
                                    <div class="callout callout-success">
                                        <p class="text-muted text-center">Keterangan</p>
                                        <ul class="list-group list-group-unbordered mb-9">
                                        <a class="float-center">{{$result->detailkerjasama}}</a>
                                        </ul>
                                    </div>
                                </div>
                                <!-- /.Pribadi -->
                            </div>

                            <div class="tab-pane" id="timeline">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="callout callout-info">
                                            <p class="text-muted text-center">Legalitas Usaha</p>
                                            <ul class="list-group list-group-unbordered mb-3">
                                                <li class="list-group-item">
                                                    <b>No. NIB + IUMK</b> <a class="float-right">{{$result->nib}}</a>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>No. NPWP</b> <a class="float-right">{{$result->npwp}}</a>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>No. HAKI</b> <a class="float-right">{{$result->haki}}</a>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>No. HALAL</b> <a class="float-right">{{$result->halal}}</a>
                                                </li>                                               
                                                <li class="list-group-item">
                                                    <b>No. P-IRT</b> <a class="float-right">{{$result->pirt}}</a>
                                                </li>                                               
                                                <li class="list-group-item">
                                                    <b>No. BPOM</b> <a class="float-right">{{$result->bpom}}</a>
                                                </li>                                               
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                                            <ol class="carousel-indicators">
                                                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                                                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                                            </ol>
                                            <div class="carousel-inner">
                                                <div class="carousel-item active">
                                                <img class="d-block w-100" src="https://placehold.it/900x500/39CCCC/ffffff&text=I+Love+Bootstrap" alt="First slide">
                                                </div>
                                                <div class="carousel-item">
                                                <img class="d-block w-100" src="https://placehold.it/900x500/3c8dbc/ffffff&text=I+Love+Bootstrap" alt="Second slide">
                                                </div>
                                                <div class="carousel-item">
                                                <img class="d-block w-100" src="https://placehold.it/900x500/f39c12/ffffff&text=I+Love+Bootstrap" alt="Third slide">
                                                </div>
                                            </div>
                                            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
    </div>
    
</section>

