@extends('layouts.app')

@section('title', 'TAMBAH DATA UMKM')

@section('content')
@if (session('statustambah'))
    <div class="alert alert-success">
        {{ session('statustambah') }}
    </div>
@endif
@if (session('error'))
    <div class="alert alert-danger">
        {{ session('statustambah') }}
    </div>
@endif
<section class="content">
	<div class="form-horizontal">
		<div class="box box-default">
			<div class="card card-primary">
				<div class="card-header">
					<h3 class="card-title"></h3>

					<div class="card-tools">
						<button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
						<i class="fas fa-minus"></i></button>
					</div>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-md-12">
							
								<div class="card">
									<div class="card-header p-2">
										<ul class="nav nav-pills">
										<li class="nav-item"><a class="nav-link active" href="#pribadi" data-toggle="tab">Data Pribadi</a></li>
										<li class="nav-item"><a class="nav-link" href="#usaha" data-toggle="tab">Data Usaha</a></li>
										<li class="nav-item"><a class="nav-link" href="#lainya" data-toggle="tab">Data Lainnya</a></li>
										<li class="nav-item"><a class="nav-link" href="#upload" data-toggle="tab">Data Upload</a></li>
										</ul>
									</div><!-- /.card-header -->
									<form class="form-horizontal" id="quickForm" method="POST" action="/create" enctype="multipart/form-data" > 
									@csrf
									<div class="card-body">
										<div class="tab-content">
											<div class="active tab-pane" id="pribadi">
												<div class="form-group clearfix">
													<label for="inputEmail3" class="col-sm-2 control-label" align="right">AKTIVASI USAHA</label>
														<div class="col-sm-1 icheck-success d-inline">
															<input type="radio" name="aktivasi" checked id="aktivasi1" value="Aktif">
															<label for="aktivasi1">Aktif
															</label>
														</div>
														<div class="col-sm-1 icheck-success d-inline">
															<input type="radio" name="aktivasi" disabled id="aktivasi2" value="Non Aktif">
															<label for="aktivasi2">
															Non Aktif
															</label>
														</div>	
												</div>

												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label" for="nik" align="right">NIK</label>

													<div class="col-sm-3">
														<input type="hidden" class="form-control" id="id_anggota" name="id_anggota" 
														value="{{$newOrderId}}">
														<input type="text" class="form-control @error('nik') is-invalid @enderror" id="nik" name="nik" placeholder="Isikan NIK" value="{{ old('nik') }}"> <span id="result"></span>
														<div class="invalid-tooltip">
															Silihkan Isikan NIK
														</div>
													</div>
												</div>
												
												<div class="form-group row ">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right">Nama Pemilik</label>
													<div class="col-sm-6">
														<input type="text" class="form-control @error('nama_dpn') is-invalid @enderror"  id="nama_dpn" name="nama_dpn"  placeholder="Nama Depan" value="{{ old('nama_dpn') }}">
														<div class="invalid-tooltip">
															Silihkan Isikan Nama Pemilik
														</div>
													</div>	
												</div>
												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right">Alamat</label>

													<div class="col-sm-10">
														<textarea name="alamat" id="alamat" class="@error('alamat') is-invalid @enderror" rows="4" cols="63" placeholder=" Isikan alamat lengkap" >{{ old('alamat') }}</textarea>
														<div class="invalid-tooltip">
															Silihkan Isikan Alamat
														</div>
													</div>
												</div>

												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label " align="right">RT</label>

													<div class="col-sm-1">
														<input type="text" style="text-align:right" id="rt" name="rt" class="form-control @error('rt') is-invalid @enderror"  placeholder="" value="{{ old('rt') }}">
														
													</div>
													
													
													<label for="inputEmail3" class="col-sm-1 col-form-label" align="right">RW</label>
													<div class="col-sm-1">
														<input type="text" style="text-align:right" class="form-control @error('rw') is-invalid @enderror" id="rw" name="rw" placeholder="" value="{{ old('rw') }}">
														
													</div>
													

													<label for="inputEmail3" class="col-sm-1 col-form-label" align="right">No. Telp</label>
													<div class="col-sm-2">
														<input type="text" class="form-control @error('telp') is-invalid @enderror" id="telp" name="telp" placeholder="No. Telepon" value="{{ old('telp') }}">
														
													</div>
													

												</div>

												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right">No. Handphone</label>

													<div class="col-sm-2">
														<input type="text" class="form-control @error('hp') is-invalid @enderror" data-inputmask="'mask': ['9999-9999-9999']" data-mask
														id="hp" name="hp" placeholder="No. Handphone" value="{{ old('hp') }}">
														<div class="invalid-tooltip">
															Silihkan Isikan No Handphone
														</div>
													</div>
													

													<label for="inputEmail3" class="col-sm-1 col-form-label" align="right">Email</label>

													<div class="col-sm-3">
														<input type="email"  class="form-control @error('email') is-invalid @enderror" id="email" name="email" placeholder="Email" value="{{ old('email') }}">
														@error('email')
															<div class="alert alert-danger">{{ $message }}</div>
														@enderror
													</div>
													
												</div>

												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label " align="right">Jenis Kelamin</label>

													<div class="col-sm-6">
														<select class="form-control select2 @error('jeniskelamin')== null is-invalid @enderror" id="jeniskelamin" name="jeniskelamin" style="width: 100%;" value="{{ old('jeniskelamin') }}">
															<option selected  value="">-- Pilih Jenis Kelamin --</option>
															<option value ="Laki - laki "<?php if(old('jeniskelamin')=="Laki - laki"){ echo "selected";}?>>Laki - Laki</option>
															<option value ="Perempuan"<?php if(old('jeniskelamin')=="Perempuan"){ echo "selected";}?>>Perempuan</option>
														</select>
														<div class="invalid-tooltip">
															Silihkan Pilih Jenis Kelamin
														</div>
														
													</div>
												</div>

												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right">Kecamatan</label>

													<div class="col-sm-6">
													<select class="form-control select2  @error('kecamatan')== null is-invalid @enderror" id="kecamatan" name="kecamatan" style="width: 100%;" >
													<option selected disabled value="">-- Pilih Kecamatan --</option>
													@foreach($kec as $k1)
														
														<option  <?php 
															if (old('kecamatan') == $k1->id_kecamatan){
															echo "selected";	
															}
															else{
															}
															?> 
														value = "<?php echo $k1->id_kecamatan?>"><?php echo $k1->kecamatan; ?>
														</option>
														@endforeach
														</select>
														<div class="invalid-tooltip">
															Silihkan Pilih Kecamatan
														</div>
													</div>
												</div>

												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right">Kelurahan</label>
			   
													 <div class="col-sm-6">
													  <select class="form-control select2" id="cari_kelurahan" name="kelurahan" style="width: 100%;">
														 
														
													   </select>
													 </div>
											   </div>

												<div class="form-group row ">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right">Kota</label>

													<div class="col-sm-3">
														<input type="text"  class="form-control" id="kota" name="kota" value="Bandung" readonly="" >
													</div>

													<label for="inputEmail3" class="col-sm-1 col-form-label" align="right">Kode Pos</label>

													<div class="col-sm-2">
														<input type="text"  class="form-control" id="kode_pos" name="kode_pos" value="{{ old('kode_pos') }}" >
													</div>
												</div>

												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right"> Pendidikan Terakhir</label>
														<div class="col-sm-1 icheck-success d-inline">
															<input type="radio" name="pendidikan" id="pendidikan1"  value="SD"
																<?php if(old('pendidikan')=="SD"){ echo "checked";}?>>	
																<label  for="pendidikan1">SD</label>
														</div>
														<div class="col-sm-1 icheck-success d-inline">
															<input type="radio" name="pendidikan" id="pendidikan2" class="col-sm-40 d-inline" value="SMP"
																<?php if(old('pendidikan')=="SMP"){ echo "checked";}?>>
																<label for="pendidikan2">SMP</label>
														</div>
														<div class="col-sm-1 icheck-success d-inline">
															<input type="radio" name="pendidikan" id="pendidikan7" class="col-sm-40 d-inline" value="SMA"
																<?php if(old('pendidikan')=="SMA"){ echo "checked";}?>>
																<label for="pendidikan7">SMA</label>
														</div>
														<div class="col-sm-1 icheck-success d-inline">
															<input type="radio" name="pendidikan" id="pendidikan3" class="col-sm-40 d-inline" value="D3"
																<?php if(old('pendidikan')=="D3"){ echo "checked";}?>>
																<label for="pendidikan3">D3</label>
														</div>
														<div class="col-sm-1 icheck-success d-inline">
														<input type="radio" name="pendidikan" id="pendidikan4" value="S1"
																<?php if(old('pendidikan')=="S1"){ echo "checked";}?>>
																<label for="pendidikan4">S1</label>
														</div>
														<div class="col-sm-1 icheck-success d-inline">
															<input type="radio" name="pendidikan" id="pendidikan5" value="S2"
																<?php if(old('pendidikan')=="S2"){ echo "checked";}?>>
																<label for="pendidikan5">S2</label>
														</div>
														<div class="col-sm-1 icheck-success d-inline">
															<input type="radio" name="pendidikan" id="pendidikan6" value="S3"
																<?php if(old('pendidikan')=="S3"){ echo "checked";}?>>
																<label for="pendidikan6">S3</label>
														</div>
													</div>
													<div class="card-footer" >
														<a href="#usaha" ><button type="button" name="next2" id="next2" class="btn btn-info float-right "><i class="far fa-hand-point-right">Lanjut</i></button></a>
													</div>
											</div>

											<div class="tab-pane" id="usaha">
												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right">Nama Usaha</label>

													<div class="col-sm-6">
														<input type="text" id="nama_usaha" name="nama_usaha" class="form-control @error('nama_usaha') is-invalid @enderror"  placeholder="Isikan Nama Usaha" value="{{ old('nama_usaha') }}">
														<div class="invalid-tooltip">
															Silihkan Isikan Nama Usaha
														</div>
													</div>
												</div>
												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right">Alamat</label>

													<div class="col-sm-6">
														<textarea name="alamat_usaha" id="alamat_usaha" class="@error('alamat_usaha') is-invalid @enderror" rows="4" cols="63" placeholder=" Isikan alamat lengkap" value="{{ old('alamat_usaha') }}">{{ old('alamat_usaha') }}</textarea>
														<div class="invalid-tooltip">
															Silihkan Isikan Alamat Usaha
														</div>
													</div>

													
												</div>
												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right">RT</label>

													<div class="col-sm-1">
														<input type="text" style="text-align:right" class="form-control" id="rt_usaha" name="rt_usaha" placeholder="" value="{{ old('rt_usaha') }}">
													</div>

													<label for="inputEmail3" class="col-sm-1 col-form-label" align="right">RW</label>
													<div class="col-sm-1">
														<input type="text" style="text-align:right" class="form-control" id="rw_usaha" name="rw_usaha" placeholder="" value="{{ old('rw_usaha') }}">
													</div>

													<label for="inputEmail3" class="col-sm-1 col-form-label" align="right">No. Telp</label>
													<div class="col-sm-2">
														<input type="text" class="form-control" id="telp_usaha" name="telp_usaha" placeholder="Isikan No. Telepon" value="{{ old('telp_usaha') }}">
													</div>

												</div>
												
												<div class="form-group row">
													<label  class="col-sm-2 col-form-label" align="right">Tahun Pendaftaran</label>
														<div class="col-sm-2">
															<div class="input-group date " id="reservationdate" data-target-input="nearest">
																<input type="text" name="thn_pendaftaran" class="form-control datetimepicker-input @error('thn_pendaftaran') is-invalid @enderror" 
																	data-target="#reservationdate" placeholder="Pendaftaran " 
																	id="reservationdate"   value="{{ old('thn_pendaftaran') }}" /> 
																<div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
																	<div class="input-group-text"><i class="fa fa-calendar"></i></div>
																</div>
																<div class="invalid-tooltip">
															Silihkan Pilih Tahun 
														</div>
															</div>
														</div>
												<!-- </div>
												<div class="form-group row"> -->
													<label  class="col-sm-2 col-form-label" align="right">Tahun Berdiri</label>
														<div class="col-sm-2">
															<div class="input-group" id="reservationdate1" data-target-input="nearest">
																<input type="text" name="thn_berdiri" class="form-control datetimepicker-input @error('thn_berdiri') is-invalid @enderror" 
																	data-target="#reservationdate1" placeholder="Berdiri " 
																	id="reservationdate1" value="{{ old('thn_berdiri') }}"/> 
																<div class="input-group-append" data-target="#reservationdate1" data-toggle="datetimepicker">
																	<div class="input-group-text"><i class="fa fa-calendar"></i></div>
																</div>
																<div class="invalid-tooltip">
															Silihkan Pilih Tahun
														</div>	
															</div>
														</div>	
												</div>

												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right">Bentuk Usaha</label>

													<div class="col-sm-2">
													<select class="form-control select2 @error('bentuk_usaha') is-invalid @enderror" name="bentuk_usaha" style="width: 100%;" >
														<option selected disabled value="" selected="selected">-- Pilih Bentuk Usaha--</option>
														<option value="PD" <?php if(old('bentuk_usaha')=="PD"){ echo "selected";}?>>PD</option>                                    	
														<option value="Perseorangan" <?php if(old('bentuk_usaha')=="Perseorangan"){ echo "selected";}?>>Perorangan</option>
														</select>
														<div class="invalid-tooltip">
															Silihkan Pilih Bentuk Usaha
														</div>
													</div>
													
												<!-- </div>
												<div class="form-group row"> -->
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right">Jenis Usaha</label>

													<div class="col-sm-2">
													<select class="form-control select2 @error('jenis_usaha') is-invalid @enderror" name="jenis_usaha" style="width: 100%;" >
													<option selected disabled value="" value="" selected="selected">-- Pilih Jenis Usaha--</option>
															<option value="Fashion" <?php if(old('jenis_usaha')=="Fashion"){ echo "selected";}?>>Fashion</option>
															<option value="Kuliner" <?php if(old('jenis_usaha')=="Kuliner"){ echo "selected";}?>>Kuliner</option>
															<option value="Jasa" <?php if(old('jenis_usaha')=="Jasa"){ echo "selected";}?>>Jasa</option>
															<option value="Perdagangan"<?php if(old('jenis_usaha')=="Perdagangan"){ echo "selected";}?>>Perdagangan</option>
															<option value="Handicraft"<?php if(old('jenis_usaha')=="Handicraft"){ echo "selected";}?>>Handicraft</option>
															<option value="lain-lain" <?php if(old('jenis_usaha')=="lain-lain"){ echo "selected";}?>>Lainnya</option>
														</select>
														<div class="invalid-tooltip">
															Silihkan Pilih Jenis Usaha
														</div>
													</div>
													
												</div>
												<hr class="col-sm-12 ">			
												<div class="input-group">
													<label for="inputEmail3" class="col-sm-5 col-form-label" align="right">Spesifikasi Produk </label>
												</div>
												<hr class="col-sm-12">
												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right">Jenis Produk</label>
													<div class="col-sm-2">
													<input type="text" name="spesifikasi_produk1" class="form-control @error('spesifikasi_produk1') is-invalid @enderror" placeholder="Contoh produk 1" value="{{ old('spesifikasi_produk1') }}"> 
													<div class="invalid-tooltip">
															Silihkan Isikan Produk 1
														</div>
													</div>
													<div class="col-sm-2">
													<input type="text" name="spesifikasi_produk2" class="form-control" placeholder="Contoh produk 2" value="{{ old('spesifikasi_produk2') }}"> 
													</div>
													<div class="col-sm-2">
													<input type="text" name="spesifikasi_produk3" class="form-control" placeholder="Contoh produk 3" value="{{ old('spesifikasi_produk3') }}"> 
													</div>
												</div>

												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right">Bahan Baku Utama</label>
													<div class="col-sm-6">
													<input type="text" name="bahanbakuutama" class="form-control" placeholder="Bahan Baku Utama" value="{{ old('bahanbakuutama') }}"> 
													</div>
												</div>

												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right">Perolehan Bahan Baku</label>
														
													<div class="col-sm-6">
														<div class="col-sm-2 icheck-primary d-inline">
															<input type="checkbox" name="bahan_baku[]" class=" col-sm-1 " id="checkboxPrimary1" value="Lokal"
															@if(is_array(old('bahan_baku')) && in_array('Lokal', old('bahan_baku'))) checked @endif>
															<label for="checkboxPrimary1">Lokal
															</label>
														</div>
														<div class="col-sm-2 icheck-primary d-inline">
															<input type="checkbox" name="bahan_baku[]" class=" col-sm-1 " id="checkboxPrimary2" value="Regional"
															@if(is_array(old('bahan_baku')) && in_array('Regional', old('bahan_baku'))) checked @endif>
															<label for="checkboxPrimary2">Regional
															</label>
														</div>
														<div class="col-sm-2 icheck-primary d-inline">
														<input type="checkbox" name="bahan_baku[]" class=" col-sm-1 " id="checkboxPrimary3" value="Nasional"
															@if(is_array(old('bahan_baku')) && in_array('Nasional', old('bahan_baku'))) checked @endif>
															<label for="checkboxPrimary3">Nasional
															</label>
														</div>
														<div class="col-sm-2 icheck-primary d-inline">
														<input type="checkbox" name="bahan_baku[]" class=" col-sm-1 " id="checkboxPrimary4" value="Import"
															@if(is_array(old('bahan_baku')) && in_array('Import', old('bahan_baku'))) checked @endif>
															<label for="checkboxPrimary4">Import
															</label>
														</div>
														<label for="inputEmail3" class="col-sm-2 col-form-label" align="right"></label>
														<div class="col-sm-12">
														<input type="text" name="perolehan" class="form-control" placeholder="detailkan perolehan bahan baku" value="{{ old('perolehan') }}">
														</div>
													</div>  
												</div>
												<hr class="col-sm-12 ">			
												<div class="input-group">
													<label for="inputEmail3" class="col-sm-5 col-form-label" align="right">Manajemen Usaha </label>
												</div>
												<hr class="col-sm-12">
												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right">Jumlah Karyawan</label>
													<div class="col-sm-6">
													<input type="text" name="jumlah_karyawan" class="form-control" placeholder="Jumlah Karyawan" value="{{ old('jumlah_karyawan') }}"> 
													</div>
												</div>
												
												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right">Sendiri </label>
													<div class="col-sm-2">
														<div class="input-group">
															<div class="input-group-prepend">
																<span class="input-group-text">Rp</span>
															</div>
															<input type="text" class="form-control" id="mdl_sendiri" name="mdl_sendiri" placeholder="" value="{{ old('mdl_sendiri') }}" >
														</div>
													</div>
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right"> Modal Luar</label>
													<div class="col-sm-2">
														<div class="input-group">
															<div class="input-group-prepend">
																<span class="input-group-text">Rp</span>
															</div>
															<input type="text" class="form-control" id="mdl_luar" name="mdl_luar" placeholder="" value="{{ old('mdl_luar') }}">
															
														</div>
													</div>
												</div>

												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right">Penjamin Keuangan</label>
													<div class="col-sm-2 icheck-primary d-inline">
													<input type="checkbox" name="penjamin[]" id="penjamin1"   value="Perbankan"
															@if(is_array(old('penjamin')) && in_array('Perbankan', old('penjamin'))) checked @endif>
														<label for="penjamin1">Perbankan
														</label>
													</div>
													<div class="col-sm-1 icheck-primary d-inline">
													<input type="checkbox" name="penjamin[]" id="penjamin2" value="PKBL"
															@if(is_array(old('penjamin')) && in_array('PKBL', old('penjamin'))) checked @endif>
														<label for="penjamin2">PKBL
														</label>
													</div>
													<div class="col-sm-1 icheck-primary d-inline">
													<input type="checkbox" name="penjamin[]" id="penjamin3"  value="CSR"
															@if(is_array(old('penjamin')) && in_array('CSR', old('penjamin'))) checked @endif>
														<label for="penjamin3">CSR
														</label>
													</div>
													<div class="col-sm-1 icheck-primary d-inline">
													<input type="checkbox" name="penjamin[]" id="penjamin4"  value="HIBAH"
															@if(is_array(old('penjamin')) && in_array('HIBAH', old('penjamin'))) checked @endif>
														<label for="penjamin4">HIBAH
														</label>
													</div>
													<div class="col-sm-1 icheck-primary d-inline">
													<input type="checkbox" name="penjamin[]" id="penjamin5"  value="Investor"
															@if(is_array(old('penjamin')) && in_array('Investor', old('penjamin'))) checked @endif>
														<label for="penjamin5">Investor
														</label>
													</div>
													<div class="col-sm-1 icheck-primary d-inline">
														<input type="checkbox" name="penjamin[]" id="penjamin6"  value="Tidak Ada"
																@if(is_array(old('penjamin')) && in_array('Tidak Ada', old('penjamin'))) checked @endif>
															<label for="penjamin6">Tidak Ada
															</label>
													</div>
													
												</div>
												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right"></label>
													<div class="col-sm-6">
													<input type="text" name="penjamindetail" class="form-control" placeholder="detailkan penjamin keuangan" value="{{ old('penjamindetail') }}">
													</div>
												</div>
												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right">Nominal Pinjaman</label>
													<div class="col-sm-6">
														<div class="input-group">
															<div class="input-group-prepend">
																<span class="input-group-text">Rp</span>
															</div>
															<input type="Text" class="form-control" id="nominalpinjaman" name="nominalpinjaman" placeholder="" value="{{ old('nominalpinjaman') }}">
														</div>
													</div>
												</div>

												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right">Omset</label>

													<div class="col-sm-6">
														<div class="input-group">
														<div class="input-group-prepend">
																<span class="input-group-text">Rp</span>
															</div>
														<input type="Text" class="form-control @error('omset') is-invalid @enderror" id="omset" name="omset" placeholder=""value="{{ old('omset') }}">
														<div class="input-group-append">
															<span class="input-group-text">/Tahun</span>
														</div>
														<div class="invalid-tooltip">
															Silihkan Isikan Omset
														</div>
														</div>
													</div>
												</div>	
												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right">Aset</label>

													<div class="col-sm-6">
														<div class="input-group">
														<div class="input-group-prepend">
																<span class="input-group-text">Rp</span>
															</div>
														<input type="Text" class="form-control @error('aset') is-invalid @enderror" id="aset" name="aset" placeholder="" value="{{ old('aset') }}">
														<div class="input-group-append">
															<span class="input-group-text">/Tahun</span>
														</div>
														<div class="invalid-tooltip">
															Silihkan Isikan Aset
														</div>
														</div>
													</div>
												</div>

												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-3 col-form-label" align="right">Manajemen Keuangan</label>
													<div class="col-sm-2 icheck-primary d-inline">
														<input type="radio" name="keuangan" id="keuangan1"  value="Manual"
														<?php if(old('keuangan')=="Manual"){ echo "checked";}?> >
															<label for="keuangan1">Manual
															</label>
													</div>
													<div class="col-sm-2 icheck-primary d-inline">
														<input type="radio" name="keuangan" id="keuangan2"  value="By Sistem"
														<?php if(old('keuangan')=="By Sistem"){ echo "checked";}?>>
															<label for="keuangan2"> By Sistem
															</label>
													</div>
												</div>
												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right"></label>
													<div class="col-sm-6">
													<input type="text" name="keuangandetail" class="form-control" placeholder="Jelaskan" value="{{ old('keuangandetail') }}"> 
													</div>
												</div>
												<div class="card-footer" >
													<a href="#pribadi" ><button type="button" name="back1" id="back1" class="btn btn-default"><i class="far fa-hand-point-left">Kembali</i></button></a>
													<a href="#lainya" ><button type="button" name="next3" id="next3" class="btn btn-info float-right"><i class="far fa-hand-point-right">Lanjut</i></button></a>
												</div>
													
											</div>

											<div class="tab-pane" id="lainya">
												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right">Media Penjualan</label>
													<div class="col-sm-2 icheck-primary d-inline">
													<input type="checkbox" name="mediapenjualan[]" id="media1" value="Market Place"
															@if(is_array(old('mediapenjualan')) && in_array('Market Place', old('mediapenjualan'))) checked @endif>
														<label for="media1">Market Place
														</label>
													</div>
													<div class="col-sm-2 icheck-primary d-inline">
													<input type="checkbox" name="mediapenjualan[]" id="media2" value="Sistem During"
															@if(is_array(old('mediapenjualan')) && in_array('Sistem During', old('mediapenjualan'))) checked @endif>
														<label for="media2">Sistem During
														</label>
													</div>
													<div class="col-sm-2 icheck-primary d-inline">
													<input type="checkbox" name="mediapenjualan[]" id="media3" value="Konsinyasi"
															@if(is_array(old('mediapenjualan')) && in_array('Konsinyasi', old('mediapenjualan'))) checked @endif>
														<label for="media3">Konsinyasi
														</label>
													</div>
													<div class="col-sm-2 icheck-primary d-inline">
													<input type="checkbox" name="mediapenjualan[]" id="media4" value="Toko"
															@if(is_array(old('mediapenjualan')) && in_array('Toko', old('mediapenjualan'))) checked @endif>
														<label for="media4">Toko
														</label>
													</div>
												</div>

												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right"></label>
													<div class="col-sm-6">
													<input type="text" name="detailmediapenjualan" class="form-control" placeholder="Jelaskan" value="{{ old('detailmediapenjualan') }}"> 
													</div>
												</div>

												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right">Daerah Pemasaran</label>
													<div class="col-sm-2 icheck-primary d-inline">
													<input type="checkbox" name="pasar[]" id="pasar1" value="Bandung Raya"
															@if(is_array(old('pasar')) && in_array('Bandung Raya', old('pasar'))) checked @endif>
														<label for="pasar1">Bandung Raya
														</label>
													</div>
													<div class="col-sm-2 icheck-primary d-inline">
													<input type="checkbox" name="pasar[]" id="pasar2" value="Regional"
															@if(is_array(old('pasar')) && in_array('Regional', old('pasar'))) checked @endif>
														<label for="pasar2"> Regional
														</label>
													</div>
													<div class="col-sm-2 icheck-primary d-inline">
													<input type="checkbox" name="pasar[]" id="pasar3" value="Nasional"
															@if(is_array(old('pasar')) && in_array('Nasional', old('pasar'))) checked @endif>
														<label for="pasar3">Nasional
														</label>
													</div>
													<div class="col-sm-2 icheck-primary d-inline">
													<input type="checkbox" name="pasar[]" id="pasar4" value="Ekspor"
															@if(is_array(old('pasar')) && in_array('Ekspor', old('pasar'))) checked @endif>
														<label for="pasar4">Ekspor
														</label>
													</div>
												</div>

												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right"></label>
													<div class="col-sm-6">
													<input type="text" name="detailpasar" class="form-control" placeholder="Jelaskan" value="{{ old('detailpasar') }}"> 
													</div>
												</div>

												<div class="form-group">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right">Kerjasama Bisnis</label>
													<div class="col-sm-2 icheck-primary d-inline">
													<input type="checkbox" name="kerjasama[]" id="kerjasama1" value="Pernah"
															@if(is_array(old('kerjasama')) && in_array('Pernah', old('kerjasama'))) checked @endif>
														<label for="kerjasama1">Pernah
														</label>
													</div>
													<div class="col-sm-2 icheck-primary d-inline">
													<input type="checkbox" name="kerjasama[]" id="kerjasama2" value="Tidak Pernah"
															@if(is_array(old('kerjasama')) && in_array('Tidak Pernah', old('kerjasama'))) checked @endif>
														<label for="kerjasama2">Tidak Pernah
														</label>
													</div>
												</div>

												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right"></label>
													<div class="col-sm-6">
													<input type="text" name="detailkerjasama" class="form-control" placeholder="Jelaskan" value="{{ old('detailkerjasama') }}"> 
													</div>
												</div>
												<hr class="col-sm-12 ">			
												<div class="input-group">
													<label for="inputEmail3" class="col-sm-5 col-form-label" align="right">PERIZINAN </label>
												</div>
												<hr class="col-sm-12">
												<div class="form-group row">
																										
													<div class="col-sm-2 icheck-primary d-inline">
													<input type="checkbox" name="ijin[]" id="izin1" value="NIB + IUMK"
															@if(is_array(old('ijin')) && in_array('NIB + IUMK', old('ijin'))) checked @endif>
														<label for="izin1">NIB + IUMK
														</label>
														<input type="text" name="nib" class="form-control @error('nib') is-invalid @enderror" class=" col-sm-3" placeholder="NOMOR NIB + IUMK" value="{{ old('nib') }}">
														<div class="invalid-tooltip">
															Silihkan Masukan  NO NIB
														</div>
													</div>		
													<div class="col-sm-2 icheck-primary d-inline">
													<input type="checkbox" name="ijin[]" id="izin2" value="NPWP"
															@if(is_array(old('ijin')) && in_array('NPWP', old('ijin'))) checked @endif>
														<label for="izin2">NPWP
														</label>
														<input type="text" name="npwp" class="form-control @error('npwp') is-invalid @enderror" class=" col-sm-3" placeholder="NOMOR NPWP" value="{{ old('npwp') }}">
														<div class="invalid-tooltip">
															Silihkan Masukan No NPWP
														</div>
													</div>		
													<div class="col-sm-2 icheck-primary d-inline">
													<input type="checkbox" name="ijin[]" id="izin3" value="HAKI"
															@if(is_array(old('ijin')) && in_array('HAKI', old('ijin'))) checked @endif>
														<label for="izin3">HAKI
														</label>
														<input type="text" name="haki" class="form-control" class=" col-sm-3" placeholder="NOMOR HAKI" value="{{ old('haki') }}">
													</div>		
													<div class="col-sm-2 icheck-primary d-inline">
													<input type="checkbox" name="ijin[]" id="izin4" value="HALAL"
															@if(is_array(old('ijin')) && in_array('HALAL', old('ijin'))) checked @endif>
														<label for="izin4">HALAL
														</label>
														<input type="text" name="halal" class="form-control" class=" col-sm-3" placeholder="NOMOR HALAL" value="{{ old('halal') }}">
													</div>		
													<div class="col-sm-2 icheck-primary d-inline">
													<input type="checkbox" name="ijin[]" id="izin5" value="P-IRT"
															@if(is_array(old('ijin')) && in_array('P-IRT', old('ijin'))) checked @endif>
														<label for="izin5">P-IRT
														</label>
														<input type="text" name="pirt" class="form-control" class=" col-sm-3" placeholder="NOMOR P-IRT" value="{{ old('pirt') }}">
													</div>		
													<div class="col-sm-2 icheck-primary d-inline">
													<input type="checkbox" name="ijin[]" id="izin6" value="BPOM"
															@if(is_array(old('ijin')) && in_array('BPOM', old('ijin'))) checked @endif>
														<label for="izin6">BPOM
														</label>
														<input type="text" name="bpom" id="bpom" class="form-control " class=" col-sm-3" placeholder="NOMOR BPOM" value="{{ old('bpom') }}">
													</div>		
												</div>

												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-2 col-form-label" align="right">KETERANGAN</label>
																		<div class="col-sm-6">
														<textarea name="keterangan" id="keterangan" rows="4" cols="63" placeholder="">{{ old('keterangan') }}</textarea>
													</div>
												</div>

												<div class="card-footer" >
														<a href="#usaha" ><button type="button" name="back2" id="back2" class="btn btn-default"><i class="far fa-hand-point-left">  Kembali</i></button></a>
														<a href="#upload" ><button type="button" name="next4" id="next4" class="btn btn-info float-right"><i class="far fa-hand-point-right">Lanjut</i></button></a>
													</div>

											</div>

											<div class="tab-pane" id="upload">
												<div class="form-group row">
													<label for="exampleInputFile" class="col-sm-2 col-form-label" align="right">KTP</label>
													<div class="col-sm-6 icheck-primary d-inline">
														<div class="input-group">
														<div class="custom-file">
															{{--  <input type="file" class="custom-file-input @error('ktp_upload') is-invalid @enderror" id="ktp_upload" name="ktp_upload">  --}}
															<input type="file" name="ktp_upload" class=" @error('ktp_upload') is-invalid @enderror">
															{{--  <label class="custom-file-label" for="ktp_upload">Choose file</label>  --}}
															<div class="invalid-tooltip">
																Silihkan 
															</div>
														</div>
														
														</div>
													</div>
												</div>
												<div class="form-group row">
													<label for="exampleInputFile" class="col-sm-2 col-form-label" align="right">NPWP</label>
													<div class="col-sm-6 icheck-primary d-inline">
														<div class="input-group">
														<div class="custom-file">
															{{--  <input type="file" class="custom-file-input" id="npwp_upload" name="npwp_upload">  --}}
															<input type="file" name="npwp_upload">
															{{--  <label class="custom-file-label" for="npwp_upload">Choose file</label>  --}}
														</div>
														
														</div>
													</div>
												</div>
												<div class="form-group row">
													<label for="exampleInputFile" class="col-sm-2 col-form-label" align="right">Pas Foto</label>
													<div class="col-sm-6 icheck-primary d-inline">
														<div class="input-group">
														<div class="custom-file">
															{{--  <input type="file" class="custom-file-input" id="foto1_upload" name="foto1_upload">  --}}
															<input type="file" name="foto1_upload">
															{{--  <label class="custom-file-label" for="foto1_upload">Choose file</label>  --}}
														</div>
														
														</div>
													</div>
												</div>
												<div class="form-group row">
													<label for="exampleInputFile" class="col-sm-2 col-form-label" align="right">Foto Produk</label>
													<div class="col-sm-6 icheck-primary d-inline">
														<div class="input-group">
														<div class="custom-file">
															{{--  <input type="file" class="custom-file-input" id="foto2_upload" name="foto2_upload">  --}}
															<input type="file" name="foto2_upload">
															{{--  <label class="custom-file-label" for="foto2_upload">Choose file</label>  --}}
														</div>
														
														</div>
													</div>
												</div>
												
												<div class="card-footer" >
													<a href="#lainya" ><button type="button" name="back3" id="back3" class="btn btn-default"><i class="far fa-hand-point-left">  Kembali</i></button></a>
													<button type="submit" class="btn btn-info float-right swalDefaultSuccess"><i class="fa fa-save">  Simpan</i></button>
												</div>
											</div>

										</div>
									</div>
									<from>
								</div>
							
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
      
</section>

@endsection

@push('scripts')
<script>
	$(document).ready(function()
    {    
      $("#nik").change(function()
      {   
        var name = $(this).val(); 
        
        if(name.length > 0)
        {   
          $("#result").html('checking...');       
          $.ajax({
            headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            type : 'POST',
            url  : '/umkmnik/' + name,
            
            success : function(data)
                  {
                       $("#result").html(data);
                    }
            });
            return false;
          
        }
        else
        {
          $("#result").html('');
        }
      });
      
    });
</script>
<script type="text/javascript">
		
        $("#kecamatan").on('change',function() {
            if ($("#kecamatan").val() != ""){
              var kecamatan = $("#kecamatan").val();
             
              $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: "/carikel/" +kecamatan,
                
                data: {
                    id:kecamatan
                    },    
               
                success: function(html) {
                  $("#cari_kelurahan").html(html);
                }
              });
            }
          });

</script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservationdate').datetimepicker({
        format: 'YYYY'
	});

	$('#reservationdate1').datetimepicker({
        format: 'YYYY'
	});
	
	$('#datepicker').datepicker({
      minViewMode: 2,
       format: 'yyyy',
       autoclose: true
    })
    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY hh:mm A'
      }
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Timepicker
    $('#timepicker').datetimepicker({
      format: 'LT'
    })
    
    //Bootstrap Duallistbox
    $('.duallistbox').bootstrapDualListbox()

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    $('.my-colorpicker2').on('colorpickerChange', function(event) {
      $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    });

    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });

  })
</script>
<script>
$(function () {
    $('#pribadi a').click(function (e) {
        e.preventDefault();
        $('a[href="' + $(this).attr('href') + '"]').tab('show');
    })

     $('#usaha a').click(function (e) {
        e.preventDefault();
        $('a[href="' + $(this).attr('href') + '"]').tab('show');
    })

      $('#lainya a').click(function (e) {
        e.preventDefault();
        $('a[href="' + $(this).attr('href') + '"]').tab('show');
	})
	$('#upload a').click(function (e) {
        e.preventDefault();
        $('a[href="' + $(this).attr('href') + '"]').tab('show');
    })
});
 </script>
<script type="text/javascript">
  $(function() {
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000
    });

    $('.swalDefaultSuccess').click(function() {
      Toast.fire({
        icon: 'success',
        title: 'Data Berhasil di Simpan.'
      })
    });
    $('.swalDefaultInfo').click(function() {
      Toast.fire({
        icon: 'info',
        title: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
      })
    });
    $('.swalDefaultError').click(function() {
      Toast.fire({
        icon: 'error',
        title: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
      })
    });
    $('.swalDefaultWarning').click(function() {
      Toast.fire({
        icon: 'warning',
        title: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
      })
    });
    $('.swalDefaultQuestion').click(function() {
      Toast.fire({
        icon: 'question',
        title: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
      })
    });

    
    
  });

</script>
<script type="text/javascript">
	$(document).ready(function () {
		bsCustomFileInput.init();
	  });
</script>
	

@endpush