@extends('layouts.app')

@section('title', 'Proses Pembuatan Surat Keterangan')

@section('content')

<section class="invoice">
    <!-- title row -->
    @foreach($v1 as $result => $result1)
    <div class="row no-print">
      <div class="col-12">
        <a href="/printsk/{{$result1->id_anggota}}" target="_blank" class="btn btn-success float-right"><i class="fas fa-print"></i> Print</a>
        
      </div>
    </div>
    <!-- info row -->
    <div class="row invoice-info">
      
      <div class="col-sm-12 invoice-col">
        <table width="100%" border="0" cellspacing="0" cellpadding="2">        
              <tr>
                    <td align="center" width="10%"><img src="../../dist/img/logo_sk.png" width="100" height="80 " /></td>
                    <td align="center" width="75%">
                    <h2 class="nomargin">PEMERINTAH KOTA BANDUNG<br /></h2>
                    <h3>DINAS KOPERASI USAHA MIKRO KECIL DAN MENENGAH</h3> 
                    <h4> Jl. Kawaluyaan No. 2 Bandung Telp./Fax (022) 7308358 Bandung </h4>
                    </td>
              </tr>
                          
          </table>
          <hr noshade="noshade"/>        
      </div>
    </div>
    <!-- /.row -->
    <br />
   
    <!-- Table row -->
    <div class="row">
      <div class="col-12 table-responsive">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td width="100%" align="center"><h5> SURAT KETERANGAN </h5></td>
            </tr>
            <tr>
                <td width="100%" align="center"><p>Nomor :    KM.01.04  /    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; /Dis-KUMKM/&nbsp;&nbsp;&nbsp;&nbsp; /&nbsp;&nbsp;&nbsp;&nbsp;</p></td>
            </tr>
                           
              
        </table>
        <br>
        
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="10%">&nbsp;</td>
              <td width="80%" align="justify"><p>Sehubungan dengan Surat dari <Strong>{{strtoupper($result1->nama_usaha)}}</Strong> Tanggal  {{$v2[$result]->tgl_permohonan}} Perihal permohonan Surat Keterangan UKM Binaan, dengan ini kami menerangkan sebagai berikut :    
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tbody>
                    <tr>
                      <td width="30%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nama </td>
                      <td width="5%">:</td>
                      <td width="55%">{{strtoupper($result1->nama_depan)}}</td>
                    </tr>
                    <tr>
                        <td width="30%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NIK </td>
                        <td width="5%">:</td>
                        <td width="55%">{{strtoupper($result1->nik)}}</td>
                    </tr>
                    <tr>
                        <td width="30%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;JABATAN </td>
                        <td width="5%">:</td>
                        <td width="55%">Pemilik</td>
                    </tr>
                    <tr>
                        <td width="30%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;JENIS USAHA </td>
                        <td width="5%">:</td>
                        <td width="55%">{{$result1->jenis_usaha}} ( {{$result1->produk_1}} )</td>
                    </tr>
                    <tr>
                        <td width="30%" style="vertical-align:top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ALAMAT USAHA </td>
                        <td width="5%" style="vertical-align:top">:</td>
                        <td width="55%"> {{$result1->alamat_usaha}}</td>
                    </tr>
                    <tr>
                        <td width="30%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NOMOR TELP/HP </td>
                        <td width="5%">:</td>
                        <td width="55%"> {{$result1->no_handphone}}</td>
                    </tr>
                    
                  </tbody>
              </table>
              <td width="5%">&nbsp;</td>
            </tr>
            <tr>
                <td width="10%">&nbsp;</td>
                <td width="80%" align="justify"></td>   
                  
                <td width="5%">&nbsp;</td>
            </tr>
            <tr>
                <td width="10%">&nbsp;</td>
                <td width="80%" align="justify"><p>Berdasarkan Undang-Undang Republik Indonesia Nomor 20 tahun 2008 tentang Usaha Mikro Kecil Menengah bahwa yang bersangkutan adalah termasuk kriteria Usaha Mikro (Profil UMKM terlampir) dan merupakan Binaan Dinas Koperasi Usaha Mikro Kecil dan Menengah Kota Bandung dibidang <strong> {{strtoupper($result1->jenis_usaha)}}</strong>.</td>    
                  
                <td width="5%">&nbsp;</td>
            </tr>
            <tr>
                <td width="10%">&nbsp;</td>
                <td width="80%" align="justify"><p>Surat Keterangan ini berlaku 1 (satu) Tahun sejak tanggal dikeluarkan.</td>
                  
                <td width="5%">&nbsp;</td>
            </tr>
            <tr>
                <td width="10%">&nbsp;</td>
                <td width="80%" align="justify"><p>Demikian Surat Keterangan ini dibuat, untuk dipergunakan sebagaimana mestinya.</td>
                  
                <td width="5%">&nbsp;</td>
            </tr>
        </table> 
        
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td width="50%" align="center"> {!! QrCode::size(100)->generate($v2[$result]->tgl_pembuatan); !!}</td>
                <td width="40%">
                <div align="center">
                Bandung, {{$v2[$result]->tgl_pembuatan}}  
                <strong><br>
                 Dinas Koperasi Usaha Mikro Kecil Dan Menengah Selaku <br>
                  Kuasa Pengguna Anggaran</strong>
                <br />
                <br />
                <br />
                <br />
                <br />
                
                <u><strong>Drs. ATET DEDI HANDIMAN</strong></u><br />
                Pembina Utama Muda <br>
                NIP. 19611210 199303 1 006
                </div>
                </td>
                <td width="10%">
            </tr>
            </table>
            @endforeach
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    
</section>

@endsection