
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta http-equiv="imagetoolbar" content="no" />
<meta http-equiv="CACHE-CONTROL" CONTENT="NO-CACHE">
<meta http-equiv="PRAGMA" CONTENT="NO-CACHE">
<meta http-equiv="EXPIRES" CONTENT="Fri, Jan 01 1900 00:00:00 GMT">
<title>e-Contract Kota Bandung ~ Cetak DokumenCetak Pelaksanaan Pengadaan Barang / Jasa</title>
<link rel="stylesheet" href="../../dist/css/cetak.css" type="text/css" />
<link href="assets/img/logo.ico" rel="shortcut icon" />
</head>

<body onload="window.print()">
<table width="100%" border="0" cellspacing="0" cellpadding="2">
  <tr>
    <td align="center" width="10%"><img src="assets/img/logo.png" width="100" height="72" /></td>
    <td align="center" width="75%">
    <h2 class="nomargin">PEMERINTAH KOTA BANDUNG<br />
        Dinas Koperasi Usaha Mikro Kecil Dan Menengah</h2>
	Jl. Kawaluyaan No. 2 Bandung Telp./Fax  Bandung
    </td>
    <td align="right" width="15%">
    	<img src="assets/img/birms.png" style="float: left;"/><img src="dok_econtract/qrcodetemp/20190924.1021198.675e0.png" style="float: left;" /><br><img src="imagecode.php?code=20190924.1021198.675e0" style="float: left;" />
    </td>
  </tr>
</table>
<hr noshade="noshade"/>
<br />
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="9%">&nbsp;</td>
    <td width="2%">&nbsp;</td>
    <td width="46%">&nbsp;</td>
    <td colspan="2">Bandung, 18 November 2019<br />
    <br /></td>
    <td width="2%">&nbsp;</td>
  </tr>
  <tr>
    <td>Nomor</td>
    <td>:</td>
    <td>027/105/24.03/KPA.AG/Dis.KUMKM/XI/2019</td>
    <td width="3%">&nbsp;</td>
    <td width="38%">Kepada</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td valign="top">Sifat<br />
      Lampiran<br />
      Perihal</td>
    <td valign="top">:<br />
      :<br />
      :</td>
    <td valign="top">Biasa<br />
     1 ( satu ) Lembar<br />
      <strong><u><em>Pelaksanaan Pengadaan Barang/ Jasa</em></u></strong></td>
    <td valign="top">Yth.  </td>
    <td valign="top"> 
    Pejabat Pembuat Komitmen<br />
    Fasilitasi pengembangan pendampingan dan kemitraan usaha pelaku usaha mikro
    <br />
di &mdash;<br /><br />
B A N D U N G</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center"><strong></strong></td>
    <td>&nbsp;</td>
  </tr>
</table>
<br />
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="10%">&nbsp;</td>
    <td width="90%" align="justify"><p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sehubungan dengan Nota Dinas dari Pejabat Pelaksana Teknis Kegiatan Fasilitasi pengembangan pendampingan dan kemitraan usaha pelaku usaha mikro  nomor 027/082/24.03/PPTK.NN/Dis.KUMKM/XI/2019 tanggal 18 November 2019, perihal sebagaimana tersebut di atas, bersama ini kami minta saudara segera melaksanakan proses pengadaan barang/ jasa tersebut sesuai ketentuan peraturan perundangan yang berlaku untuk pelaksanaan :    
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody>
          <tr>
            <td width="22%">Program</td>
            <td width="2%">:</td>
            <td width="76%">Pemberdayaan UMKM</td>
          </tr>
          <tr>
            <td>Kegiatan</td>
            <td>:</td>
            <td>Fasilitasi pengembangan pendampingan dan kemitraan usaha pelaku usaha mikro</td>
          </tr>
          <tr>
            <td>Pekerjaan</td>
            <td>:</td>
            <td>Makanan Minuman Kegiatan &mdash; Pesta Dagang UMKM 2019</td>
          </tr>
          <tr>
            <td>Lokasi</td>
            <td>:</td>
            <td>Jl. Kawaluyaan No. 2</td>
          </tr>
          <tr>
            <td>Pagu Anggaran</td>
            <td>:</td>
            <td>Rp. 29.615.000,-</td>
          </tr>
          <tr>
            <td>DPA Perangkat Daerah</td>
            <td>:</td>
	        <td>2.11.01.01</td>
          </tr>
          <tr>
            <td>Kode Rekening</td>
            <td>:</td>
            <td>2.11.2.11.01.01.24.03.5.2.2.11.05</td>
          </tr>
          <tr>
            <td>Sumber Biaya</td>
            <td>:</td>
            <td><strong>Anggaran Belanja APBD PEMERINTAH KOTA BANDUNG</strong></td>
          </tr>
          <tr>
            <td>Tahun Anggaran</td>
            <td>:</td>
	        <td>2019</td>
          </tr>
        </tbody>
    </table>
    <p>Adapun kebutuhan barang/ jasa sebagaimana dimaksud adalah sebagai berikut :</p>
	<table width="100%" border="1" cellspacing="0" cellpadding="2" class="grid">
							<thead>
								<tr>
									<th colspan="3">No.</th>
									<th>Uraian Barang / Jasa</th>
									<th>Kuantitas</th>
									<th>Satuan</th>
								</tr>
							</thead>
						<tbody>
							<tr valign="top">
								<td style="border-width: 1px 0px 0px 0px;">5.2.2.11.05&nbsp;</td>
								<td style="border-width: 1px 0px 0px 0px;">&nbsp;</td>
								<td style="border-width: 1px 0px 0px 0px;">&nbsp;</td>
								<td>Belanja Makanan dan Minuman Kegiatan&nbsp;</td>
								<td align="center">&nbsp;</td>
								<td>&nbsp;</td>
							</tr><tr valign="top">
								<td style="border-width: 1px 0px 0px 0px;">&nbsp;</td>
								<td style="border-width: 1px 0px 0px 0px;">-&nbsp;</td>
								<td style="border-width: 1px 0px 0px 0px;">&nbsp;</td>
								<td>Makanan dan Minuman Kegiatan&nbsp;</td>
								<td align="center">&nbsp;</td>
								<td>&nbsp;</td>
							</tr><tr valign="top">
								<td style="border-width: 1px 0px 0px 0px;">&nbsp;</td>
								<td style="border-width: 1px 0px 0px 0px;">&nbsp;</td>
								<td style="border-width: 1px 0px 0px 0px;">1&nbsp;</td>
								<td>Nasi Dus&nbsp;</td>
								<td align="center">310&nbsp;</td>
								<td>Dus&nbsp;</td>
							</tr><tr valign="top">
								<td style="border-width: 1px 0px 0px 0px;">&nbsp;</td>
								<td style="border-width: 1px 0px 0px 0px;">&nbsp;</td>
								<td style="border-width: 1px 0px 0px 0px;">2&nbsp;</td>
								<td>Snack&nbsp;</td>
								<td align="center">550&nbsp;</td>
								<td>Dus&nbsp;</td>
							</tr></tbody>
			</table>
			
	</td>
  </tr>
</table>
<br />
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td width="60%" align="center">&nbsp;</td>
    <td width="40%">
	<div align="center">
	<strong><br>
 	Dinas Koperasi Usaha Mikro Kecil Dan Menengah Selaku <br>
  	Kuasa Pengguna Anggaran</strong>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <u><strong>Drs. Ahmad Guntara, MM</strong></u><br />
    NIP. 19611210 199303 1 006
    </div>
    </td>
</tr>
</table>
<p><strong><em><u>Tembusan:</u></em></strong><br>
  Yth: <ol>
  		<li></li>
  		<li></li>
  		<li></li>
  </ol>
</p>
</body>
</html>
