@extends('layouts.app')

@section('title', '')

@section('content')


<section class="content">
    <div class="container-fluid">
        <div class="card bg-gradient-danger">
            <div class="card-header">
              <h3 class="card-title">LEVEL AKSES</h3>
            </div>
            <div class="card-body">
              ANDA TIDAK MENDAPATKAN IZIN UNTUK MENGAKSES HALAMAN INI
            </div>
            <!-- /.card-body -->
        </div>
    </div>
    
</section>

@endsection

@push('scripts')


@endpush