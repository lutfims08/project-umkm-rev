@extends('layouts.app')

@section('title', 'Proses Pembuatan Surat Keterangan')

@section('content')

<section class="content">
    <div class="container-fluid">
         <div class="form-horizontal">
            <div class="box box-default">
                 <div class="card card-primary">
                     <div class="card-header">
                         <h3 class="card-title"></h3>
                         <div class="card-tools">
                             <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                                 <i class="fas fa-minus"></i></button>
                             </button>
                         </div>
                     </div>
                     <form action="/carimonitoring" method="post">
                         <div class="row">
                             <div class="col-md-6">                           
                                 <div class="card-body">
                                     <div class="form-group">
                                     <label for="nik">NIK</label>
                                     <input type="text" class="form-control" id="nik" name="nik" placeholder="NIK">
                                     </div>
                                     <div class="form-group">
                                     <label for="nama_dpn">Nama Pemilik</label>
                                     <input type="text" class="form-control" id="nama_dpn" name="nama_dpn" placeholder="Nama Pemilik">
                                     </div>
                                     <div class="form-group">
                                     <label for="nama_usaha">Nama Usaha</label>
                                     <input type="text" class="form-control" id="nama_usaha" name="nama_usaha" placeholder="Nama Usaha">
                                     </div>
                                     <div class="form-group">
                                         <div class="row">
                                            <div class="col-sm-6">
                                        <label  for="exampleInputPassword1">Tanggal Surat Permohonan</label>
                                            
                                                <div class="input-group" id="tglpermohonan" data-target-input="nearest">
                                                    <input type="text" name="tgl_permohonan" id="tgl_permohonan" class="form-control datetimepicker-input @error('tgl_permohonan') is-invalid @enderror" 
                                                        data-target="#tglpermohonan" placeholder="Tanggal Permohonan " 
                                                        id="tglpermohonan" value="{{ old('tgl_permohonan') }}"/> 
                                                    <div class="input-group-append" data-target="#tglpermohonan" data-toggle="datetimepicker">
                                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                    </div>
                                                    <div class="invalid-tooltip">
                                                Silihkan Pilih Tahun
                                            </div>  
                                                </div>
                                            </div>
                                            <div class="col-sm-">
                                        <label  for="exampleInputPassword1">Tanggal Surat Pembuatan</label>
                                            
                                                <div class="input-group" id="tglpembuatan" data-target-input="nearest">
                                                    <input type="text" name="tgl_pembuatan" id="tgl_pembuatan" class="form-control datetimepicker-input @error('tgl_pembuatan') is-invalid @enderror" 
                                                        data-target="#tglpembuatan" placeholder="Tanggal Permohonan " 
                                                        id="tglpembuatan" value="{{ old('tgl_pembuatan') }}"/> 
                                                    <div class="input-group-append" data-target="#tglpembuatan" data-toggle="datetimepicker">
                                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                    </div>
                                                    <div class="invalid-tooltip">
                                                Silihkan Pilih Tahun
                                            </div>  
                                                </div>
                                            </div>
                                        </div>  
                                    </div>
                                     
                                     <div class="card-footer">
                                         <button type="button" id="caridata" class="btn btn-primary detail" data-toggle="modal" data-target="#modal-default">Submit</button>
                                         
                                     </div>
                                     
                                 </div>
                             </div>
                             <div class="col-md-6">                           
                                 <div class="card-body">
                                     <div class="callout callout-danger">
                                         <h5>Hal yang harus diperhatikan :</h5>      
                                         
                                         <ol>
                                             <li>Silahkan inputkan Data Profile UMKM Terlebih Dahulu</li>
                                             <li>Isikan NIK KTP, Nama Pemilik, dan Nama Usaha</li>
                                             <li>Masukan Tanggal Surat Permohanan</li>
                                             <li>Masukan Tanggal Pembuatan Surat Keterangan</li>

                                         </ol>              
                                         
                                       </div> 
                                 </div>
                             </div>
                         </div>
                         
                     </form>
                      
                     
                     
 
                 </div>
            </div>
         </div>
    </div>
    <div class="modal fade" id="modal-default">
         <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
               <h4 class="modal-title">Data UMKM</h4>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                 <span aria-hidden="true">&times;</span>
               </button>
             </div>
             <div class="modal-body">
               
               
             </div>
             
           </div>
           <!-- /.modal-content -->
         </div>
         <!-- /.modal-dialog -->
       </div>
 </section>

@endsection

@push('scripts')
<script>
    
    $(function () {
     
  
      $('#tglpermohonan').datetimepicker({
         locale:'id',
          format: 'D MMMM YYYY'
      });

      $('#tglpembuatan').datetimepicker({
        locale:'id',
         format: 'D MMMM YYYY'
     });
     
  
    })
  </script>
  
  <script type="text/javascript">
  

    $(document).on('click','.detail',function(e){
      var nik = $("#nik").val();
      var depan = $("#nama_dpn").val();
      var usaha = $("#nama_usaha").val();
      var permohonan = $("#tgl_permohonan").val();
      var pembuatan = $("#tgl_pembuatan").val();
      e.preventDefault();
      $("#modal-default").modal('show');
      $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
        });
     
      $.post("/skdata",
          {
          nik:nik,
          nama_dpn: depan,
          nama_usaha: usaha,
          tgl_permohonan:permohonan,
          tgl_pembuatan:pembuatan
          },
          function(data,html){
            $(".modal-body").html(data);
          });
      
  });
</script>

@endpush