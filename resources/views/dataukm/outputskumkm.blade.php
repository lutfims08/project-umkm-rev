<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>


<style type="text/css">
.media all
    {
        .page-break {display:none;}
    }
.media print
    {
        .page-break {display:block; page-break-before:always; }
    }
    .thinborder {
        border: thin solid #000;border-width: 1px 1px 1px 1px;
    }
    .thinleftborder {
        border: thin solid #000;border-width: 1px 1px 1px 0px;
    }
    .thintopborder {
        border: thin solid #000;border-width: 0px 1px 1px 1px;
    }
    .thintopleftborder {
        border: thin solid #000;border-width: 0px 1px 1px 1px;
    }
    .thinbottomborder {
        border: thin solid #000;border-width: 1px 1px 1px 0px;
    }
    .mediumborder {
        border: medium solid #000;border-width: 1px 1px 1px 1px;
    }
.reg2 {
	text-align: right;
	font-size: 9px;
}
.reg {
	font-size: 9px;
}
.logo {
	text-align: center;
}
.title {
	text-align: center;
	font-size: 22px;
	font-weight: bold;
	font-family: Tahoma, Geneva, sans-serif;
}
.title2 {
	text-align: center;
	font-family: Tahoma, Geneva, sans-serif;
	font-size: 18px;
	font-weight: bold;
}
.title3 {
	text-align: center;
	font-size: 16px;
}
.hr {
        color:#000;
        height: 1.5px;
        width: 100%;
    }
.head1 {
	text-align: center;
	font-size: 20px;
	font-family: "Bookman Old Style";
	font-weight: bold;
}

.head2 {
	text-align: center;
	font-family: "Bookman Old Style";
	font-size: 16px;
}
.body1 {
	font-family: "Bookman Old Style";
}
.body12 {
	font-family: "Bookman Old Style";
	font-size: 16px;
	text-align: justify;
}
.body31 {
	font-size: 14px;
}
.body31 {
	font-family: "Bookman Old Style";
}
.body41 {
	font-size: 14px;
	text-align: justify;
}
.body41 {
	font-size: 14px;
	text-align: justify;
}
.body41 {
	text-align: justify;
	font-size: 16px;
}
.ttd {
	font-size: 14px;
}
.ttd {
	font-size: 14px;
}
.ttd {
	text-align: center;
}
.ttd {
	text-align: center;
}
.ttd {
	text-align: center;
}
.ttd {
	text-align: center;
}
.ttd {
	text-align: center;
	font-size: 16px;
}
.ttd1 {
	text-align: center;
}
.bobyisi {
	font-size: 16px;
}
</style>
</head>
<body onload="window.print()">
<table width="100%" border="0">
  <tr>
    <td width="18%" class="reg">Register By SID UMKM</td>
    <td colspan="2" class="reg2">Dinas KUMKM Kota Bandung</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td width="84%">&nbsp;</td>
    <td width="4%">&nbsp;</td>
  </tr>
  <tr>
    <td rowspan="3" class="logo"><img src="../../dist/img/logo_sk.png" width="89" height="76" /></td>
    <td height="30" class="title">PEMERINTAH KOTA BANDUNG</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td height="30" class="title2">DINAS KOPERASI USAHA MIKRO KECIL DAN MENENGAH</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td height="30" class="title3">Jl. Kawaluyaan No. 2  Telp &amp; Fax (022) 7308358 Bandung</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3">
   		<hr noshade="noshade"/>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  @foreach($v1 as $result => $result1)
  <tr>
    <td colspan="3" class="head1">SURAT KETERANGAN</td>
  </tr>
  <tr>
    <td colspan="3" class="head2"><p>Nomor :    KM.01.04  /    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; /Dis-KUMKM/&nbsp;&nbsp;&nbsp;&nbsp; /&nbsp;&nbsp;&nbsp;&nbsp;</p></td>
  </tr>
</table>
<br />
<table width="100%" border="0">
  <tr>
    <td width="4%">&nbsp;</td>
    <td width="7%">&nbsp;</td>
    <td width="23%">&nbsp;</td>
    <td width="3%">&nbsp;</td>
    <td width="59%">&nbsp;</td>
    <td width="4%">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="4" class="body12">Sehubungan dengan Surat dari <Strong>{{strtoupper($result1->nama_usaha)}}</Strong> Tanggal  {{$v2[$result]->tgl_permohonan}} Perihal  permohonan Surat Keterangan UKM Binaan, dengan ini kami menerangkan sebagai  berikut :</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td class="body31">&nbsp;</td>
    <td class="body31">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td class="bobyisi">Nama</td>
    <td class="bobyisi">:</td>
    <td class="bobyisi">{{strtoupper($result1->nama_depan)}}</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td class="bobyisi">NIK</td>
    <td class="bobyisi">:</td>
    <td class="bobyisi">{{strtoupper($result1->nik)}}</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td class="bobyisi">Jabatan</td>
    <td class="bobyisi">:</td>
    <td class="bobyisi">Pemilik</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td class="bobyisi">Jenis Usaha </td>
    <td class="bobyisi">:</td>
    <td class="bobyisi">{{$result1->jenis_usaha}} ( {{$result1->produk_1}} )</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td class="bobyisi">Alamat Usaha</td>
    <td class="bobyisi">:</td>
    <td class="bobyisi">{{$result1->alamat_usaha}}</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td class="bobyisi">No Tlp / HP ( WA )</td>
    <td class="bobyisi">:</td>
    <td class="bobyisi">{{$result1->no_handphone}}</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="4" class="body41">Berdasarkan Undang-Undang Republik Indonesia  Nomor 20 tahun 2008 tentang Usaha Mikro Kecil Menengah bahwa yang bersangkutan  adalah termasuk kriteria Usaha Mikro (Profil UMKM terlampir) dan merupakan Binaan  Dinas Koperasi Usaha Mikro Kecil dan Menengah Kota Bandung dibidang<strong> {{strtoupper($result1->jenis_usaha)}} </strong></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="4" class="body41">Surat Keterangan ini berlaku 1 (satu) Tahun  sejak tanggal dikeluarkan.</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="4" class="body41">Demikian Surat Keterangan ini dibuat, untuk  dipergunakan sebagaimana mestinya.</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
<table width="100%" border="0">
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td class="ttd">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td width="9%">&nbsp;</td>
    <td width="29%">&nbsp;</td>
    <td width="3%">&nbsp;</td>
    <td width="49%" class="ttd">Bandung, {{$v2[$result]->tgl_permohonan}}</td>
    <td width="10%">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td rowspan="7" class="ttd1">{!! QrCode::size(100)->generate( "umkmbandung.online/view/0/sk/umkm/out/".$result1->id_anggota);!!}</td>
    <td>&nbsp;</td>
    <td class="ttd">KEPALA DINAS KOPERASI, USAHA  MIKRO,</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td class="ttd">KECIL DAN MENENGAH</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td class="ttd"><strong><u>Drs.  ATET DEDI HANDIMAN</u></strong></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td class="ttd">Pembina Utama Muda</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td class="ttd">NIP. 19640225 199303 1 004</td>
    <td>&nbsp;</td>
  </tr>
</table>
<p>&nbsp;</p>
@endforeach
</body>
</html>
