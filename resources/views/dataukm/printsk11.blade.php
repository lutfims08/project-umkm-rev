<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 3 | Invoice Print</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Bootstrap 4 -->

  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">

  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <style>
    body, table {
        margin:0;
        padding:0;
        font-family: 'Bookman Old Style';
        font-size: 22px;
        color: #000;
    }
    
    @media all
    {
        .page-break {display:none;}
    }
    
    @media print
    {
        .page-break {display:block; page-break-before:always; }
    }
    .thinborder {
        border: thin solid #000;border-width: 1px 1px 1px 1px;
    }
    .thinleftborder {
        border: thin solid #000;border-width: 1px 1px 1px 0px;
    }
    .thintopborder {
        border: thin solid #000;border-width: 0px 1px 1px 1px;
    }
    .thintopleftborder {
        border: thin solid #000;border-width: 0px 1px 1px 1px;
    }
    .thinbottomborder {
        border: thin solid #000;border-width: 1px 1px 1px 0px;
    }
    .mediumborder {
        border: medium solid #000;border-width: 1px 1px 1px 1px;
    }
    
    .title {
        font-family: Arial, Helvetica, sans-serif;
        font-size:14px; 
        font-weight:bold;
    }
    .subtitle {
        font-family: Arial, Helvetica, sans-serif;
        font-size:12px; 
        font-weight:bold;
    }
    fieldset {
        display: block;
        margin:0;
        padding:0;
    }
    
    fieldset legend {
        font-weight: bold;
        font-size: 13px;
        padding-right: 10px;
        color: #999;
        margin:0;
        padding:0;
    }
    
    fieldset form {
        padding-top: 15px;
        margin:0;
        padding:0;
    }
    
    fieldset p label {
        float: left;
        width: 200px;
        margin:0;
        padding:0;
    }
    
    table.grid{
        border-collapse:collapse;
        border: thin solid #000;
    }
    table.grid th{
        padding-top:1mm;
        padding-bottom:1mm;
    }
    table.grid th{
        background: #F0F0F0;
        border-top: 0.3mm solid #000;
        border-bottom: 0.2mm solid #000;
        text-align:center;
        padding-left:0.2cm;
    }
    table.grid tr td{
        padding-top:0.5mm;
        padding-bottom:0.5mm;
        padding-left:2mm;
        border-bottom:0.2mm solid #000;
    }
    .nomargin {
        margin-bottom: 0; 
        padding-bottom: 0;
    }
    hr {
        color:#000;
        height: 0.1px;
        width: 100%;
    }
</style>
</head>

<body>
<div class="wrapper">
  <!-- Main content -->
  <section class="invoice">
    <!-- title row -->
    <div class="row">
        <div class="col-12">
          <h4>
            <i class="fas fa-globe"></i> AdminLTE, Inc.
            <small class="float-right">Date: 2/10/2014</small>
          </h4>
          
        </div>
        <!-- /.col -->
      </div>
    <!-- info row -->
    <div class="row invoice-info">
      <div class="col-sm-12 invoice-col">
        <table width="100%" border="0" cellspacing="0" cellpadding="4">        
              <tr>
                    <td align="center" width="10%"><img src="../../dist/img/logo_sk.png" width="100" height="80 " /></td>
                    <td align="center" width="75%">
                    <h2 class="nomargin">PEMERINTAH KOTA BANDUNG<br /></h2>
                    <h3>DINAS KOPERASI USAHA MIKRO KECIL DAN MENENGAH</h3> 
                    <h4> Jl. Kawaluyaan No. 2 Bandung Telp./Fax (022) 7308358 Bandung </h4>
                    </td>
              </tr>
                          
          </table>
          <hr noshade="noshade"/>        
      </div>
    </div>
    <!-- /.row -->
    <br />
    @foreach($v1 as $result => $result1)
    <!-- Table row -->
    <div class="row">
      <div class="col-12 table-responsive">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td width="100%" align="center"><h5> SURAT KETERANGAN </h5></td>
            </tr>
            <tr>
                <td width="100%" align="center"><p>Nomor :    KM.01.04  /    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; /Dis-KUMKM/&nbsp;&nbsp;&nbsp;&nbsp; /&nbsp;&nbsp;&nbsp;&nbsp;</p></td>
            </tr>
                           
              
        </table>
        <br>
        
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="5%">&nbsp;</td>
              <td width="82%" align="justify"><p>Sehubungan dengan Surat dari <Strong>{{strtoupper($result1->nama_usaha)}}</Strong> Tanggal  {{$v2[$result]->tgl_permohonan}} Perihal permohonan Surat Keterangan UKM Binaan, dengan ini kami menerangkan sebagai berikut :    
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tbody>
                    <tr>
                      <td width="30%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nama </td>
                      <td width="5%">:</td>
                      <td width="55%">{{strtoupper($result1->nama_depan)}}</td>
                    </tr>
                    <tr>
                        <td width="30%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NIK </td>
                        <td width="5%">:</td>
                        <td width="55%">{{strtoupper($result1->nik)}}</td>
                    </tr>
                    <tr>
                        <td width="30%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;JABATAN </td>
                        <td width="5%">:</td>
                        <td width="55%">Pemilik</td>
                    </tr>
                    <tr>
                        <td width="30%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;JENIS USAHA </td>
                        <td width="5%">:</td>
                        <td width="55%">{{$result1->jenis_usaha}} ( {{$result1->produk_1}} )</td>
                    </tr>
                    <tr>
                        <td width="30%" style="vertical-align:top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ALAMAT USAHA </td>
                        <td width="5%" style="vertical-align:top">:</td>
                        <td width="55%"> {{$result1->alamat_usaha}}</td>
                    </tr>
                    <tr>
                        <td width="30%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NOMOR TELP/HP </td>
                        <td width="5%">:</td>
                        <td width="55%"> {{$result1->no_handphone}}</td>
                    </tr>
                    
                  </tbody>
                </table>
              <td width="10%">&nbsp;</td>
            </tr>
            <tr>
                <td width="10%">&nbsp;</td>
                <td width="80%" align="justify"></td>   
                  
                <td width="5%">&nbsp;</td>
            </tr>
            <tr>
                <td width="10%">&nbsp;</td>
                <td width="80%" align="justify"><p>Berdasarkan Undang-Undang Republik Indonesia Nomor 20 tahun 2008 tentang Usaha Mikro Kecil Menengah bahwa yang bersangkutan adalah termasuk kriteria Usaha Mikro (Profil UMKM terlampir) dan merupakan Binaan Dinas Koperasi Usaha Mikro Kecil dan Menengah Kota Bandung dibidang <strong> {{strtoupper($result1->jenis_usaha)}}</strong>.</td>    
                  
                <td width="5%">&nbsp;</td>
            </tr>
            <tr>
                <td width="10%">&nbsp;</td>
                <td width="80%" align="justify"><p>Surat Keterangan ini berlaku 1 (satu) Tahun sejak tanggal dikeluarkan.</td>
                  
                <td width="5%">&nbsp;</td>
            </tr>
            <tr>
                <td width="10%">&nbsp;</td>
                <td width="80%" align="justify"><p>Demikian Surat Keterangan ini dibuat, untuk dipergunakan sebagaimana mestinya.</td>
                  
                <td width="5%">&nbsp;</td>
            </tr>
        </table> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td width="45%" align="center"> {!! QrCode::size(100)->generate( "umkmbandung.online/view/0/sk/umkm/out/".$result1->id_anggota);!!}</td>
                <td width="40%">
                <div align="center">
                Bandung, {{$v2[$result]->tgl_pembuatan}}  
                <strong><br>
                 Dinas Koperasi Usaha Mikro Kecil Dan Menengah Selaku <br>
                  Kuasa Pengguna Anggaran</strong>
                <br />
                <br />
                <br />
                <br />
                <br />
                
                <u><strong>Drs. ATET DEDI HANDIMAN</strong></u><br />
                Pembina Utama Muda <br>
                NIP. 19611210 199303 1 006
                </div>
                </td>
                <td width="15%">
            </tr>
            </table>
            @endforeach
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    
</section>
  <!-- /.content -->
</div>
<!-- ./wrapper -->

<script type="text/javascript"> 
  window.addEventListener("load", window.print());
</script>
</body>
</html>
