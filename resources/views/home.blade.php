@extends('layouts.app')
@section('title','Database UMKM || Dinas KUMKM ')

@section('content')

@if (session('statustambah'))
    <div class="alert alert-success">
        {{ session('statustambah') }}
    </div>
@endif
    <div class="container-fluid">
        <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3>{{$total}}</h3>

                <p>Unit Usaha</p>
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
              <a href="/binaans/" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>{{$mikro}}</h3>

                <p>Usaha Mikro</p>
              </div>
              <div class="icon">
              
                <i class="ion ion-stats-bars"></i>
              </div>
              <a href="/binaans/{{'Mikro'}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
                <h3>{{$kecil}}</h3>

                <p>Usaha Kecil </p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
              <a href="/binaans/{{'Kecil'}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-danger">
              <div class="inner">
                <h3>{{$menengah}}</h3>

                <p>Usaha Menengah</p>
              </div>
              <div class="icon">
                <i class="ion ion-pie-graph"></i>
              </div>
              <a href="/binaans/{{'Menengah'}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
        </div>
        <div class="row">
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box">
              <span class="info-box-icon bg-info elevation-1"><i class="fas fa-cog"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Non Aktif</span>
                <span class="info-box-number">
                {{$totalnon}}
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-thumbs-up"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Usaha Mikro Non Aktif</span>
                <span class="info-box-number">
                {{$mikro1}}
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->

          <!-- fix for small devices only -->
          <div class="clearfix hidden-md-up"></div>

          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-success elevation-1"><i class="fas fa-shopping-cart"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Usaha Kecil Non Aktif</span>
                <span class="info-box-number">
                {{$kecil1}}
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-users"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Usaha Kecil Non Aktif</span>
                <span class="info-box-number">
                {{$menengah1}}
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
        </div>

    </div>
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-6">
            <!-- AREA CHART -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Aktivitas Usaha</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                  {{--  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>  --}}
                </div>
              </div>
              <div class="card-body">
                <div class="chart">
                  <canvas id="stackedBarChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            
          </div>
          <!-- /.col (LEFT) -->
          <div class="col-md-6">
           
            <!-- BAR CHART -->
            <div class="card card-success">
              <div class="card-header">
                <h3 class="card-title">Tahun Pendaftaran Berdasarkan Klasifikasi Usaha</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                  {{--  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>  --}}
                </div>
              </div>
              <div class="card-body">
                <div class="chart">
                  <canvas id="barChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            
          </div>
          <!-- /.col (RIGHT) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>   
    
   
@endsection

@push('scripts')
<script>
  $(function () {
    /* ChartJS
     * -------
     * Here we will create a few charts using ChartJS
     */

    //--------------
    //- AREA CHART -
    //--------------

    // Get context with jQuery - using jQuery's .get() method.
    

    var areaChartData = {
      labels  : [<?php foreach($sqlthn as $dataT) { echo '"' . $dataT->thndaftar . '",';} ?>],
      datasets: [
        {
          label               : 'Kuliner',
          backgroundColor     : 'rgba(86, 225, 71, 1)',
          borderColor         : 'rgba(86, 225, 71, 1)',
          pointRadius          : false,
          pointColor          : '#3b8bba',
          pointStrokeColor    : 'rgba(86, 225, 71, 1)',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(86, 225, 71, 1)',
          data                : [<?php foreach($sqlmakanan as $dataT) { echo '"' . $dataT->jenisUsaha . '",';} ?>]
        },
        {
          label               : 'Fashion',
          backgroundColor     : 'rgba(225, 189, 71, 1)',
          borderColor         : 'rgba(225, 189, 71, 1)',
          pointRadius         : false,
          pointColor          : 'rgba(225, 189, 71, 1)',
          pointStrokeColor    : '#c1c7d1',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(225, 189, 71, 1)',
          data                : [<?php foreach($sqlfashion as $dataT) { echo '"' . $dataT->jenisUsaha . '",';} ?>]
        },
        {
          label               : 'Handicraft',
          backgroundColor     : 'rgba(71, 212, 225, 1)',
          borderColor         : 'rgba(71, 212, 225, 1)',
          pointRadius         : false,
          pointColor          : 'rgba(71, 212, 225, 1)',
          pointStrokeColor    : '#c1c7d1',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(71, 212, 225, 1)',
          data                : [<?php foreach($sqlhandicraft as $dataT) { echo '"' . $dataT->jenisUsaha . '",';} ?>]
        },
        {
          label               : 'Jasa',
          backgroundColor     : 'rgba(225, 71, 76, 1)',
          borderColor         : 'rgba(225, 71, 76, 1)',
          pointRadius         : false,
          pointColor          : 'rgba(225, 71, 76, 1)',
          pointStrokeColor    : '#c1c7d1',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(225, 71, 76, 1)',
          data                : [<?php foreach($sqljasa as $dataT) { echo '"' . $dataT->jenisUsaha . '",';} ?>]
        },
        {
          label               : 'Perdagangan',
          backgroundColor     : 'rgba(84, 71, 225, 1)',
          borderColor         : 'rgba(84, 71, 225, 1)',
          pointRadius         : false,
          pointColor          : 'rgba(84, 71, 225, 1)',
          pointStrokeColor    : '#c1c7d1',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(84, 71, 225, 1)',
          data                : [<?php foreach($sqldagang as $dataT) { echo '"' . $dataT->jenisUsaha . '",';} ?>]
        },
        {
          label               : 'Lain - Lainnya',
          backgroundColor     : 'rgba(225, 114, 71, 1)',
          borderColor         : 'rgba(225, 114, 71, 1)',
          pointRadius         : false,
          pointColor          : 'rgba(225, 114, 71, 1)',
          pointStrokeColor    : '#c1c7d1',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(225, 114, 71, 1)',
          data                : [<?php foreach($sqldagang1 as $dataT) { echo '"' . $dataT->jenisUsaha . '",';} ?>]
        },
      ]
    }

    //-------------
    //- BAR CHART -
    //-------------
    var barChartCanvas = $('#barChart').get(0).getContext('2d')
    var barChartData = jQuery.extend(true, {}, areaChartData)
    var temp0 = areaChartData.datasets[0]
    var temp1 = areaChartData.datasets[1]
    barChartData.datasets[0] = temp1
    barChartData.datasets[1] = temp0

    var barChartOptions = {
      responsive              : true,
      maintainAspectRatio     : false,
      datasetFill             : false
    }

    var barChart = new Chart(barChartCanvas, {
      type: 'bar', 
      data: barChartData,
      options: barChartOptions
    })



    var aktivitas = {
      labels  : [<?php foreach($sqlthn as $dataT) { echo '"' . $dataT->thndaftar . '",';} ?>],
      datasets: [
        {
          label               : 'AKTIF',
          backgroundColor     : 'rgba(60,141,188,1)',
          borderColor         : 'rgba(60,141,188,1)',
          pointRadius          : true,
          pointColor          : '#3b8bba',
          pointStrokeColor    : 'rgba(60,141,188,1)',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(60,141,188,1)',
          data                : [<?php foreach($sqlthn as $dataT) { echo '"' . $dataT->jumlahthndaftar . '",';} ?>]
        },
        
        
      ]
    }
    //---------------------
    //- STACKED BAR CHART -
    //---------------------
    var stackedBarChartCanvas = $('#stackedBarChart').get(0).getContext('2d')
    var stackedBarChartData = jQuery.extend(true, {}, aktivitas)
    var resultaktif = aktivitas.datasets[0]
    //var resultnonaktif = aktivitas.datasets[1]
    //stackedBarChartData.datasets[0] = resultnonaktif
    stackedBarChartData.datasets[0] = resultaktif

    var stackedBarChartOptions = {
      responsive              : true,
      maintainAspectRatio     : false,
      scales: {
        xAxes: [{
          stacked: true,
        }],
        yAxes: [{
          stacked: true
        }]
      }
    }

    var stackedBarChart = new Chart(stackedBarChartCanvas, {
      type: 'bar', 
      data: stackedBarChartData,
      options: stackedBarChartOptions
    })
  })
</script>


@endpush
