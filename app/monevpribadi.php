<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class monevpribadi extends Model
{
    protected $table = 'monev_pribadi';
    public $incrementing = false;
    public $primaryKey = 'id_anggota';
    protected $fillable =['id_anggota','nik','nama_depan','jenis_kelamin','alamat',
    'rt','rw','no_telepon','no_handphone','email','id_kelurahan','id_kecamatan','kota',
    'kode_pos','pendidikan','aktifasi','created_at'];
}
