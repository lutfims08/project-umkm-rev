<?php

namespace App;

use App\Usaha;
use App\kecamatanmodel;
use Illuminate\Database\Eloquent\Model;

class Pribadi extends Model
{
    protected $table = 'db_pribadi';
    protected $fillable =['id_anggota','nik','nama_depan','jenis_kelamin','alamat','rt','rw','no_telepon','no_handphone','email','id_kelurahan','id_kecamatan','kota','kode_pos','pendidikan','aktifasi','tanggal_input'];
    public $incrementing = false;
    public $primaryKey = 'id_anggota';
    public function Usaharelasi()
    {
        // $Pribadi = Usaha::get();
        return $this->hasManyThrough(
             'App\Usaha','App\kecamatanmodel',
            'id_kecamatan', 'id_anggota', 'id_anggota'
        );
        
    }

    public function Kecamatanrelasi()
    {
        // $Pribadi = Usaha::get();
        return $this->hasMany(kecamatanmodel::class, 'id_kecamatan');
        
    }
    


}
