<?php

namespace App\DataTables;

use App\Bpum;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class BansosDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', '');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Bansos $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Bpum $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
        ->setTableId('users-table')
        ->columns($this->getColumns())
        ->minifiedAjax()
        ->dom('Bfrtip')
        ->orderBy(0)
        ->buttons(
            
            Button::make('export'),
            Button::make('print'),
            
            Button::make('reload')
        );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::computed('action')
                  ->exportable(true)
                  ->printable(true)
                  ->width(60)
                  ->addClass('text-center'),
            Column::make('nik'),
            Column::make('nama_lengkap'),
            Column::make('alamat'),
            Column::make('kelurahan'),
            Column::make('kecamatan'),
            Column::make('bidang_usaha'),
            Column::make('no_telp'),
            Column::make('no_reg'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Bansos_' . date('YmdHis');
    }
}
