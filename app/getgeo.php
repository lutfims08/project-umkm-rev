<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class getgeo extends Model
{
    protected $table = 'db_lokasi_update';
    public $incrementing = false;
    public $primaryKey = 'id_anggota';
    protected $fillable =
    [
        'id_anggota','Latitude','Longitude','created_at'
    ];
}
