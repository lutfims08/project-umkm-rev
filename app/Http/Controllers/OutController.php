<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class OutController extends Controller
{
   
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
       $id=$request->id;

       $v2 = DB::table('db_sk_umkm')->get();
    //    dd($v2);
    foreach ($v2 as $data) {
        if ($data->tandatangan == 'Terbitkan') {
            $v1 = DB::table('db_sk_umkm')
            ->join('db_pribadi', 'db_sk_umkm.id_anggota','=','db_pribadi.id_anggota')
            ->join('db_perusahaan','db_sk_umkm.id_anggota','=','db_perusahaan.id_anggota')
            ->where('db_sk_umkm.id_anggota', '=', $id)
                ->get();
                return view('laporan.outsk',compact('v1'));
        } else {
            $v1 = DB::table('db_pribadi')
      ->join('db_perusahaan', 'db_pribadi.id_anggota','=','db_perusahaan.id_anggota')
      ->join('db_lain', 'db_pribadi.id_anggota','=','db_lain.id_anggota')
      ->join('db_kecamatan', 'db_pribadi.id_kecamatan','=','db_kecamatan.id_kecamatan')
      ->join('db_kelurahan', 'db_pribadi.id_kelurahan','=','db_kelurahan.id_kelurahan')
      ->where('db_pribadi.id_anggota', '=', $id)
        ->get(); 

        // $v1 = DB::table('db_sk_umkm')
        // ->join('db_pribadi', 'db_sk_umkm.id_anggota','=','db_pribadi.id_anggota')
        // ->join('db_perusahaan','db_sk_umkm.id_anggota','=','db_perusahaan.id_anggota')
        // ->where('db_sk_umkm.id_anggota', '=', $id)
        //   ->get();
         
          return view('dataukm.indexskout',compact('v1'));
        }
    }
        
            

       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
