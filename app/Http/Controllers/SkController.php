<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Pribadi;
use App\skumkm;

class SkController extends Controller
{
    public function __construct()
    {
        $this->middleware('verified');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dataukm.skumkm');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id= $request->id_anggota;
        skumkm::create([
            'id_anggota'=>$request->id_anggota,
            'nik'=>$request->nik,
            'nama_depan'=>$request->nama_pemilik,
            'nama_usaha'=>$request->nama_usaha,
            'tgl_permohonan'=>$request->tglmohon,
            'tgl_pembuatan'=>$request->tglbuat
        ]);
        return redirect('/skprintview/'.$id)->with('statustambah', 'Data Berhasil ditambah!!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $v1 = DB::table('db_pribadi')
      ->join('db_perusahaan', 'db_pribadi.id_anggota','=','db_perusahaan.id_anggota')
      ->join('db_lain', 'db_pribadi.id_anggota','=','db_lain.id_anggota')
      ->join('db_kecamatan', 'db_pribadi.id_kecamatan','=','db_kecamatan.id_kecamatan')
      ->join('db_kelurahan', 'db_pribadi.id_kelurahan','=','db_kelurahan.id_kelurahan')
      ->where('db_pribadi.id_anggota', '=', $id)
        ->get();
        $v2 = DB::table('db_sk_umkm')->where('db_sk_umkm.id_anggota','=', $id)->get();
        $qr =\QrCode::size(100)
        ->Style("square",0.5)
        ->generate('A simple example of QR code');
        return view('dataukm.skprint',compact('v1','v2','qr'));
    }

    public function indexshow($id)
    {
        $v1 = DB::table('db_pribadi')
      ->join('db_perusahaan', 'db_pribadi.id_anggota','=','db_perusahaan.id_anggota')
      ->join('db_lain', 'db_pribadi.id_anggota','=','db_lain.id_anggota')
      ->join('db_kecamatan', 'db_pribadi.id_kecamatan','=','db_kecamatan.id_kecamatan')
      ->join('db_kelurahan', 'db_pribadi.id_kelurahan','=','db_kelurahan.id_kelurahan')
      ->where('db_pribadi.id_anggota', '=', $id)
        ->get();
        $v2 = DB::table('db_sk_umkm')->where('db_sk_umkm.id_anggota','=', $id)->get();
        $qr =\QrCode::size(100)
        ->Style("square",0.5)
        ->generate('A simple example of QR code');
        return view('dataukm.outputskumkm',compact('v1','v2','qr'));
    }

    public function testshow($id)
    {
        $v1 = DB::table('db_pribadi')
      ->join('db_perusahaan', 'db_pribadi.id_anggota','=','db_perusahaan.id_anggota')
      ->join('db_lain', 'db_pribadi.id_anggota','=','db_lain.id_anggota')
      ->join('db_kecamatan', 'db_pribadi.id_kecamatan','=','db_kecamatan.id_kecamatan')
      ->join('db_kelurahan', 'db_pribadi.id_kelurahan','=','db_kelurahan.id_kelurahan')
      ->where('db_pribadi.id_anggota', '=', $id)
        ->get();
        $v2 = DB::table('db_sk_umkm')->where('db_sk_umkm.id_anggota','=', $id)->get();
        $qr =\QrCode::size(100)
        ->Style("square",0.5)
        ->generate('A simple example of QR code');
        return view('dataukm.outputskumkm',compact('qr','v1','v2'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    public function showskumkm()
    {
        $v1 = DB::table('db_sk_umkm')->get();

        return view('dataukm.viewskumkm',compact('v1'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        skumkm::where('id_anggota',$id)
            ->update([
                'tandatangan'=>$request->aktivasi,
                'nosurat'=>$request->nosurat
            ]);
            return redirect('/skumkmview')->with('statustambah', 'Surat Keterangan Sudah diterbitkan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        skumkm::destroy($id);
        return redirect('/skumkmview')->with('statustambah', 'Data Berhasil dihapus!!!');
    }

    public function skdatacari1(Request $request)
    {
        $result = DB::table('db_sk_umkm')
                ->where('db_sk_umkm.id_anggota','=',$request->id)
                ->get();
        // dd($result);
        foreach ($result as $data) {
            # code...
        
    ?>

                    <div class="row">
                    <div class="col-md-12">
                    <div class="card card-default">
                    
                    <!-- /.card-header -->
                    <div class="card-body">
                    <form method="POST" action="/updatesk/<?php echo $data->id_anggota;  ?>">
                    

                        <div class="card-body">
                        <div class="form-group row">
                            <label for="exampleInputEmail1" class="col-sm-6 col-form-label">NIK</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="nik" name="nik" value="<?php echo $data->nik;  ?>" readonly>
                                <input type="hidden" class="form-control" id="id_anggota" name="id_anggota" value="<?php echo $data->id_anggota;  ?>" readonly>
                            </div>  
                        </div>
                        <div class="form-group row">
                            <label for="exampleInputEmail1" class="col-sm-6 col-form-label">Nama Pemilik</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="nama_pemilik" name="nama_pemilik" value="<?php echo $data->nama_depan;  ?>" readonly>
                            </div>  
                        </div>
                        <div class="form-group row">
                            <label for="exampleInputEmail1" class="col-sm-6 col-form-label">Nama Usaha</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="nama_usaha" name="nama_usaha" value="<?php echo $data->nama_usaha;  ?>" readonly>
                            </div>  
                        </div>
                        
                        <div class="form-group row">
                            <label for="exampleInputEmail1" class="col-sm-6 col-form-label">Tanggal Permohonan</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="tglmohon" name="tglmohon" value="<?php echo $data->tgl_permohonan;  ?>" readonly>
                            </div>  
                        </div>
                        <div class="form-group row">
                            <label for="exampleInputEmail1" class="col-sm-6 col-form-label">Tanggal Pembuatan</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="tglbuat" name="tglbuat" value="<?php echo $data->tgl_pembuatan;  ?>" readonly>
                            </div>  
                        </div>
                        <div class="form-group clearfix">
                            <label for="inputEmail3" class="col-sm-2 control-label" align="right"></label>
                                <div class="col-sm-1 icheck-success d-inline">
                                    <input type="radio" name="aktivasi" checked id="aktivasi1" value="Terbitkan">
                                    <label for="aktivasi1">Terbitkan
                                    </label>
                                </div>
                                <div class="col-sm-1 icheck-success d-inline">
                                    <input type="radio" name="aktivasi" id="aktivasi2" value="Batalkan">
                                    <label for="aktivasi2">
                                    Batalkan
                                    </label>
                                </div>	
                        </div>
                        <div class="form-group row">
                            <label for="exampleInputEmail1" class="col-sm-6 col-form-label">Nomor Surat</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="nosurat" name="nosurat" value="" >
                            </div>  
                        </div>
                        
                
                        <!-- /.card-body -->

                        <div class="card-footer">
                        <button type="submit" class="btn btn-primary">PROSES</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                        
                    
                    </div>
                        <!-- /.card-body -->
                    
                    
                    </div>
                    <!-- /.card -->
                </div>
                </div>

        <?php
        }
    }

    public function skdatacari(Request $request)
    {
       
        $depan = $request->nama_dpn;
        $usaha = $request->nama_usaha;
        $nik = $request->nik;
        $permohonan = $request->tgl_permohonan;
        $pembuatan = $request->tgl_pembuatan;
        
        $result = DB::table('db_pribadi')
        ->join('db_perusahaan', 'db_pribadi.id_anggota','=','db_perusahaan.id_anggota')
        ->join('db_kecamatan', 'db_pribadi.id_kecamatan','=','db_kecamatan.id_kecamatan')
        ->where([['db_pribadi.nama_depan','=',$depan],
                ['db_perusahaan.nama_usaha','=',$usaha],
                ['db_pribadi.nik','=',$nik]
                ])
        ->get();
        $cekskumkm = DB::table('db_sk_umkm')->where('db_sk_umkm.nik','=', $nik)->get();
    //    dump($cekskumkm);

        if(count($result) > 0 ){
            if(count($cekskumkm) > 0){
                ?> <span style='color:red;'>MAAF SURAT KETERANGAN SUDAH TERBIT</span>
            <?php
           
            
        }
        else {
            // 
            foreach($result as $data){
                $id = $data->id_anggota;
                $nama = $data->nama_depan;
                $namausaha = $data->nama_usaha;
                $jenisusaha = $data->jenis_usaha;
                $produk = $data->produk_1;
                ?>
                    <div class="row">
                    <div class="col-md-12">
                    <div class="card card-default">
                    
                    <!-- /.card-header -->
                    <div class="card-body">
                    <form method="POST" action="/createsk">
                    

                        <div class="card-body">
                        <div class="form-group row">
                            <label for="exampleInputEmail1" class="col-sm-6 col-form-label">NIK</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="nik" name="nik" value="<?php echo $data->nik;  ?>" readonly>
                                <input type="hidden" class="form-control" id="id_anggota" name="id_anggota" value="<?php echo $data->id_anggota;  ?>" readonly>
                            </div>  
                        </div>
                        <div class="form-group row">
                            <label for="exampleInputEmail1" class="col-sm-6 col-form-label">Nama Pemilik</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="nama_pemilik" name="nama_pemilik" value="<?php echo $data->nama_depan;  ?>" readonly>
                            </div>  
                        </div>
                        <div class="form-group row">
                            <label for="exampleInputEmail1" class="col-sm-6 col-form-label">Nama Usaha</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="nama_usaha" name="nama_usaha" value="<?php echo $namausaha;  ?>" readonly>
                            </div>  
                        </div>
                        <div class="form-group row">
                            <label for="exampleInputEmail1" class="col-sm-6 col-form-label">Jenis Usaha</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="jenis_usaha" name="jenis_usaha" value="<?php echo $jenisusaha;  ?>" readonly>
                            </div>  
                        </div>
                        <div class="form-group row">
                            <label for="exampleInputEmail1" class="col-sm-6 col-form-label">Tanggal Permohonan</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="tglmohon" name="tglmohon" value="<?php echo $permohonan;  ?>" readonly>
                            </div>  
                        </div>
                        <div class="form-group row">
                            <label for="exampleInputEmail1" class="col-sm-6 col-form-label">Tanggal Pembuatan</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="tglbuat" name="tglbuat" value="<?php echo $pembuatan;  ?>" readonly>
                            </div>  
                        </div>
                        
                
                        <!-- /.card-body -->

                        <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                        
                    
                    </div>
                        <!-- /.card-body -->
                    
                    
                    </div>
                    <!-- /.card -->
                </div>
                </div>
        <?php   
            }
            
          }
            }
        
          else {
            ?> <span style='color:red;'>MAAF DATA TIDAK DITEMUKAN</span>
            <?php
          }
        
        
       
    }
}
