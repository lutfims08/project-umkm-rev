<?php

namespace App\Http\Controllers;

use App\Bansos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\DataTables\BansosDataTable;


class BansosContoller extends Controller
{

    public function __construct()
    {
        $this->middleware('verified');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $totaldaftar    = DB::table('db_bansos')
        ->select(DB::raw('count(db_bansos.nik) as jumlahnik '))     
        ->get();
        $totalbandung    = DB::table('db_bansos')
        ->select(DB::raw('count(db_bansos.nik) as jumlahnik ')) 
        ->where('db_bansos.VERIFIKASI','=','YA')    
        ->get();
        $totalluar    = DB::table('db_bansos')
        ->select(DB::raw('count(db_bansos.nik) as jumlahnik ')) 
        ->where('db_bansos.status','=','Luar Bandung')    
        ->get();
        $totalver    = DB::table('db_bansos')
        ->select(DB::raw('count(db_bansos.nik) as jumlahnik ')) 
        ->where('db_bansos.VERIFIKASI','=','NON')    
        ->get();
        $total    = DB::table('db_bansos') 
        ->select(DB::raw('count(db_bansos.nik) as jumlahnik '),
                 DB::raw('count(db_bansos.NAMA_LENGKAP) as jumlahnama '),
                 DB::raw('count(db_bansos.ALAMAT_LENGKAP) as jumlahalamat '),
                 DB::raw('count(db_bansos.JENIS_USAHA) as jumlahjenis '),
                 DB::raw('count(db_bansos.NO_TELP) as jumlahhp ')
        )
        ->where('db_bansos.VERIFIKASI','=','NON')    
        ->get();
        $totalya    = DB::table('db_bansos') 
        ->select(DB::raw('count(db_bansos.nik) as jumlahnik '),
                 DB::raw('count(db_bansos.NAMA_LENGKAP) as jumlahnama '),
                 DB::raw('count(db_bansos.ALAMAT_LENGKAP) as jumlahalamat '),
                 DB::raw('count(db_bansos.JENIS_USAHA) as jumlahjenis '),
                 DB::raw('count(db_bansos.NO_TELP) as jumlahhp ')
        )
        ->where('db_bansos.VERIFIKASI','=','YA')    
        ->get();
        $totalnik    = DB::table('db_bansos') 
        ->select('db_bansos.NIK')   
        ->where([['db_bansos.NIK','=',NULL],['db_bansos.VERIFIKASI','=','NON']])    
        ->get();
        $niknull =count($totalnik);
        $totalnama    = DB::table('db_bansos') 
        ->select('db_bansos.NAMA_LENGKAP')   
        ->where([['db_bansos.NAMA_LENGKAP','=',NULL],['db_bansos.VERIFIKASI','=','NON']])    
        ->get();
        $namanull =count($totalnama);
        $totalalamat    = DB::table('db_bansos') 
        ->select('db_bansos.ALAMAT_LENGKAP')   
        ->where([['db_bansos.ALAMAT_LENGKAP','=',NULL],['db_bansos.VERIFIKASI','=','NON']])    
        ->get();
        $alamatnull =count($totalalamat);
        $totalhp    = DB::table('db_bansos') 
        ->select('db_bansos.NO_TELP')   
        ->where([['db_bansos.NO_TELP','=',NULL],['db_bansos.VERIFIKASI','=','NON']])    
        ->get();
        $hpnull =count($totalhp);
        $totaljenis    = DB::table('db_bansos') 
        ->select('db_bansos.JENIS_USAHA')   
        ->where([['db_bansos.JENIS_USAHA','=',NULL],['db_bansos.VERIFIKASI','=','NON']])    
        ->get();
        $jenisnull =count($totaljenis);

        $totalnik1    = DB::table('db_bansos') 
        ->select('db_bansos.NIK')   
        ->where([['db_bansos.NIK','=',NULL],['db_bansos.VERIFIKASI','=','NON']])    
        ->get();
        $niknull1 =count($totalnik1);
        $totalnama1    = DB::table('db_bansos') 
        ->select('db_bansos.NAMA_LENGKAP')   
        ->where([['db_bansos.NAMA_LENGKAP','=',NULL],['db_bansos.VERIFIKASI','=','YA']])    
        ->get();
        $namanull1 =count($totalnama1);
        $totalalamat1    = DB::table('db_bansos') 
        ->select('db_bansos.ALAMAT_LENGKAP')   
        ->where([['db_bansos.ALAMAT_LENGKAP','=',NULL],['db_bansos.VERIFIKASI','=','YA']])    
        ->get();
        $alamatnull1 =count($totalalamat1);
        $totalhp1    = DB::table('db_bansos') 
        ->select('db_bansos.NO_TELP')   
        ->where([['db_bansos.NO_TELP','=',NULL],['db_bansos.VERIFIKASI','=','YA']])    
        ->get();
        
        $hpnull1 =count($totalhp1);
        $totaljenis1    = DB::table('db_bansos') 
        ->select('db_bansos.JENIS_USAHA')   
        ->where([['db_bansos.JENIS_USAHA','=',NULL],['db_bansos.VERIFIKASI','=','YA']])    
        ->get();
        $jenisnull1 =count($totaljenis1);
        
        
        return view('bansos.index',compact('totaldaftar','totalbandung','totalluar','totalver','total','niknull','namanull','alamatnull','hpnull','jenisnull','totalya','niknull1','namanull1','alamatnull1','hpnull1','jenisnull1'));
    }

    public function laporankelurahan($id)
    {
        
        
        $totaldaftar    = DB::table('db_bansos')
        ->where([['db_bansos.KELURAHAN','=',$id],['db_bansos.status','=',"Bandung"]])   
        ->get();
        // dd($totaldaftar);
        return view('bansos.showindex',compact('totaldaftar'));
    }

    public function laporankelurahantahap2($id)
    {
        
        
        $totaldaftar    = DB::table('bpums')
        ->where('bpums.kelurahan','=',$id)   
        ->get();
        // dd($totaldaftar);
        return view('bansos.showindextahap2',compact('totaldaftar'));
    }

    public function cari($id)
    {
        $kecamatan = DB::table('db_kelurahan_1')->where('db_kelurahan_1.Kelurahan','=',$id)->get();
        foreach ($kecamatan as $key ) {
           $kec= $key->id_kecamatan;
        }
       
        $kelurahan = DB::table('db_kecamatan_1')->where('db_kecamatan_1.id_kecamatan','=', $kec)->get();
        foreach($kelurahan AS $item){
            $data =[
                'KECAMATAN'=>$item->kecamatan,
            ];
            return($data);
           
       }
    }

    public function carinik($id)
    {
       
        $result = DB::table('db_bansos')->where('db_bansos.NIK','=', $id)->get();
         
        // dd($result);
        if(count($result) > 0){
            
                echo "<span style='color:red;'>MAAF NIK SUDAH TERSEDIA, SIlAHKAN UPDATE DATA
                
                </span>";
           
            }
          else{
            echo "<span style='color:green;'>NIK TERSEDIA</span>";
          }
    }
    public function carinik12($id)
    {
        
        $result = DB::table('db_bansos')->where('db_bansos.NIK','=', $id)->get();
        foreach ($result as $request) {
            $data=[
                'NIK'=>$request->NIK,
                'NAMA_LENGKAP'=>$request->NAMA_LENGKAP,
                'ALAMAT_LENGKAP'=>$request->ALAMAT_LENGKAP,
                'KELURAHAN'=>$request->KELURAHAN,
                'KECAMATAN'=>$request->KECAMATAN,
                'JENIS_USAHA'=>$request->JENIS_USAHA,
                
                'NO_TELP'=>$request->NO_TELP,
                ];
                 return($data);
        }
        
        
        
        }
    public function ceknik(Request $request)
    {
        $request->validate([
            // 'nik1'=>'required|unique:db_bansos|size:16',
            'nik1'=>'required|numeric|size:16',
            
            // 'bahan_baku'=>'required',
        ]);
        $result = DB::table('db_bansos')->where('db_bansos.NIK','=', $request->nik)->get();
         
        return view('bansos.create',compact('result'));
       
           
    }

    public function inputdata()
    {
        $kec = DB::table('db_kecamatan_1')
        ->orderBy('kecamatan')
        ->get();
        
        // dd($kec);
        $kel = DB::table('db_kelurahan_1')->get();
        // dd($totaldaftar);
        return view('bansos.create',compact('kec','kel'));
    }

    public function inputdata12()
    {
        $kec = DB::table('db_kecamatan_1')
        ->orderBy('kecamatan')
        ->get();
        
        // dd($kec);
        $kel = DB::table('db_kelurahan_1')->get();
        // dd($totaldaftar);
        return view('bansos.create12',compact('kec','kel'));
    }
    public function inputdatafix()
    {
        $kec = DB::table('db_kecamatan_1')
        ->orderBy('kecamatan')
        ->get();
        
        // dd($kec);
        $kel = DB::table('db_kelurahan_1')->get();
        // dd($totaldaftar);
        return view('bansos.createfix',compact('kec','kel'));
    }

    public function showindex(BansosDataTable $dataTable)
    {
        // return $dataTable->render('bansos.showindex');

        $totaldaftar    = DB::table('db_bansos')     
        ->get();
        
        return view('bansos.showindex',compact('totaldaftar'));
    }
    public function showindex1(BansosDataTable $dataTable)
    {
        return $dataTable->render('bansos.showindex1');
    }
    public function getdatauser()
    {
        $model = App\Bpum::query();

        return DataTables::eloquent($model)
                ->addColumn('intro', 'Hi {{$name}}!')
                ->toJson();
    }

    public function indexkecamatan()
    {
        $totaldaftar    = DB::table('db_bansos')
        ->select(DB::raw('count(db_bansos.nik) as jumlahnik '),
        DB::raw('db_bansos.kecamatan as kec '))
        ->where('db_bansos.status','=','BANDUNG')
        ->groupBy('kecamatan')
        ->get();
        $total    = DB::table('db_bansos')
        ->where('db_bansos.status','=','BANDUNG')              
        ->get();
        $grand = Count($total);
        // dd($grand);
        return view('bansos.kecamatan',compact('totaldaftar','grand'));
    }

    public function indexkecamatantahap2()
    {
        $totaldaftar    = DB::table('bpums')
        ->select(DB::raw('count(bpums.nik) as jumlahnik '),
        DB::raw('bpums.kecamatan as kec '))
        
        ->groupBy('kecamatan')
        ->get();
        $total    = DB::table('bpums')
                      
        ->get();
        $grand = Count($total);
        // dd($grand);
        return view('bansos.kecamatantahap2',compact('totaldaftar','grand'));
    }
    public function indexkelurahan($id)
    {
        
        $totaldaftar    = DB::table('db_bansos')
        ->select(DB::raw('count(db_bansos.nik) as jumlahnik '),
        DB::raw('db_bansos.kelurahan as kel '))
        ->where([['db_bansos.kecamatan','=', $id],
        ['db_bansos.status','=','BANDUNG']])
        ->groupBy('kelurahan')
        ->get();
        $total    = DB::table('db_bansos')
        ->where('db_bansos.kecamatan','=', $id)
        ->get();
        $grand = Count($total);
        // dd($totaldaftar);
        return view('bansos.kelurahan',compact('totaldaftar','grand'));
    }

    public function indexkelurahantahap2($id)
    {
        
        $totaldaftar    = DB::table('bpums')
        ->select(DB::raw('count(bpums.nik) as jumlahnik '),
        DB::raw('bpums.kelurahan as kel '))
        ->where('bpums.kecamatan','=', $id)
        ->groupBy('kelurahan')
        ->get();
        $total    = DB::table('bpums')
        ->where('bpums.kecamatan','=', $id)
        ->get();
        $grand = Count($total);
        // dd($totaldaftar);
        return view('bansos.kelurahantahap2',compact('totaldaftar','grand'));
    }

    public function indexcari()
    {
        
        
        return view('bansos.caridata');
    }

    public function indexcaridata(Request $request)
    {
        $nama = $request->nama_dpn;
        $nik = $request->nik;
        
        if ($nama == null ) {
            $total    = DB::table('db_bansos')
            ->where('db_bansos.NIK','=', $nik)
            ->limit(1)
            ->get();
            // dd($total);
            if(count($total)>0){
                foreach($total as $data){
                    ?>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card card-default">
                                    <div class="card-body">
                                        <div class="row">
                                            <dt class="col-sm-4">NIK</dt>
                                            <dd class="col-sm-8">:<?php echo $nik;  ?></dd>
                                            <dt class="col-sm-4">Nama </dt>
                                            <dd class="col-sm-8">:<?php echo $data->NAMA_LENGKAP;  ?></dd>
                                            <dt class="col-sm-4">alamat</dt>
                                            <dd class="col-sm-8">:<?php echo $data->ALAMAT_LENGKAP;  ?></dd>
                                            <dt class="col-sm-4">Kelurahan</dt>
                                            <dd class="col-sm-8">:<?php echo $data->KELURAHAN;  ?></dd>
                                            <dt class="col-sm-4">Kecamatan</dt>
                                            <dd class="col-sm-8">:<?php echo $data->KECAMATAN;  ?></dd>
                                            <dt class="col-sm-4">Nomor Hp</dt>
                                            <dd class="col-sm-8">:<?php echo $data->NO_TELP;  ?></dd>
                                            <dt class="col-sm-4">Nama Usaha</dt>
                                            <dd class="col-sm-8">:<?php echo $data->NAMA_USAHA;  ?></dd>
                                            <dt class="col-sm-4">Jenis Usaha</dt>
                                            <dd class="col-sm-8">:<?php echo $data->JENIS_USAHA;  ?></dd>
                                            <dt class="col-sm-4">Persyaratan</dt>
                                            <dd class="col-sm-8">:<?php echo $data->CEKLIST_PERSYARATAN;  ?></dd>
                                        </div>
                                        <span style='color:green;'> <strong>
                                        Anda telah terdaftar dalam Usulan Penerima Bansos Modal Usaha Produktif bagi usaha mikro dan ultra mikro
                                        </strong> </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php
                }
            }
            else{
                ?> <span style='color:red;'> <strong>MOHON MAAF DATA TIDAK DITEMUKAN</strong> </span>
                <?php
            }
        } 
        elseif($nik == null) {
            $total    = DB::table('db_bansos')
            ->where('db_bansos.NAMA_LENGKAP','=',$nama)
            ->limit(1)
            ->get();
            // dd($total);
            if(count($total)>0){
                foreach($total as $data){
                    ?>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card card-default">
                                    <div class="card-body">
                                        <div class="row">
                                            <dt class="col-sm-4">NIK</dt>
                                            <dd class="col-sm-8">:<?php echo $data->NIK;  ?></dd>
                                            <dt class="col-sm-4">Nama </dt>
                                            <dd class="col-sm-8">:<?php echo $data->NAMA_LENGKAP;  ?></dd>
                                            <dt class="col-sm-4">alamat</dt>
                                            <dd class="col-sm-8">:<?php echo $data->ALAMAT_LENGKAP;  ?></dd>
                                            <dt class="col-sm-4">Kelurahan</dt>
                                            <dd class="col-sm-8">:<?php echo $data->KELURAHAN;  ?></dd>
                                            <dt class="col-sm-4">Kecamatan</dt>
                                            <dd class="col-sm-8">:<?php echo $data->KECAMATAN;  ?></dd>
                                            <dt class="col-sm-4">Nomor Hp</dt>
                                            <dd class="col-sm-8">:<?php echo $data->NO_TELP;  ?></dd>
                                            <dt class="col-sm-4">Nama Usaha</dt>
                                            <dd class="col-sm-8">:<?php echo $data->NAMA_USAHA;  ?></dd>
                                            <dt class="col-sm-4">Jenis Usaha</dt>
                                            <dd class="col-sm-8">:<?php echo $data->JENIS_USAHA;  ?></dd>
                                            <dt class="col-sm-4">Persyaratan</dt>
                                            <dd class="col-sm-8">:<?php echo $data->CEKLIST_PERSYARATAN;  ?></dd>
                                        </div>
                                        <span style='color:green;'> <strong>
                                        Anda telah terdaftar dalam Usulan Penerima Bansos Modal Usaha Produktif bagi usaha mikro dan ultra mikro
                                        </strong> </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php
                }
            }
            else{
                ?> <span style='color:red;'> <strong>MOHON MAAF DATA TIDAK DITEMUKAN</strong> </span>
                <?php
            }
        }
        
        
        
        

        // return view('bansos.caridata');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            // 'nik1'=>'required|unique:db_bansos|size:16',
            'nik'=>'required|size:16',
            'nama_dpn'=>'required',
            'alamat'=>'required',
            'kecamatan'=>'required',
            'kelurahan'=>'required',
            'hp'=>'required',
            
            
            // 'pendidikan'=>'required',
            // 'bahan_baku'=>'required',
        ]);
        // $bahan = implode(',', $request->bahan_baku);
        $result = DB::table('db_bansos')->where('db_bansos.NIK','=', $request->nik)->get();
        if(count($result) >0){
            Bansos::where('NIK',$request->nik)
            ->update([
                'NAMA_LENGKAP'=>$request->nama_dpn,
                'ALAMAT_LENGKAP'=>$request->alamat,
                'KELURAHAN'=>$request->kelurahan,
                'KECAMATAN'=>$request->kecamatan,
                'NO_TELP'=>$request->hp,
                'VERIFIKASI'=>"resend_data",
                // 'JENIS_USAHA'=>$request->pendidikan
                
    
            ]);
            return redirect('/inputbansos')->with('statustambah', 'Data Berhasil ditambah!!!');
        }
        else{
            
            Bansos::create([
                // $data=[
                'NIK'=>$request->nik,
                'NAMA_LENGKAP'=>$request->nama_dpn,
                'ALAMAT_LENGKAP'=>$request->alamat,
                'KELURAHAN'=>$request->kelurahan,
                'KECAMATAN'=>$request->kecamatan,
                'status'=>"BANDUNG",
                'NO_TELP'=>$request->hp,
                'VERIFIKASI'=>"YA",
                'JENIS_USAHA'=>$request->pendidikan,
                'CEKLIST_PERSYARATAN'=>"lengkap"
                
                // ];
                // dd($data);
            ]);
           
                return redirect('/inputbansos')->with('statustambah', 'Data Berhasil ditambah!!!');
        }

        

        

      
        
        

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bansos  $bansos
     * @return \Illuminate\Http\Response
     */
    public function show(Bansos $bansos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Bansos  $bansos
     * @return \Illuminate\Http\Response
     */
    public function edit(Bansos $bansos)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bansos  $bansos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        // dd($request->hp);
        Bansos::where('NIK',$request->NIK1)
                    ->update([
                        'NO_TELP'=>$request->hp,
                        'VERIFIKASI'=>"YA",
                        'JENIS_USAHA'=>$request->pendidikan
                        

                    ]);
                    
                        return redirect('/inputbansos')->with('statustambah', 'Data Berhasil ditambah!!!');
                    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bansos  $bansos
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bansos $bansos)
    {
        //
    }
}
