<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class cekdt2Controller extends Controller
{
    public function show(request $request)
    {
        $result = DB::table('bpums')->where('bpums.nik','=', $request->nik)->get();
        // dd($result);
        return view('bpum.tandadt2',compact('result'));
    }
    public function print($id)
    {
        $result = DB::table('bpums')->where('bpums.nik','=', $id)->get();
        // dd($result);
        return view('bpum.tandadt2',compact('result'));
    }
}
