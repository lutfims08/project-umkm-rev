<?php

namespace App\Http\Controllers;

use App\Rekon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RekonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rekon = DB::table('db_bpum_all')->groupBy('KELURAHAN')->get();

        return view ('rekon.index', compact('rekon'));
    }
    public function indexkelurahan($id)
    {
        $rekon = DB::table('db_bpum_all')
        ->where('db_bpum_all.KELURAHAN','=',$id)
        ->get();

        return view ('rekon.showindex', compact('rekon'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Rekon  $rekon
     * @return \Illuminate\Http\Response
     */
    public function show(Rekon $rekon)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Rekon  $rekon
     * @return \Illuminate\Http\Response
     */
    public function edit(Rekon $rekon)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rekon  $rekon
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Rekon $rekon)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Rekon  $rekon
     * @return \Illuminate\Http\Response
     */
    public function destroy(Rekon $rekon)
    {
        //
    }
}
