<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\monevlainnya;
use App\monevperusahaan;
use App\monevpribadi;
use App\Pribadi;
use App\Gambar;
use App\getgeo;

class MonitoringController extends Controller
{
    public function __construct()
    {
        $this->middleware('verified');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kec = DB::table('db_kecamatan')->get();
        if((auth()->user()->role == 'superadmin') or (auth()->user()->role == 'Bid_UMKM')){
            // return view('dataukm.view',compact('member'));
            return view('monitoring.index', compact('kec'));
           }
           else{
            return view('dataukm.roleinfo');
           }
    }

    public function indexshow()
    {
        $member = DB::table('monev_pribadi')
        ->join('monev_perusahaan', 'monev_pribadi.id_anggota','=','monev_perusahaan.id_anggota')
        ->join('db_kecamatan', 'monev_pribadi.id_kecamatan','=','db_kecamatan.id_kecamatan')
        ->get();
        
        if((auth()->user()->role == 'superadmin') or (auth()->user()->role == 'Bid_UMKM')){
            // return view('dataukm.view',compact('member'));
            return view('monitoring.view',compact('member'));
           }
           else{
            return view('dataukm.roleinfo');
           }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $gambar = DB::table('monev_upload')->get();
        return view('monitoring.upload',compact('id','gambar'));
    }

    public function proses_upload(Request $request){
		$this->validate($request, [
			'file' => 'required|file|image|mimes:jpeg,png,jpg|max:10048',
			'jenisdokumen' => 'required',
		]);
 
		// menyimpan data file yang diupload ke variabel $file
		$file = $request->file('file');
 
		$nama_file = time()."_".$file->getClientOriginalName();
 
      	        // isi dengan nama folder tempat kemana file diupload
		$tujuan_upload = 'data_file';
		$file->move($tujuan_upload,$nama_file);
        $ket = $request->id_anggota."_".$request->jenisdokumen;
		Gambar::create([
            'id_anggota' =>$request->id_anggota,
			'file' => $nama_file,
			'keterangan' => $ket,
		]);
 
		return redirect()->back();
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function tampildatacari(Request $request)
    {
        $depan = $request->nama_dpn;
        $usaha = $request->nama_usaha;
        $kec = $request->kecamatan;
        
        $result = DB::table('db_pribadi')
        ->join('db_perusahaan', 'db_pribadi.id_anggota','=','db_perusahaan.id_anggota')
        ->join('db_kecamatan', 'db_pribadi.id_kecamatan','=','db_kecamatan.id_kecamatan')
        ->where([['db_pribadi.nama_depan','=',$depan],
                ['db_perusahaan.nama_usaha','=',$usaha],
                ['db_pribadi.id_kecamatan','=',$kec]
                ])
        ->get();
        $resultmonev = DB::table('monev_pribadi')
        ->join('monev_perusahaan', 'monev_pribadi.id_anggota','=','monev_perusahaan.id_anggota')
        ->join('db_kecamatan', 'monev_pribadi.id_kecamatan','=','db_kecamatan.id_kecamatan')
        ->where([['monev_pribadi.nama_depan','=',$depan],
                ['monev_perusahaan.nama_usaha','=',$usaha],
                ['monev_pribadi.id_kecamatan','=',$kec]
                ])
        ->get();
       
       
        if(count($result) > 0 ){
            if(count($resultmonev) > 0 ){
                
            ?> <span style='color:green;'>SUDAH DILAKUKAN MONITORING</span>
            <?php
            }
            else {
                # code...
            
            foreach($result as $data){
                $id = $data->id_anggota;
                $nama = $data->nama_depan;
                $namausaha = $data->nama_usaha;
                $jenisusaha = $data->jenis_usaha;
                $produk = $data->produk_1;
                ?>
            <div class="row">
        <div class="col-md-12">
            <div class="card card-default">
             
              <!-- /.card-header -->
              <div class="card-body">
              
                    <div class="row">
                        <dt class="col-sm-4">Nama Depan</dt>
                        <dd class="col-sm-8">:<?php echo $nama;  ?></dd>
                        <dt class="col-sm-4">Nama Usaha</dt>
                        <dd class="col-sm-8">:<?php echo $namausaha;  ?></dd>
                        <dt class="col-sm-4">Jenis Usaha</dt>
                        <dd class="col-sm-8">:<?php echo $jenisusaha;  ?></dd>
                        <dt class="col-sm-4">Jenis Produk</dt>
                        <dd class="col-sm-8">:<?php echo $produk;  ?></dd>
                    </div>
              
              </div>
                <!-- /.card-body -->
            <div class="card-footer text-muted">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <a href="monitoring/<?php echo $id;  ?>/monev" class="btn btn-block btn-primary col-sm-4 d-inline">Monev Data</a>
                <a href="monitoring/<?php echo $id;  ?>/nonaktif" class="btn btn-danger float-right col-sm-3 d-inline">Non Aktif</a>
                
            </div>
             
            </div>
            <!-- /.card -->
          </div>
        </div>
        <?php   
        }
        }
            }
         
          else {
            ?> <span style='color:red;'>MAAF DATA TIDAK DITEMUKAN</span>
            <?php
          }
        
        
       
    }
    
    public function store(Request $request)
    {
        $idanggota = $request->id_anggota;
        // dd($idanggota);
        

        $request->validate([
            'nik'=>'required|size:16',
            'nama_dpn'=>'required',
            'alamat'=>'required',
            'hp'=>'required',
            'rt'=>'required',
            'rw'=>'required',
            'telp'=>'required',
            'email'=>'required',
            'jeniskelamin'=>'required',
            'kecamatan'=>'required',
            'kelurahan'=>'required',
            'nama_usaha'=>'required',
            'alamat_usaha'=>'required',
            'thn_pendaftaran'=>'required',
            'thn_berdiri'=>'required',
            'bentuk_usaha'=>'required',
            'jenis_usaha'=>'required',
            'spesifikasi_produk1'=>'required',
            'omset'=>'required',
            'aset'=>'required',
            'nib'=>'required',
            'npwp'=>'required',    
        ]);

        $nominal =preg_replace('/\D/', '', $request->nominalpinjaman);
        $sendiri =preg_replace('/\D/', '', $request->mdl_sendiri);
        $luar =preg_replace('/\D/', '', $request->mdl_luar);
        $omset = preg_replace('/\D/', '', $request->omset);
        $aset = preg_replace('/\D/', '', $request->aset);

        $bahan = implode(',', $request->bahan_baku);
        $penjamin1 = implode(',', $request->penjamin);
        $media = implode(',', $request->mediapenjualan);
        $pasar = implode(',', $request->pasar);
        $kerjasama1 = implode(',', $request->kerjasama);
        $ijin1 = implode(',', $request->ijin);
        
        if ($omset <= 300000000) {
            $jenis_omset = "Mikro";
        } 
        else if (($omset >= 300000000) && ($omset <= 2500000000)){
            $jenis_omset = "Kecil";
        } 
        
        else if (($omset >= 2500000000) && ($omset <= 50000000000 )) {
            $jenis_omset = "Menengah";
        }

        if ($aset <= 50000000) {
            $jenis_aset = "Mikro";
        } 
        else if (($aset >= 50000000) && ($aset <= 500000000)){
            $jenis_aset = "Kecil";
        } 
        
        else if (($aset >= 500000000) && ($aset <= 10000000000 )) {
            $jenis_aset = "Menengah";
        }
        
        // data pribadi

        monevpribadi::create([
            // $data=[
            'id_anggota'=>$request->id_anggota,
            'nik'=>$request->nik,
            'nama_depan'=>$request->nama_dpn,
            'jenis_kelamin'=>$request->jeniskelamin,
            'alamat'=>$request->alamat,
            'rt'=>$request->rt,
            'rw'=>$request->rw,
            'no_telepon'=>$request->telp,
            'no_handphone'=>$request->hp,
            'email'=>$request->email,
            'id_kelurahan'=>$request->kelurahan,
            'id_kecamatan'=>$request->kecamatan,
            'kota'=>$request->kota,
            'kode_pos'=>$request->kode_pos,
            'pendidikan'=>$request->pendidikan,
            'aktifasi'=>$request->aktivasi,
            'created_at'=>now()
            // ];
        ]);

        // end data pribadi

        // data perusahaan
        monevperusahaan::create([
                'id_anggota'=>$request->id_anggota,
                'nama_usaha'=>$request->nama_usaha,
                'alamat_usaha'=>$request->alamat_usaha,
                'rt_usaha'=>$request->rt_usaha,
                'rw_usaha'=>$request->rw_usaha,
                'no_telepon_usaha'=>$request->telp_usaha,
                'thn_daftar'=>$request->thn_pendaftaran,
                'thn_berdiri'=>$request->thn_berdiri,
                'bentuk_usaha'=>$request->bentuk_usaha,
                'jenis_usaha'=>$request->jenis_usaha,
                'produk_1'=>$request->spesifikasi_produk1,
                'produk_2'=>$request->spesifikasi_produk2,
                'produk_3'=>$request->spesifikasi_produk3,
                'bahan_utama'=>$request->bahanbakuutama,
                'perolehan'=>$bahan,
                'detail_perolehan'=>$request->perolehan,
                'jumlah_karyawan'=>$request->jumlah_karyawan,
                'manajemen_keuangan'=>$request->keuangan,
                'detail_manajemen'=>$request->keuangandetail,
                'modal_sendiri'=>$sendiri,
                'modal_luar'=>$luar,
                'penjamin'=>$penjamin1,
                'nominal'=>$nominal,
                'detail_penjamin'=>$request->penjamindetail,
                'jenis_omset'=>$jenis_omset,
                'jumlah_omset'=>$omset,
                'jumlah_aset'=>$aset,
                'jenis_aset'=>$jenis_aset,
                'media_penjualan'=>$media,
                'detail_media'=>$request->detailmediapenjualan,
                'kerjasama'=>$kerjasama1,
                'detailkerjasama'=>$request->detailkerjasama,
                'created_at'=>now()
            ]);
        // end data peprusahaan
        
        //  data lain

        monevlainnya::create([
            'id_anggota'=>$request->id_anggota,
            'bahan_baku'=>$bahan,
            'pemasaran'=>$pasar,
            'detailpemasaran'=>$request->detailpasar,
            'pelatihan'=>$request->keterangan,
            'perijinan'=>$ijin1,
            'nib'=>$request->nib,
            'npwp'=>$request->npwp,
            'haki'=>$request->haki,
            'pirt'=>$request->pirt,
            'halal'=>$request->halal,
            'bpom'=>$request->bpom,
            'created_at'=>now()
        ]);

        getgeo::create([
            'id_anggota'=>$request->id_anggota,
            'Latitude'=>$request->luti1,
            'Longitude'=>$request->long1,
            'created_at'=>now(),
        ]);

        //  end data lain
    
        return redirect('/monitoring/upload/'.$idanggota);
        

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
               
      
      $v1 = DB::table('db_pribadi')
      ->join('db_perusahaan', 'db_pribadi.id_anggota','=','db_perusahaan.id_anggota')
      ->join('db_lain', 'db_pribadi.id_anggota','=','db_lain.id_anggota')
      ->join('db_kecamatan', 'db_pribadi.id_kecamatan','=','db_kecamatan.id_kecamatan')
      ->join('db_kelurahan', 'db_pribadi.id_kelurahan','=','db_kelurahan.id_kelurahan')
      ->where('db_pribadi.id_anggota', '=', $id)
        ->get();
        $v2 = DB::table('monev_pribadi')
      ->join('monev_perusahaan', 'monev_pribadi.id_anggota','=','monev_perusahaan.id_anggota')
      ->join('monev_lainnya', 'monev_pribadi.id_anggota','=','monev_lainnya.id_anggota')
      ->join('db_kecamatan', 'monev_pribadi.id_kecamatan','=','db_kecamatan.id_kecamatan')
      ->join('db_kelurahan', 'monev_pribadi.id_kelurahan','=','db_kelurahan.id_kelurahan')
      ->where('monev_pribadi.id_anggota', '=', $id)
        ->get();
    //    dd($v2);
        $gambar = DB::table('monev_upload')
        ->where('monev_upload.id_anggota','=', $id)
        ->get();
        $countgambar = count($gambar);
        return view('monitoring.detail',compact('v1','v2','gambar','countgambar'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {  
      
      $v1 = DB::table('db_pribadi')
      ->join('db_perusahaan', 'db_pribadi.id_anggota','=','db_perusahaan.id_anggota')
      ->join('db_lain', 'db_pribadi.id_anggota','=','db_lain.id_anggota')
      ->join('db_kecamatan', 'db_pribadi.id_kecamatan','=','db_kecamatan.id_kecamatan')
      ->join('db_kelurahan', 'db_pribadi.id_kelurahan','=','db_kelurahan.id_kelurahan')
      ->where('db_pribadi.id_anggota', '=', $id)
        ->get();
        $kec = DB::table('db_kecamatan')->get();
        $kel = DB::table('db_kelurahan')->get();
        return view('monitoring.edit',compact('v1','kec','kel'));
        
    }

    public function nonaktif($id)
    {
        $v1 = DB::table('db_pribadi')
      ->join('db_perusahaan', 'db_pribadi.id_anggota','=','db_perusahaan.id_anggota')
      ->join('db_lain', 'db_pribadi.id_anggota','=','db_lain.id_anggota')
      ->join('db_kecamatan', 'db_pribadi.id_kecamatan','=','db_kecamatan.id_kecamatan')
      ->join('db_kelurahan', 'db_pribadi.id_kelurahan','=','db_kelurahan.id_kelurahan')
      ->where('db_pribadi.id_anggota', '=', $id)
        ->get();

        $cek = DB::table('monev_pribadi')
        ->join('monev_perusahaan', 'monev_pribadi.id_anggota','=','monev_perusahaan.id_anggota')
        ->join('monev_lainnya', 'monev_pribadi.id_anggota','=','monev_lainnya.id_anggota')
        ->join('db_kecamatan', 'monev_pribadi.id_kecamatan','=','db_kecamatan.id_kecamatan')
        ->join('db_kelurahan', 'monev_pribadi.id_kelurahan','=','db_kelurahan.id_kelurahan')
        ->where('monev_pribadi.id_anggota', '=', $id)
          ->get();
        
        // dd($v1);
        
        foreach($v1 as $result ){
        monevpribadi::create([
            // $data[]=[
            'id_anggota'=>$result->id_anggota,
            'nik'=>$result->nik,
            'nama_depan'=>$result->nama_depan,
            'jenis_kelamin'=>$result->jenis_kelamin,
            'alamat'=>$result->alamat,
            'rt'=>$result->rt,
            'rw'=>$result->rw,
            'no_telepon'=>$result->no_telepon,
            'no_handphone'=>$result->no_handphone,
            'email'=>$result->email,
            'id_kelurahan'=>$result->id_kelurahan,
            'id_kecamatan'=>$result->id_kecamatan,
            'kota'=>$result->kota,
            'kode_pos'=>$result->kode_pos,
            'pendidikan'=>$result->pendidikan,
            'aktifasi'=>'Non Aktif',
            'created_at'=>now()
            // ];
        ]);
        Pribadi::where('id_anggota',$result->id_anggota)
                    ->update([
                        // $data=[
                        'aktifasi'=>'Non Aktif',
                        'tanggal_rubah'=>now()
                        // ];
                    ]);

        monevperusahaan::create([
            'id_anggota'=>$result->id_anggota,
            'nama_usaha'=>$result->nama_usaha,
            'alamat_usaha'=>$result->alamat_usaha,
            'rt_usaha'=>$result->rt_usaha,
            'rw_usaha'=>$result->rw_usaha,
            'no_telepon_usaha'=>$result->no_telepon_usaha,
            'thn_daftar'=>$result->thn_daftar,
            'thn_berdiri'=>$result->thn_berdiri,
            'bentuk_usaha'=>$result->bentuk_usaha,
            'jenis_usaha'=>$result->jenis_usaha,
            'produk_1'=>$result->produk_1,
            'produk_2'=>$result->produk_2,
            'produk_3'=>$result->produk_3,
            'bahan_utama'=>$result->bahan_utama,
            'perolehan'=>$result->perolehan,
            'detail_perolehan'=>$result->detail_perolehan,
            'jumlah_karyawan'=>$result->jumlah_karyawan,
            'manajemen_keuangan'=>$result->manajemen_keuangan,
            'detail_manajemen'=>$result->detail_manajemen,
            'modal_sendiri'=>$result->modal_sendiri,
            'modal_luar'=>$result->modal_luar,
            'penjamin'=>$result->penjamin,
            'nominal'=>$result->nominal,
            'detail_penjamin'=>$result->detail_penjamin,
            'jenis_omset'=>$result->jenis_omset,
            'jumlah_omset'=>$result->jumlah_omset,
            'jumlah_aset'=>$result->jumlah_aset,
            'jenis_aset'=>$result->jenis_aset,
            'media_penjualan'=>$result->media_penjualan,
            'detail_media'=>$result->detail_media,
            'kerjasama'=>$result->kerjasama,
            'detailkerjasama'=>$result->detailkerjasama,
            'created_at'=>now()
        ]);
        // dd($data);
        monevlainnya::create([
            'id_anggota'=>$result->id_anggota,
            'bahan_baku'=>$result->bahan_baku,
            'pemasaran'=>$result->pemasaran,
            'detailpemasaran'=>$result->detailpemasaran,
            'pelatihan'=>$result->pelatihan,
            'perijinan'=>$result->perijinan,
            'nib'=>$result->nib,
            'npwp'=>$result->npwp,
            'haki'=>$result->haki,
            'pirt'=>$result->pirt,
            'halal'=>$result->halal,
            'bpom'=>$result->bpom,
            'created_at'=>now()
        ]);
        return redirect('/monitoring')->with('statustambah', 'Movev Telah dilakukan!!!');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($pribadi)
    {
        // dd($pribadi);
        monevpribadi::destroy($pribadi);
        monevperusahaan::destroy($pribadi);
        monevlainnya::destroy($pribadi);
        getgeo::destroy($pribadi);
    
        return redirect('/monev')->with('statustambah', 'Data Berhasil dihapus!!!');
    }

    public function destroygambar($id)
    {
        // dd($id);
        DB::table('monev_upload')->where('keterangan', '=', $id)->delete();
        return redirect()->back();
        // getgeo::destroy($id->id_anggota);
    
        // return redirect('/binaans')->with('statustambah', 'Data Berhasil dihapus!!!');
    }
}
