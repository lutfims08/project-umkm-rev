<?php

namespace App\Http\Controllers;

use App\Kegiatan;
use App\Dkegiatan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KegiatanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kegiatan = DB::table('db_kegiatan-old')->get();
        // dd($kegiatan);
        return view('kegiatan.index' , compact('kegiatan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('kegiatan.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id=$request->id;
        
        Kegiatan::create([
            'id_anggota'=>$request->anggota,
            'id_kegiatan'=>$request->id,
        ]);
        return redirect('/kegiatan/'.$id)->with('statustambah', 'Peserta berhasil ditambahkan!!!');
    }

    public function storekegiatan(Request $request)
    {
        $lastorderId = DB::table('db_kegiatan-old')->orderBy('id_kegiatan', 'desc')->first()->id_kegiatan;

        // // Get last 3 digits of last order id
        $lastIncreament = substr($lastorderId, -3);

        // // Make a new order id with appending last increment + 1
        $newOrderId = 'K' . date('Ymds') . str_pad($lastIncreament + 1, 3, 0, STR_PAD_LEFT);
        
        Dkegiatan::create([
            'id_kegiatan'=>$newOrderId,
            'nama_kegiatan'=>$request->nama_kegiatan,
            'deskripsi'=>$request->deskripsi,
            'jumlah_peserta'=>$request->jumlah_peserta,
            'tempat_kegiatan'=>$request->tempat_kegiatan,
            'tangal_kegiatan'=>$request->tgl_kegiatan,
        ]);
        return redirect('/kegiatan/'.$newOrderId)->with('statustambah', 'Silahkan Tambahkan Peserta!!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Kegiatan  $kegiatan
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        $idkegiatan = $id;
        $member = DB::table('db_pribadi')
        ->join('db_perusahaan', 'db_pribadi.id_anggota','=','db_perusahaan.id_anggota')
        ->join('db_kecamatan', 'db_pribadi.id_kecamatan','=','db_kecamatan.id_kecamatan')
        ->get();
        $kegiatandata = DB::table('db_kegiatan-old')->where('db_kegiatan-old.id_kegiatan','=',$id)->get();
        $kegiatan = DB::table('db_kegiatan-old')
        ->join('db_d_kegiatan','db_kegiatan-old.id_kegiatan','=','db_d_kegiatan.id_kegiatan')
        ->join('db_pribadi','db_d_kegiatan.id_anggota','=','db_pribadi.id_anggota')
        ->join('db_perusahaan','db_d_kegiatan.id_anggota','=','db_perusahaan.id_anggota')
        ->where('db_kegiatan-old.id_kegiatan','=',$id)
        ->get();
        // dd($kegiatan);
        return view('kegiatan.view',compact('kegiatandata', 'kegiatan','member','idkegiatan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Kegiatan  $kegiatan
     * @return \Illuminate\Http\Response
     */
    public function edit(Kegiatan $kegiatan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Kegiatan  $kegiatan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Kegiatan $kegiatan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Kegiatan  $kegiatan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Kegiatan $kegiatan)
    {
        //
    }
}
