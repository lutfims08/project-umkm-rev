<?php

namespace App\Http\Controllers;

use App\Bpum;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BpumcekController extends Controller
{

    public function __construct()
    {
        $this->middleware('verified');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kec = DB::table('db_kecamatan_1')
        ->orderBy('kecamatan')
        ->get();
        
        // dd($kec);
        $kel = DB::table('db_kelurahan_1')->get();
        // dd($totaldaftar);
        return view('bpum.update2',compact('kec','kel'));
    }

    public function carinik12($id)
    {
        
        $result = DB::table('bpums')->where('bpums.nik','=', $id)->get();
        foreach ($result as $request) {
            $data=[
                'nik'=>$request->nik,
                'nama_lengkap'=>$request->nama_lengkap,
                'alamat'=>$request->alamat,
                'kelurahan'=>$request->kelurahan,
                'kecamatan'=>$request->kecamatan,
                'bidang_usaha'=>$request->bidang_usaha,
                'no_telp'=>$request->no_telp,
                'no_reg'=>$request->no_reg,
                ];
                 return($data);
        }
        
        
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function verifikasi()
    {
        $kec = DB::table('db_kecamatan_1')
        ->orderBy('kecamatan')
        ->get();
        
        // dd($kec);
        $kel = DB::table('db_kelurahan_1')->get();
        // dd($totaldaftar);
        return view('bpum.verifikasi',compact('kec','kel'));
    }

    public function verifikasistore(Request $request)
    {
       if ($request->aktivasi == "invalid" ) {
            Bpum::where('NIK',$request->nik)
            ->update([
                'status'=>$request->aktivasi,
                'keterangan'=>$request->keterangan,
                'no_reg'=>"(".$request->no_reg.")",

        ]);
        return redirect('/validasibpum2')->with('statustambah', 'VALIDASI BERHASIL!!!');
       } else {
            Bpum::where('NIK',$request->nik)
            ->update([
                'status'=>$request->aktivasi,
                'keterangan'=>$request->keterangan,
                'no_reg'=>$request->no_reg,
            ]);
            return redirect('/validasibpum2')->with('statustambah', 'VALIDASI BERHASIL!!!');
       }
       
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $nik=$request->nik;
            $nama=$request->nama_dpn;
            $alamat=$request->alamat;
            $hp=$request->hp;
            $no=$request->no_reg;
        Bpum::where('NIK',$request->nik)
        ->update([
            'nik'=>$request->nik,
                'nama_lengkap'=>$request->nama_dpn,
                'alamat'=>$request->alamat,
                'kelurahan'=>$request->kelurahan,
                'kecamatan'=>$request->kecamatan,
                'bidang_usaha'=>$request->bidang_usaha,
                'no_telp'=>$request->hp,
                'no_reg'=>$request->no_reg,

        ]);
        
        return view('bpum.tanda',compact('nik','nama','alamat','hp','no'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $result = DB::table('bpums')->where('bpums.status','=','resend_data')->get();
        return view('bpum.view',compact('result'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
