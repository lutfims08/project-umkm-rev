<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DatasebaranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $kec_out = DB::table('db_pribadi')
        ->select(DB::raw('db_kecamatan.kecamatan'),DB::raw('count(db_pribadi.id_kecamatan) as TOTAL '))
        ->join('db_perusahaan', 'db_pribadi.id_anggota','=','db_perusahaan.id_anggota')
        ->join('db_kecamatan', 'db_pribadi.id_kecamatan','=','db_kecamatan.id_kecamatan')
        ->groupBy('kecamatan')
        ->get();

        
        $point = DB::table('db_lokasi_update')
        ->join('db_pribadi','db_lokasi_update.id_anggota','=','db_pribadi.id_anggota')
        ->join('db_perusahaan', 'db_lokasi_update.id_anggota','=','db_perusahaan.id_anggota')
        ->join('db_kecamatan', 'db_pribadi.id_kecamatan','=','db_kecamatan.id_kecamatan')
        ->get();
        
       
        
        return view('monitoring.getgeo',compact('kec_out','point'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
