<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GeustController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('geust.index');
    }

    public function cek(Request $request)
    {
        $cek = $request->id_anggota;
        if ($cek == null) {
            return view('geust.inputdata');
        } else {
            return view('geust.validasi');
            
        }
        
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kec = DB::table('db_kecamatan')->get();
        $kel = DB::table('db_kelurahan')->get();
        // Get the last order id
        $lastorderId = DB::table('db_pribadi')->orderBy('id_anggota', 'desc')->first()->id_anggota;

        // Get last 3 digits of last order id
        $lastIncreament = substr($lastorderId, -3);

        // Make a new order id with appending last increment + 1
        $newOrderId = 'UMKM' . date('Ymd') . str_pad($lastIncreament + 1, 3, 0, STR_PAD_LEFT);
       

        return view('dataukm.create',compact('kec','newOrderId', 'kel'));
    }
    public function cari($id)
    {
       
        $kelurahan = DB::table('db_kelurahan')->where('db_kelurahan.id_kecamatan','=', $id)->get();
        foreach($kelurahan AS $data){
            $id         = $data->id_kelurahan;
            $title      = $data->kelurahan;
           ?>
            <option value='<?php echo $id; ?>'><?php echo $title; ?></option>
            <?php
       }
    }
    public function carinik($id)
    {
      
        $result = DB::table('db_pribadi')
        ->join('db_perusahaan','db_pribadi.id_anggota','=','db_perusahaan.id_anggota')
        ->where('db_pribadi.nik','=', $id)->get();
        
        
            # code...
        
            if(count($result) > 0){
                foreach ($result as $key) {
            ?>
            <!-- <span style='color:green;'>ANDA SUDAH TERDAFTAR SEBAGAI UMKM BINAAN </span> -->
                <div class="form-group row ">
                    <label for="inputEmail3" class="col-sm-3 col-form-label" align="right">Nama Pemilik</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control "  id="nama_dpn" name="nama_dpn"  placeholder="Nama Depan" value="<?php echo $key->nama_depan ?>">
                        <input type="hidden" class="form-control "  id="id_anggota" name="id_anggota"  placeholder="Nama Depan" value="<?php echo $key->id_anggota ?>">
                        
                    </div>	
                </div>
                <div class="form-group row ">
                    <label for="inputEmail3" class="col-sm-3 col-form-label" align="right">Nama Usaha</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control "  id="nama_usaha" name="nama_usaha"  placeholder="Nama Depan" value="<?php echo $key->nama_usaha ?>">
                        
                    </div>	
                </div>
                <span style='color:green;'>ANDA SUDAH TERDAFTAR SEBAGAI UMKM BINAAN, Klik lanjut untuk melanjutkan proses  </span>
                <br>
                <br>
                <a href="/geust" class="btn btn-info">Kembali</a>
                <button type="submit" class="btn btn-info"><i class="far fa-hand-point-right ">Lanjut</i></button>
            <?php }
             }

            else{
                
                
                ?>
                <span style='color:brown;'>MAAF BELUM TERDAFTAR SEBAGAI BINAAN KLIK LANJUTKAN UNTUK MENGISI DATA PROFILE</span>
                <br>
                <br>
                <button type="submit" class="btn btn-info"><i class="far fa-hand-point-right ">Lanjut</i></button>
            <?php 
                    
                }
        
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
