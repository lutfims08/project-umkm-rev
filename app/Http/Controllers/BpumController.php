<?php

namespace App\Http\Controllers;

use App\Bpum;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BpumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
         return view('bpum.index');
        //  return redirect('https://abqdev.site/');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function monev()
    {
        $totaldaftar    = DB::table('bpums')
        ->select(DB::raw('count(bpums.nik) as jumlahnik '))     
        ->get();
        $totalvalid    = DB::table('bpums')
        ->select(DB::raw('count(bpums.status) as jumlahstatus '))
        ->where('bpums.status','=','YA')     
        ->get();
        $totalvalid1    = DB::table('bpums')
        ->select(DB::raw('count(bpums.status) as jumlahcekdata '))
        ->where('bpums.status','=','cekdata')     
        ->get();
        $totalvalid12    = DB::table('bpums')
        ->select(DB::raw('count(bpums.status) as jumlahcekdata '))
        ->whereIn('status',['valid','validasi'])
        // ->whereNull('status')     
        ->get();
        $totalvalid123   = DB::table('bpums')
        ->select(DB::raw('count(bpums.status) as jumlahcekdata '))
        ->where('bpums.status','=','invalid')
        // ->whereNull('status')     
        ->get();
        // dd($totalvalid);
        return view('bpum.monitoring',compact('totaldaftar','totalvalid','totalvalid1','totalvalid12','totalvalid123'));
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->nik, $request->nama_dpn, $request->nama_usaha, $request->bidang_usaha, $request->txtEmpPhone, $request->alamat, $request->no_reg);
        $request->validate([
            // 'nik1'=>'required|unique:db_bansos|size:16',
            'nik'=>'required|size:16',
            
            // 'bahan_baku'=>'required',
        ]);
        $result = DB::table('db_bansos')->where('db_bansos.NIK','=', $request->nik)->get();
        if (count($result) > 0) {
            return view('bpum.hasil',compact('result'));
        } else {
            $nik=$request->nik;
            $nama=$request->nama_dpn;
            $alamat=$request->alamat;
            $hp=$request->txtEmpPhone;
            $no=$request->no_reg;
            
            $request->validate([
                // 'nik1'=>'required|unique:db_bansos|size:16',
                'nik'=>'required|unique:bpums|size:16',
                'nama_dpn'=>'required',
                'alamat'=>'required',
                'no_reg'=>'required',
                'txtEmpPhone'=>'required',
                'kecamatan'=>'required',
                
                ]);
                $result1 = DB::table('bpums')->where('bpums.no_reg','=', $request->no_reg)->get();
                if (count($result1) > 0) {
                    return view('bpum.hasil1',compact('result1'));
                } else{
                    Bpum::create([
                        'nik'=>$request->nik,
                        'nama_lengkap'=>$request->nama_dpn,
                        'alamat'=>$request->alamat,
                        'bidang_usaha'=>$request->bidang_usaha,
                        'no_telp'=>$request->txtEmpPhone,
                        'no_reg'=>$request->no_reg,
                        'kecamatan'=>$request->kecamatan,
                        'kelurahan'=>$request->kelurahan,
                        'status'=>"validasi",
                        'keterangan'=>""
    
                    ]);
                    
                    return view('bpum.tanda',compact('nik','nama','alamat','hp','no'));
                }
                
                
        }
        

    }

    public function cari($id)
    {
        $kecamatan = DB::table('db_kelurahan_1')->where('db_kelurahan_1.Kelurahan','=',$id)->get();
        foreach ($kecamatan as $key ) {
           $kec= $key->id_kecamatan;
        }
       
        $kelurahan = DB::table('db_kecamatan_1')->where('db_kecamatan_1.id_kecamatan','=', $kec)->get();
        foreach($kelurahan AS $item){
            $data =[
                'KECAMATAN'=>$item->kecamatan,
            ];
            return($data);
           
       }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bpum  $bpum
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $result = DB::table('bpums')->where('bpums.nik','=', $request->nik)->get();
        return view('bpum.tanda1',compact('result'));
    }
    public function print($id)
    {
        $result = DB::table('bpums')->where('bpums.nik','=', $id)->get();
        return view('bpum.tanda1',compact('result'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Bpum  $bpum
     * @return \Illuminate\Http\Response
     */
    public function edit(Bpum $bpum)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bpum  $bpum
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Bpum $bpum)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bpum  $bpum
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bpum $bpum)
    {
        //
    }
}
