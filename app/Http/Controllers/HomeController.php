<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('verified');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $jumlah = DB::table('db_pribadi')
        ->join('db_perusahaan', 'db_pribadi.id_anggota','=','db_perusahaan.id_anggota')
        ->get();
        $total = count($jumlah);

        $jumlah1 = DB::table('monev_pribadi')
        ->join('monev_perusahaan', 'monev_pribadi.id_anggota','=','monev_perusahaan.id_anggota')
        
        ->where('monev_pribadi.aktifasi','=','Non Aktif')
        ->get();
        $totalnon = count($jumlah1);
   
        $jumlahmikro = DB::table('db_pribadi')
        ->join('db_perusahaan', 'db_pribadi.id_anggota','=','db_perusahaan.id_anggota')
        
        ->where('db_perusahaan.jenis_omset','=','Mikro')
        ->get();
        $mikro = count($jumlahmikro);

        $jumlahmikro1 = DB::table('monev_pribadi')
        ->select('monev_pribadi.*')
        ->join('monev_perusahaan', 'monev_pribadi.id_anggota','=','monev_perusahaan.id_anggota')
        ->where([
            ['monev_perusahaan.jenis_omset','=','Mikro'],
            ['monev_pribadi.aktifasi','=','Non Aktif'],
        ])
        ->get();
        $mikro1 = count($jumlahmikro1);
      
        $jumlahkecil = DB::table('db_pribadi')
        ->join('db_perusahaan', 'db_pribadi.id_anggota','=','db_perusahaan.id_anggota')
        
        ->where('db_perusahaan.jenis_omset','=','Kecil')
        ->get();
        $kecil = count($jumlahkecil);

        $jumlahkecil1 = DB::table('monev_pribadi')
        ->select('monev_pribadi.*')
        ->join('monev_perusahaan', 'monev_pribadi.id_anggota','=','monev_perusahaan.id_anggota')
        ->where([
            ['monev_perusahaan.jenis_omset','=','Kecil'],
            ['monev_pribadi.aktifasi','=','Non Aktif'],
        ])
        ->get();
        $kecil1 = count($jumlahkecil1);

        $jumlahmenengah = DB::table('db_pribadi')
        ->join('db_perusahaan', 'db_pribadi.id_anggota','=','db_perusahaan.id_anggota')
        
        ->where('db_perusahaan.jenis_omset','=','menengah')
        ->get();
        $menengah = count($jumlahmenengah);

        $jumlahmenengah1 = DB::table('monev_pribadi')
        ->select('monev_pribadi.*')
        ->join('monev_perusahaan', 'monev_pribadi.id_anggota','=','monev_perusahaan.id_anggota')
        ->where([
            ['monev_perusahaan.jenis_omset','=','Menengah'],
            ['monev_pribadi.aktifasi','=','Non Aktif'],
        ])
        ->get();
        $menengah1 = count($jumlahmenengah1);

        $thn    = DB::table('db_perusahaan')
        ->select('thn_daftar')
        ->groupBy('thn_daftar')
        ->get();

        
        $aktif ='Aktif';
        $sqlaktif    = DB::table('db_pribadi')
        ->select(DB::raw('db_perusahaan.thn_daftar as thndaftar '),
                 DB::raw('count(db_pribadi.aktifasi) as status '))
        ->when($aktif, function($query, $aktif){
            return $query->where('db_pribadi.aktifasi', $aktif);
        })
        ->join('db_perusahaan', 'db_pribadi.id_anggota','=','db_perusahaan.id_anggota')
        
        ->groupBy('thn_daftar')
       
        ->get();
        $nonaktif ='Aktif';
        $sqlnonaktif    = DB::table('db_pribadi')
        ->select(DB::raw('db_perusahaan.thn_daftar as thndaftar '),
                 DB::raw('count(db_pribadi.aktifasi) as status '))
        ->when($nonaktif, function($query, $nonaktif){
            return $query->where('db_pribadi.aktifasi', $nonaktif);
        })
        ->join('db_perusahaan', 'db_pribadi.id_anggota','=','db_perusahaan.id_anggota')
        
        ->groupBy('thn_daftar')
       
        ->get();
        $sqlthn    = DB::table('db_pribadi')
        ->select(DB::raw('db_perusahaan.thn_daftar as thndaftar '),
                 DB::raw('count(db_perusahaan.thn_daftar) as jumlahthndaftar '))
        ->join('db_perusahaan', 'db_pribadi.id_anggota','=','db_perusahaan.id_anggota')
        ->groupBy('thn_daftar')
       
        ->get();
        $sqlmakanan    = DB::table('db_pribadi')
        ->select(DB::raw('db_perusahaan.thn_daftar as thndaftar '),
                 DB::raw('count(db_perusahaan.jenis_usaha) as jenisUsaha '))      
        ->join('db_perusahaan', 'db_pribadi.id_anggota','=','db_perusahaan.id_anggota')
        
        ->where('db_perusahaan.jenis_usaha','=','Makanan')
        ->groupBy('thn_daftar')
       
        ->get();
        $sqlfashion    = DB::table('db_pribadi')
        ->select(DB::raw('db_perusahaan.thn_daftar as thndaftar '),
                 DB::raw('count(db_perusahaan.jenis_usaha) as jenisUsaha '))      
        ->join('db_perusahaan', 'db_pribadi.id_anggota','=','db_perusahaan.id_anggota')
        
        ->where('db_perusahaan.jenis_usaha','=','Fashion')
        ->groupBy('thn_daftar')
       
        ->get();

        $sqlhandicraft    = DB::table('db_pribadi')
        ->select(DB::raw('db_perusahaan.thn_daftar as thndaftar '),
                 DB::raw('count(db_perusahaan.jenis_usaha) as jenisUsaha '))      
        ->join('db_perusahaan', 'db_pribadi.id_anggota','=','db_perusahaan.id_anggota')
        
        ->where('db_perusahaan.jenis_usaha','=','Handicraft')
        ->groupBy('thn_daftar')
       
        ->get();

        $sqljasa    = DB::table('db_pribadi')
        ->select(DB::raw('db_perusahaan.thn_daftar as thndaftar '),
                 DB::raw('count(db_perusahaan.jenis_usaha) as jenisUsaha '))      
        ->join('db_perusahaan', 'db_pribadi.id_anggota','=','db_perusahaan.id_anggota')
        
        ->where('db_perusahaan.jenis_usaha','=','Jasa')
        ->groupBy('thn_daftar')
       
        ->get();

        $sqldagang    = DB::table('db_pribadi')
        ->select(DB::raw('db_perusahaan.thn_daftar as thndaftar '),
                 DB::raw('count(db_perusahaan.jenis_usaha) as jenisUsaha '))      
        ->join('db_perusahaan', 'db_pribadi.id_anggota','=','db_perusahaan.id_anggota')
        
        ->where('db_perusahaan.jenis_usaha','=','Perdagangan')
        ->groupBy('thn_daftar')
       
        ->get();

        $sqldagang1    = DB::table('db_pribadi')
        ->select(DB::raw('db_perusahaan.thn_daftar as thndaftar '),
                 DB::raw('count(db_perusahaan.jenis_usaha) as jenisUsaha '))      
        ->join('db_perusahaan', 'db_pribadi.id_anggota','=','db_perusahaan.id_anggota')
        
        ->where('db_perusahaan.jenis_usaha','=','Lain-lain')
        ->groupBy('thn_daftar')
       
        ->get();
     
               
        return view('home',compact('total','totalnon','mikro', 'kecil','menengah','mikro1', 'kecil1','menengah1','sqlthn','sqlmakanan','sqlfashion','sqlhandicraft','sqljasa','sqldagang','sqldagang1'));
        
    }

    public function total()
    {
       
        
    }
}
