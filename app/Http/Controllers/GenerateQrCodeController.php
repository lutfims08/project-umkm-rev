<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator,Redirect,Response;

class GenerateQrCodeController extends Controller
{
    public function simpleQrCode() 
    {

     return \QrCode::size(100)->generate('A basic example of QR code!');
       
    }    

    public function colorQrCode() 
    {

     return \QrCode::size(100)
             ->Style("square",0.5)
             ->generate('https://bit.ly/inputUMKMbdg');
       
    }    
    
    public function imageQrCode() 
    {

      $image = \QrCode::format('png')
               ->merge('dist/img/LOGO-DISKOP2.png', 0.5, true)
               ->size(100)->errorCorrection('H')
               ->generate('A simple example of QR code!');
      return response($image)->header('Content-type','image/png');
       
    }
}
