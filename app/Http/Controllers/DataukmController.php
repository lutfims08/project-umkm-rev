<?php

//iyeu contorller na at


namespace App\Http\Controllers;

use App\dataukm;
use App\datausaha;
use Illuminate\Http\Request;
use App\DataTables\DataukmDataTable;

class DataukmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('verified');
    }
    
    public function index(DataukmDataTable $dataTable)
    {
        // dump($dataTable); 
        // $user= datausaha::join('db_pribadi','id_anggota')->get();
        return $dataTable->render('dataukm.index');

        
   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\dataukm  $dataukm
     * @return \Illuminate\Http\Response
     */
    public function show(dataukm $dataukm)
    {
      
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\dataukm  $dataukm
     * @return \Illuminate\Http\Response
     */
    public function edit(dataukm $dataukm)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\dataukm  $dataukm
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, dataukm $dataukm)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\dataukm  $dataukm
     * @return \Illuminate\Http\Response
     */
    public function destroy(dataukm $dataukm)
    {
        //
    }

    
   
}
