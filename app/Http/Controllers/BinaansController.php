<?php

namespace App\Http\Controllers;
use App\Http\Requests;
use App\Pribadi;
use App\kecamatanmodel;
use App\Usaha;
use App\lainnya;
use App\UploadDokumen;
use App\Http\Controllers;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\DB;

class BinaansController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     
    public function __construct()
    {
        $this->middleware('verified');
    }
    
    public function index()
    {
        // return view('binaans.index');
        // return Datatables::of(Pribadi::query())->make(true);
        // $list_binaan = Pribadi::all();
        // dd($list_binaan);
        // return response()->json($list_binaan);
        // $pribadi = Pribadi::all();

        // $list_binaan =  $pribadi->fresh('Usaharelasi');
        // return view('binaans.index',compact('list_binaan'));

        $member = DB::table('db_pribadi')
        ->join('db_perusahaan', 'db_pribadi.id_anggota','=','db_perusahaan.id_anggota')
        ->join('db_kecamatan', 'db_pribadi.id_kecamatan','=','db_kecamatan.id_kecamatan')
        ->get();
        
       if((auth()->user()->role == 'superadmin') or (auth()->user()->role == 'Bid_UMKM')){
        return view('dataukm.view',compact('member'));
       }
       else{
        return view('dataukm.roleinfo');
       }

    }

   

    public function mikro()
    {
        // return view('binaans.index');
        // return Datatables::of(Pribadi::query())->make(true);
        // $list_binaan = Pribadi::all();
        // dd($list_binaan);
        // return response()->json($list_binaan);
        // $pribadi = Pribadi::all();

        // $list_binaan =  $pribadi->fresh('Usaharelasi');
        // return view('binaans.index',compact('list_binaan'));

        $member = DB::table('db_pribadi')
        ->join('db_perusahaan', 'db_pribadi.id_anggota','=','db_perusahaan.id_anggota')
        ->join('db_kecamatan', 'db_pribadi.id_kecamatan','=','db_kecamatan.id_kecamatan')
        ->where('db_perusahaan.jenis_omset','=','Mikro')
        ->get();
        
       
        if((auth()->user()->role == 'superadmin') or (auth()->user()->role == 'Bid_UMKM')){
            return view('dataukm.view',compact('member'));
           }
           else{
            return view('dataukm.roleinfo');
           }

    }
    public function kecil()
    {
        // return view('binaans.index');
        // return Datatables::of(Pribadi::query())->make(true);
        // $list_binaan = Pribadi::all();
        // dd($list_binaan);
        // return response()->json($list_binaan);
        // $pribadi = Pribadi::all();

        // $list_binaan =  $pribadi->fresh('Usaharelasi');
        // return view('binaans.index',compact('list_binaan'));

        $member = DB::table('db_pribadi')
        ->join('db_perusahaan', 'db_pribadi.id_anggota','=','db_perusahaan.id_anggota')
        ->join('db_kecamatan', 'db_pribadi.id_kecamatan','=','db_kecamatan.id_kecamatan')
        ->where('db_perusahaan.jenis_omset','=','Kecil')
        ->get();
        
       
        if((auth()->user()->role == 'superadmin') or (auth()->user()->role == 'Bid_UMKM')){
            return view('dataukm.view',compact('member'));
           }
           else{
            return view('dataukm.roleinfo');
           }

    }

    public function menengah()
    {
        // return view('binaans.index');
        // return Datatables::of(Pribadi::query())->make(true);
        // $list_binaan = Pribadi::all();
        // dd($list_binaan);
        // return response()->json($list_binaan);
        // $pribadi = Pribadi::all();

        // $list_binaan =  $pribadi->fresh('Usaharelasi');
        // return view('binaans.index',compact('list_binaan'));

        $member = DB::table('db_pribadi')
        ->join('db_perusahaan', 'db_pribadi.id_anggota','=','db_perusahaan.id_anggota')
        ->join('db_kecamatan', 'db_pribadi.id_kecamatan','=','db_kecamatan.id_kecamatan')
        ->where('db_perusahaan.jenis_omset','=','Menengah')
        ->get();
        
       
        if((auth()->user()->role == 'superadmin') or (auth()->user()->role == 'Bid_UMKM')){
            return view('dataukm.view',compact('member'));
           }
           else{
            return view('dataukm.roleinfo');
           }

    }

    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kec = DB::table('db_kecamatan')->get();
        $kel = DB::table('db_kelurahan')->get();
        // Get the last order id
        $lastorderId = DB::table('db_pribadi')->orderBy('id_anggota', 'desc')->first()->id_anggota;

        // Get last 3 digits of last order id
        $lastIncreament = substr($lastorderId, -3);

        // Make a new order id with appending last increment + 1
        $newOrderId = 'UMKM' . date('Ymds') . str_pad($lastIncreament + 1, 3, 0, STR_PAD_LEFT);
       
        if((auth()->user()->role == 'superadmin') or (auth()->user()->role == 'Bid_UMKM')){
            // return view('dataukm.view',compact('member'));
            return view('dataukm.create',compact('kec','newOrderId', 'kel'));
           }
           else{
            return view('dataukm.roleinfo');
           }
    }
    public function cari($id)
    {
       
        $kelurahan = DB::table('db_kelurahan')->where('db_kelurahan.id_kecamatan','=', $id)->get();
        foreach($kelurahan AS $data){
            $id         = $data->id_kelurahan;
            $title      = $data->kelurahan;
           ?>
            <option value='<?php echo $id; ?>'><?php echo $title; ?></option>
            <?php
       }
    }

    public function carinikumkm($id)
    {
      
        $result = DB::table('db_pribadi')->where('db_pribadi.nik','=', $id)->get();
        if(count($result) > 0){
            
            echo "<span style='color:red;'>MAAF NIK SUDAH TERSEDIA, SIlAHKAN UPDATE DATA
            
            </span>";
       
        }
      else{
        echo "<span style='color:green;'>NIK TERSEDIA</span>";
      }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    //   dd($request->ktp_upload);
        if ($request->ktp_upload == null) {
            $request->validate([
                'nik'=>'required|size:16',
                'nama_dpn'=>'required',
                'alamat'=>'required',
                'hp'=>'required',
                'rt'=>'required',
                'rw'=>'required',
                'telp'=>'required',
                'email'=>'required',
                'jeniskelamin'=>'required',
                'kecamatan'=>'required',
                'kelurahan'=>'required',
                'nama_usaha'=>'required',
                'alamat_usaha'=>'required',
                'thn_pendaftaran'=>'required',
                'thn_berdiri'=>'required',
                'bentuk_usaha'=>'required',
                'jenis_usaha'=>'required',
                'spesifikasi_produk1'=>'required',
                'omset'=>'required',
                'aset'=>'required',
                'nib'=>'required',
                'npwp'=>'required',
                
            ]);
    
            
    
            $nominal =preg_replace('/\D/', '', $request->nominalpinjaman);
            $sendiri =preg_replace('/\D/', '', $request->mdl_sendiri);
            $luar =preg_replace('/\D/', '', $request->mdl_luar);
            $omset = preg_replace('/\D/', '', $request->omset);
            $aset = preg_replace('/\D/', '', $request->aset);
    
            $bahan = implode(',', $request->bahan_baku);
            $penjamin1 = implode(',', $request->penjamin);
            $media = implode(',', $request->mediapenjualan);
            $pasar = implode(',', $request->pasar);
            $kerjasama1 = implode(',', $request->kerjasama);
            $ijin1 = implode(',', $request->ijin);
            
            if ($omset <= 300000000) {
                $jenis_omset = "Mikro";
            } 
            else if (($omset >= 300000000) && ($omset <= 2500000000)){
                $jenis_omset = "Kecil";
            } 
            
            else if (($omset >= 2500000000) && ($omset <= 50000000000 )) {
                $jenis_omset = "Menengah";
            }
    
            if ($aset <= 50000000) {
                $jenis_aset = "Mikro";
            } 
            else if (($aset >= 50000000) && ($aset <= 500000000)){
                $jenis_aset = "Kecil";
            } 
            
            else if (($aset >= 500000000) && ($aset <= 10000000000 )) {
                $jenis_aset = "Menengah";
            }
    
           
    
            Pribadi::create([
                // $data=[
                'id_anggota'=>$request->id_anggota,
                'nik'=>$request->nik,
                'nama_depan'=>$request->nama_dpn,
                'jenis_kelamin'=>$request->jeniskelamin,
                'alamat'=>$request->alamat,
                'rt'=>$request->rt,
                'rw'=>$request->rw,
                'no_telepon'=>$request->telp,
                'no_handphone'=>$request->hp,
                'email'=>$request->email,
                'id_kelurahan'=>$request->kelurahan,
                'id_kecamatan'=>$request->kecamatan,
                'kota'=>$request->kota,
                'kode_pos'=>$request->kode_pos,
                'pendidikan'=>$request->pendidikan,
                'aktifasi'=>$request->aktivasi,
                'tanggal_input'=>now()
                // ];
            ]);
    
            // end data pribadi
    
            // data perusahaan
                Usaha::create([
                    'id_anggota'=>$request->id_anggota,
                    'nama_usaha'=>$request->nama_usaha,
                    'alamat_usaha'=>$request->alamat_usaha,
                    'rt_usaha'=>$request->rt_usaha,
                    'rw_usaha'=>$request->rw_usaha,
                    'no_telepon_usaha'=>$request->telp_usaha,
                    'thn_daftar'=>$request->thn_pendaftaran,
                    'thn_berdiri'=>$request->thn_berdiri,
                    'bentuk_usaha'=>$request->bentuk_usaha,
                    'jenis_usaha'=>$request->jenis_usaha,
                    'produk_1'=>$request->spesifikasi_produk1,
                    'produk_2'=>$request->spesifikasi_produk2,
                    'produk_3'=>$request->spesifikasi_produk3,
                    'bahan_utama'=>$request->bahanbakuutama,
                    'perolehan'=>$bahan,
                    'detail_perolehan'=>$request->perolehan,
                    'jumlah_karyawan'=>$request->jumlah_karyawan,
                    'manajemen_keuangan'=>$request->keuangan,
                    'detail_manajemen'=>$request->keuangandetail,
                    'modal_sendiri'=>$sendiri,
                    'modal_luar'=>$luar,
                    'penjamin'=>$penjamin1,
                    'nominal'=>$nominal,
                    'detail_penjamin'=>$request->penjamindetail,
                    'jenis_omset'=>$jenis_omset,
                    'jumlah_omset'=>$omset,
                    'jumlah_aset'=>$aset,
                    'jenis_aset'=>$jenis_aset,
                    'media_penjualan'=>$media,
                    'detail_media'=>$request->detailmediapenjualan,
                    'kerjasama'=>$kerjasama1,
                    'detailkerjasama'=>$request->detailkerjasama,
                    'tanggal_input'=>now()
                ]);
            // end data peprusahaan
            
            //  data lain
    
            lainnya::create([
                'id_anggota'=>$request->id_anggota,
                'bahan_baku'=>$bahan,
                'pemasaran'=>$pasar,
                'detailpemasaran'=>$request->detailpasar,
                'pelatihan'=>$request->keterangan,
                'perijinan'=>$ijin1,
                'nib'=>$request->nib,
                'npwp'=>$request->npwp,
                'haki'=>$request->haki,
                'pirt'=>$request->pirt,
                'halal'=>$request->halal,
                'bpom'=>$request->bpom,
                'tanggal_input'=>now()
            ]);
            
           
            UploadDokumen::create([
                'id_anggota'=>$request->id_anggota,
                'ktp' =>"",
                'npwp'=>"",
                'pasfoto'=>"",
                'foto1'=>""
                
                // 'foto3'=>$nama_foto3,
    
            ]);
    
            //  end data lain
    
            
            return redirect('/dashboard')->with('statustambah', 'Data Berhasil ditambah!!!');
        } else {
            $request->validate([
                'nik'=>'required|size:16',
                'nama_dpn'=>'required',
                'alamat'=>'required',
                'hp'=>'required',
                'rt'=>'required',
                'rw'=>'required',
                'telp'=>'required',
                'email'=>'required',
                'jeniskelamin'=>'required',
                'kecamatan'=>'required',
                'kelurahan'=>'required',
                'nama_usaha'=>'required',
                'alamat_usaha'=>'required',
                'thn_pendaftaran'=>'required',
                'thn_berdiri'=>'required',
                'bentuk_usaha'=>'required',
                'jenis_usaha'=>'required',
                'spesifikasi_produk1'=>'required',
                'omset'=>'required',
                'aset'=>'required',
                'nib'=>'required',
                'npwp'=>'required',
                'ktp_upload'=> 'required|file|image|mimes:jpeg,png,jpg|max:10048',
                'npwp_upload'=> 'required|file|image|mimes:jpeg,png,jpg|max:10048',
                'foto1_upload'=> 'required|file|image|mimes:jpeg,png,jpg|max:10048',
                'foto2_upload'=> 'required|file|image|mimes:jpeg,png,jpg|max:10048',
            ]);
    
            
    
            $nominal =preg_replace('/\D/', '', $request->nominalpinjaman);
            $sendiri =preg_replace('/\D/', '', $request->mdl_sendiri);
            $luar =preg_replace('/\D/', '', $request->mdl_luar);
            $omset = preg_replace('/\D/', '', $request->omset);
            $aset = preg_replace('/\D/', '', $request->aset);
    
            $bahan = implode(',', $request->bahan_baku);
            $penjamin1 = implode(',', $request->penjamin);
            $media = implode(',', $request->mediapenjualan);
            $pasar = implode(',', $request->pasar);
            $kerjasama1 = implode(',', $request->kerjasama);
            $ijin1 = implode(',', $request->ijin);
            
            if ($omset <= 300000000) {
                $jenis_omset = "Mikro";
            } 
            else if (($omset >= 300000000) && ($omset <= 2500000000)){
                $jenis_omset = "Kecil";
            } 
            
            else if (($omset >= 2500000000) && ($omset <= 50000000000 )) {
                $jenis_omset = "Menengah";
            }
    
            if ($aset <= 50000000) {
                $jenis_aset = "Mikro";
            } 
            else if (($aset >= 50000000) && ($aset <= 500000000)){
                $jenis_aset = "Kecil";
            } 
            
            else if (($aset >= 500000000) && ($aset <= 10000000000 )) {
                $jenis_aset = "Menengah";
            }
    
            $ktp = $request->file('ktp_upload');
            
            $npwp = $request->file('npwp_upload');
            $pasfoto = $request->file('foto1_upload');
            $foto2 = $request->file('foto2_upload');
            // $foto3 = $request->file('foto3_upload');
     
            
            $nama_ktp = time()."_".$ktp->getClientOriginalName();
            $nama_npwp = time()."_".$npwp->getClientOriginalName();
            $nama_foto1 = time()."_".$pasfoto->getClientOriginalName();
            $nama_foto2 = time()."_".$foto2->getClientOriginalName();
            // $nama_foto3 = time()."_".$foto3->getClientOriginalName();
     
                      // isi dengan nama folder tempat kemana file diupload
            $tujuan_upload = 'data_file';
            
            $ktp->move($tujuan_upload,$nama_ktp);
            $npwp->move($tujuan_upload,$nama_npwp);
            $pasfoto->move($tujuan_upload,$nama_foto1);
            $foto2->move($tujuan_upload,$nama_foto2);
            // $foto3->move($tujuan_upload,$nama_foto3);
            
            
    
            // data pribadi
    
            Pribadi::create([
                // $data=[
                'id_anggota'=>$request->id_anggota,
                'nik'=>$request->nik,
                'nama_depan'=>$request->nama_dpn,
                'jenis_kelamin'=>$request->jeniskelamin,
                'alamat'=>$request->alamat,
                'rt'=>$request->rt,
                'rw'=>$request->rw,
                'no_telepon'=>$request->telp,
                'no_handphone'=>$request->hp,
                'email'=>$request->email,
                'id_kelurahan'=>$request->kelurahan,
                'id_kecamatan'=>$request->kecamatan,
                'kota'=>$request->kota,
                'kode_pos'=>$request->kode_pos,
                'pendidikan'=>$request->pendidikan,
                'aktifasi'=>$request->aktivasi,
                'tanggal_input'=>now()
                // ];
            ]);
    
            // end data pribadi
    
            // data perusahaan
                Usaha::create([
                    'id_anggota'=>$request->id_anggota,
                    'nama_usaha'=>$request->nama_usaha,
                    'alamat_usaha'=>$request->alamat_usaha,
                    'rt_usaha'=>$request->rt_usaha,
                    'rw_usaha'=>$request->rw_usaha,
                    'no_telepon_usaha'=>$request->telp_usaha,
                    'thn_daftar'=>$request->thn_pendaftaran,
                    'thn_berdiri'=>$request->thn_berdiri,
                    'bentuk_usaha'=>$request->bentuk_usaha,
                    'jenis_usaha'=>$request->jenis_usaha,
                    'produk_1'=>$request->spesifikasi_produk1,
                    'produk_2'=>$request->spesifikasi_produk2,
                    'produk_3'=>$request->spesifikasi_produk3,
                    'bahan_utama'=>$request->bahanbakuutama,
                    'perolehan'=>$bahan,
                    'detail_perolehan'=>$request->perolehan,
                    'jumlah_karyawan'=>$request->jumlah_karyawan,
                    'manajemen_keuangan'=>$request->keuangan,
                    'detail_manajemen'=>$request->keuangandetail,
                    'modal_sendiri'=>$sendiri,
                    'modal_luar'=>$luar,
                    'penjamin'=>$penjamin1,
                    'nominal'=>$nominal,
                    'detail_penjamin'=>$request->penjamindetail,
                    'jenis_omset'=>$jenis_omset,
                    'jumlah_omset'=>$omset,
                    'jumlah_aset'=>$aset,
                    'jenis_aset'=>$jenis_aset,
                    'media_penjualan'=>$media,
                    'detail_media'=>$request->detailmediapenjualan,
                    'kerjasama'=>$kerjasama1,
                    'detailkerjasama'=>$request->detailkerjasama,
                    'tanggal_input'=>now()
                ]);
            // end data peprusahaan
            
            //  data lain
    
            lainnya::create([
                'id_anggota'=>$request->id_anggota,
                'bahan_baku'=>$bahan,
                'pemasaran'=>$pasar,
                'detailpemasaran'=>$request->detailpasar,
                'pelatihan'=>$request->keterangan,
                'perijinan'=>$ijin1,
                'nib'=>$request->nib,
                'npwp'=>$request->npwp,
                'haki'=>$request->haki,
                'pirt'=>$request->pirt,
                'halal'=>$request->halal,
                'bpom'=>$request->bpom,
                'tanggal_input'=>now()
            ]);
            
           
            UploadDokumen::create([
                'id_anggota'=>$request->id_anggota,
                'ktp' =>$nama_ktp,
                'npwp'=>$nama_npwp,
                'pasfoto'=>$nama_foto1,
                'foto1'=>$nama_foto2
                
                // 'foto3'=>$nama_foto3,
    
            ]);
    
            //  end data lain
    
            
            return redirect('/dashboard')->with('statustambah', 'Data Berhasil ditambah!!!');
        }
        
        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pribadi  $pribadi
     * @return \Illuminate\Http\Response
     */
    public function show(Pribadi $pribadi)
    {
    //    
        // return $pribadi;
        $id = $pribadi->id_anggota;
      
      
      $v1 = DB::table('db_pribadi')
      ->join('db_perusahaan', 'db_pribadi.id_anggota','=','db_perusahaan.id_anggota')
      ->join('db_lain', 'db_pribadi.id_anggota','=','db_lain.id_anggota')
      ->join('db_kecamatan', 'db_pribadi.id_kecamatan','=','db_kecamatan.id_kecamatan')
      ->join('db_kelurahan', 'db_pribadi.id_kelurahan','=','db_kelurahan.id_kelurahan')
      ->where('db_pribadi.id_anggota', '=', $id)
        ->get();
       
        $gambar = DB::table('monev_upload')
                ->where('monev_upload.id_anggota','=', $id)
                ->get();
        $countgambar = count($gambar);
        return view('dataukm.index',compact('v1','gambar','countgambar'));
         
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pribadi  $pribadi
     * @return \Illuminate\Http\Response
     */
    public function edit(Pribadi $pribadi)
    {
        $id = $pribadi->id_anggota;
      
      
      $v1 = DB::table('db_pribadi')
      ->join('db_perusahaan', 'db_pribadi.id_anggota','=','db_perusahaan.id_anggota')
      ->join('db_lain', 'db_pribadi.id_anggota','=','db_lain.id_anggota')
      ->join('db_kecamatan', 'db_pribadi.id_kecamatan','=','db_kecamatan.id_kecamatan')
      ->join('db_kelurahan', 'db_pribadi.id_kelurahan','=','db_kelurahan.id_kelurahan')
      ->where('db_pribadi.id_anggota', '=', $id)
        ->get();
        $kec = DB::table('db_kecamatan')->get();
        $kel = DB::table('db_kelurahan')->get();
        return view('dataukm.edit',compact('v1','kec','kel'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pribadi  $pribadi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pribadi $pribadi)
    {
        $nominal =preg_replace('/\D/', '', $request->nominalpinjaman);
        $sendiri =preg_replace('/\D/', '', $request->mdl_sendiri);
        $luar =preg_replace('/\D/', '', $request->mdl_luar);
        $omset = preg_replace('/\D/', '', $request->omset);
        $aset = preg_replace('/\D/', '', $request->aset);

        $bahan = implode(',', $request->bahan_baku);
        $penjamin1 = implode(',', $request->penjamin);
        $media = implode(',', $request->mediapenjualan);
        $pasar = implode(',', $request->pasar);
        $kerjasama1 = implode(',', $request->kerjasama);
        $ijin1 = implode(',', $request->ijin);
        
        if ($omset <= 300000000) {
            $jenis_omset = "Mikro";
        } 
        else if (($omset >= 300000000) && ($omset <= 2500000000)){
            $jenis_omset = "Kecil";
        } 
        
        else if (($omset >= 2500000000) && ($omset <= 50000000000 )) {
            $jenis_omset = "Menengah";
        }

        if ($aset <= 50000000) {
            $jenis_aset = "Mikro";
        } 
        else if (($aset >= 50000000) && ($aset <= 500000000)){
            $jenis_aset = "Kecil";
        } 
        
        else if (($aset >= 500000000) && ($aset <= 10000000000 )) {
            $jenis_aset = "Menengah";
        }


         // data pribadi

         Pribadi::where('id_anggota',$pribadi->id_anggota)
                    ->update([
                        // $data=[
                        
                        'nik'=>$request->nik,
                        'nama_depan'=>$request->nama_dpn,
                        'jenis_kelamin'=>$request->jeniskelamin,
                        'alamat'=>$request->alamat,
                        'rt'=>$request->rt,
                        'rw'=>$request->rw,
                        'no_telepon'=>$request->telp,
                        'no_handphone'=>$request->hp,
                        'email'=>$request->email,
                        'id_kelurahan'=>$request->kelurahan,
                        'id_kecamatan'=>$request->kecamatan,
                        'kota'=>$request->kota,
                        'kode_pos'=>$request->kode_pos,
                        'pendidikan'=>$request->pendidikan,
                        'aktifasi'=>$request->aktivasi,
                        'tanggal_rubah'=>now()
                        // ];
                    ]);

        // end data pribadi

        // data perusahaan
        Usaha::where('id_anggota',$pribadi->id_anggota)
                ->update([
                    
                    'nama_usaha'=>$request->nama_usaha,
                    'alamat_usaha'=>$request->alamat_usaha,
                    'rt_usaha'=>$request->rt_usaha,
                    'rw_usaha'=>$request->rw_usaha,
                    'no_telepon_usaha'=>$request->telp_usaha,
                    'thn_daftar'=>$request->thn_pendaftaran,
                    'thn_berdiri'=>$request->thn_berdiri,
                    'bentuk_usaha'=>$request->bentuk_usaha,
                    'jenis_usaha'=>$request->jenis_usaha,
                    'produk_1'=>$request->spesifikasi_produk1,
                    'produk_2'=>$request->spesifikasi_produk2,
                    'produk_3'=>$request->spesifikasi_produk3,
                    'bahan_utama'=>$request->bahanbakuutama,
                    'perolehan'=>$bahan,
                    'detail_perolehan'=>$request->perolehan,
                    'jumlah_karyawan'=>$request->jumlah_karyawan,
                    'manajemen_keuangan'=>$request->keuangan,
                    'detail_manajemen'=>$request->keuangandetail,
                    'modal_sendiri'=>$sendiri,
                    'modal_luar'=>$luar,
                    'penjamin'=>$penjamin1,
                    'nominal'=>$nominal,
                    'detail_penjamin'=>$request->penjamindetail,
                    'jenis_omset'=>$jenis_omset,
                    'jumlah_omset'=>$omset,
                    'jumlah_aset'=>$aset,
                    'jenis_aset'=>$jenis_aset,
                    'media_penjualan'=>$media,
                    'detail_media'=>$request->detailmediapenjualan,
                    'kerjasama'=>$kerjasama1,
                    'detailkerjasama'=>$request->detailkerjasama,
                    'tanggal_rubah'=>now()
                ]);
    // end data peprusahaan
                     //  data lain

        lainnya::where('id_anggota',$pribadi->id_anggota)
                    ->update([
                        
                        'bahan_baku'=>$bahan,
                        'pemasaran'=>$pasar,
                        'detailpemasaran'=>$request->detailpasar,
                        'pelatihan'=>$request->keterangan,
                        'perijinan'=>$ijin1,
                        'nib'=>$request->nib,
                        'npwp'=>$request->npwp,
                        'haki'=>$request->haki,
                        'pirt'=>$request->pirt,
                        'halal'=>$request->halal,
                        'bpom'=>$request->bpom,
                        'tanggal_rubah'=>now()
                    ]);

        //  end data lain
    
        
        return redirect('/binaans')->with('statustambah', 'Data Berhasil diupdate!!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pribadi  $pribadi
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pribadi $pribadi)
    {
        Pribadi::destroy($pribadi->id_anggota);
        Usaha::destroy($pribadi->id_anggota);
        lainnya::destroy($pribadi->id_anggota);
        UploadDokumen::destroy($pribadi->id_anggota);
        
        return redirect('/binaans')->with('statustambah', 'Data Berhasil dihapus!!!');
    }
}
