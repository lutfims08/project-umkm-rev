<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\DataTables\LaporanTahunanDataTable;
use PDF;

class LaporanController extends Controller
{
    public function alldata()
    {
        // $id = $request->thn;
        $thn = DB::table('db_pribadi')
        ->join('db_perusahaan', 'db_pribadi.id_anggota','=','db_perusahaan.id_anggota')
        ->join('db_lain', 'db_pribadi.id_anggota','=','db_lain.id_anggota')
        ->join('db_kecamatan', 'db_pribadi.id_kecamatan','=','db_kecamatan.id_kecamatan')
        ->join('db_kelurahan', 'db_pribadi.id_kelurahan','=','db_kelurahan.id_kelurahan')
        // ->where('db_perusahaan.thn_daftar', '=', $id)
          ->get();
          $title = "";
          return view("laporan.outputtahun" ,compact('thn','title'));
    }  

    public function index()
    {
        $sqlthn    = DB::table('db_pribadi')
        ->select(DB::raw('db_perusahaan.thn_daftar as thndaftar '),
                 DB::raw('count(db_perusahaan.thn_daftar) as jumlahthndaftar '))  
        ->join('db_perusahaan', 'db_pribadi.id_anggota','=','db_perusahaan.id_anggota')
        ->groupBy('thn_daftar')
        ->get();
        $jenisusaha    = DB::table('db_perusahaan')
        ->select(DB::raw('db_perusahaan.jenis_usaha as jenis '))  
        ->groupBy('jenis_usaha')
        ->get();
        $kecamatan = DB::table('db_kecamatan')->get();
        return view("laporan.index" ,compact('sqlthn','kecamatan','jenisusaha'));
    }

    public function resulttahun(Request $request)
    {
        $id = $request->thn;
        $thn = DB::table('db_pribadi')
        ->join('db_perusahaan', 'db_pribadi.id_anggota','=','db_perusahaan.id_anggota')
        ->join('db_lain', 'db_pribadi.id_anggota','=','db_lain.id_anggota')
        ->join('db_kecamatan', 'db_pribadi.id_kecamatan','=','db_kecamatan.id_kecamatan')
        ->join('db_kelurahan', 'db_pribadi.id_kelurahan','=','db_kelurahan.id_kelurahan')
        ->where('db_perusahaan.thn_daftar', '=', $id)
          ->get();
          $title = "Per Tahun $id";
          return view("laporan.outputtahun" ,compact('thn','title'));
        // dd($id);
        //   $pdf = PDF::loadView('laporan.outputtahun', compact('thn'))->setPaper('F4', 'landscape');
        //     return $pdf->stream();

            // $pdf = PDF::loadView('pdf.invoice', $data);
            // return $pdf->download('invoice.pdf');
    }
    public function resultjenis(Request $request)
    {
        $id = $request->jenis;
        $thn = DB::table('db_pribadi')
        ->join('db_perusahaan', 'db_pribadi.id_anggota','=','db_perusahaan.id_anggota')
        ->join('db_lain', 'db_pribadi.id_anggota','=','db_lain.id_anggota')
        ->join('db_kecamatan', 'db_pribadi.id_kecamatan','=','db_kecamatan.id_kecamatan')
        ->join('db_kelurahan', 'db_pribadi.id_kelurahan','=','db_kelurahan.id_kelurahan')
        ->where('db_perusahaan.jenis_omset', '=', $id)
          ->get();
          $title = "Per Klasifikasi Usaha $id";
          return view("laporan.outputtahun" ,compact('thn','title'));
        // dd($id);
        //   $pdf = PDF::loadView('laporan.outputtahun', compact('thn'))->setPaper('F4', 'landscape');
        //     return $pdf->stream();

            // $pdf = PDF::loadView('pdf.invoice', $data);
            // return $pdf->download('invoice.pdf');
    }
    public function resultjenis1(Request $request)
    {
        $id = $request->jenis1;
        $thn = DB::table('db_pribadi')
        ->join('db_perusahaan', 'db_pribadi.id_anggota','=','db_perusahaan.id_anggota')
        ->join('db_lain', 'db_pribadi.id_anggota','=','db_lain.id_anggota')
        ->join('db_kecamatan', 'db_pribadi.id_kecamatan','=','db_kecamatan.id_kecamatan')
        ->join('db_kelurahan', 'db_pribadi.id_kelurahan','=','db_kelurahan.id_kelurahan')
        ->where('db_perusahaan.jenis_usaha', '=', $id)
          ->get();
          $title = "Per Jenis Usaha $id";
          return view("laporan.outputtahun" ,compact('thn','title'));
        // dd($id);
        //   $pdf = PDF::loadView('laporan.outputtahun', compact('thn'))->setPaper('F4', 'landscape');
        //     return $pdf->stream();

            // $pdf = PDF::loadView('pdf.invoice', $data);
            // return $pdf->download('invoice.pdf');
    }

    public function resultkecamatan(Request $request)
    {
        $id = $request->kecamatan;
        $thn = DB::table('db_pribadi')
        ->join('db_perusahaan', 'db_pribadi.id_anggota','=','db_perusahaan.id_anggota')
        ->join('db_lain', 'db_pribadi.id_anggota','=','db_lain.id_anggota')
        ->join('db_kecamatan', 'db_pribadi.id_kecamatan','=','db_kecamatan.id_kecamatan')
        ->join('db_kelurahan', 'db_pribadi.id_kelurahan','=','db_kelurahan.id_kelurahan')
        ->where('db_pribadi.id_kecamatan', '=', $id)
          ->get();
        $kecamatan = DB::table('db_kecamatan')
        ->select(DB::raw('db_kecamatan.kecamatan as kec '))
        ->where('db_kecamatan.id_kecamatan','=',$id)
        ->get();
        foreach ($kecamatan as $kec) {
            $data[]=[
                $kec->kec
            ];
        }
        
          $title = "Per Kecamatan $kec->kec";
          return view("laporan.outputtahun" ,compact('thn','title'));
        // dd($id);
        //   $pdf = PDF::loadView('laporan.outputtahun', compact('thn'))->setPaper('F4', 'landscape');
        //     return $pdf->stream();

            // $pdf = PDF::loadView('pdf.invoice', $data);
            // return $pdf->download('invoice.pdf');
    }
    
    
}
