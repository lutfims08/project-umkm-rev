<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kegiatan extends Model
{
    protected $table = 'db_d_kegiatan';
    public $incrementing = false;
    protected $fillable =
    [
        'id_kegiatan','id_anggota'];

}
