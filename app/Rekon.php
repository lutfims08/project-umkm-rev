<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rekon extends Model
{
    protected $table = 'db_bpum_all';
    protected $fillable =['NIK','NAMA_LENGKAP','ALAMAT_LENGKAP','KECAMATAN','KELURAHAN','NO_TELP'];
    public $incrementing = false;
}
