<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dkegiatan extends Model
{
    protected $table = 'db_kegiatan-old';
    public $incrementing = false;
    protected $fillable =
    [
        'id_kegiatan','nama_kegiatan','deskripsi','jumlah_peserta','tempat_kegiatan'
    ];
}
