<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class monevlainnya extends Model
{
    protected $table = 'monev_lainnya';
    public $incrementing = false;
    public $primaryKey = 'id_anggota';
    protected $fillable =
    [
        'id_anggota','bahan_baku','pemasaran',
        'detailpemasaran','pelatihan','perijinan',
        'nib','npwp','haki','pirt','halal','bpom','created_at'
    ];
}
