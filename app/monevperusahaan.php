<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class monevperusahaan extends Model
{
    protected $table = 'monev_perusahaan';
    public $incrementing = false;
    public $primaryKey = 'id_anggota';
    protected $fillable =
    ['id_anggota','nama_usaha','alamat_usaha','rt_usaha',
    'rw_usaha','no_telepon_usaha','thn_daftar','thn_berdiri',
    'bentuk_usaha','jenis_usaha','produk_1','produk_2','produk_3',
    'bahan_utama','perolehan','detail_perolehan','jumlah_karyawan',
    'manajemen_keuangan','detail_manajemen','modal_sendiri','modal_luar',
    'penjamin','detail_penjamin', 'nominal','jenis_omset','jumlah_omset','jumlah_aset',
    'jenis_aset','media_penjualan','detail_media','kerjasama','detailkerjasama','created_at'];
}
