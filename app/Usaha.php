<?php

namespace App;
use App\Pribadi;
use Illuminate\Database\Eloquent\Model;

class Usaha extends Model
{
    protected $table = 'db_perusahaan';
    protected $fillable =
    ['id_anggota','nama_usaha','alamat_usaha','rt_usaha',
    'rw_usaha','no_telepon_usaha','thn_daftar','thn_berdiri',
    'bentuk_usaha','jenis_usaha','produk_1','produk_2','produk_3',
    'bahan_utama','perolehan','detail_perolehan','jumlah_karyawan',
    'manajemen_keuangan','detail_manajemen','modal_sendiri','modal_luar',
    'penjamin','detail_penjamin', 'nominal','jenis_omset','jumlah_omset','jumlah_aset',
    'jenis_aset','media_penjualan','detail_media','kerjasama','detailkerjasama','tanggal_input'];
    // protected $foreignKey = 'id_anggota';
    public $primaryKey = 'id_anggota';
    // public $incrementing = false;

   
   
    public function Pribadirelasi()
    {
        // $Usaha = Pribadi::get();
        return $this->belongsTo(Pribadi::class);
        
    }
}
