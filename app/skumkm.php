<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class skumkm extends Model
{
    protected $table = 'db_sk_umkm';
    protected $fillable =['id_anggota','nik','nama_depan','nama_usaha','tgl_permohonan','tgl_pembuatan'];
    public $incrementing = false;
    public $primaryKey = 'id_anggota';
}
