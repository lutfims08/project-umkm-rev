<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UploadDokumen extends Model
{
    protected $table = "db_upload";
    public $incrementing = false;
    public $primaryKey = 'id_anggota';
    protected $fillable = ['id_anggota','ktp','npwp','foto1','pasfoto'];
}
