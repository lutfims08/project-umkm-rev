<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kecamatanmodel extends Model
{
    protected $table = 'db_kecamatan';
    public $primaryKey = 'id_kecamatan';
    public $incrementing = false;


    public function kecamatanrelasi()
    {
        return $this->belongsToMany(Pribadi::class);
    }
}
