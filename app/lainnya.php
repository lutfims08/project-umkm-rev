<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class lainnya extends Model
{
    protected $table = 'db_lain';
    public $primaryKey = 'id_anggota';
    public $incrementing = false;
    protected $fillable =
    [
        'id_anggota','bahan_baku','pemasaran',
        'detailpemasaran','pelatihan','perijinan',
        'nib','npwp','haki','pirt','halal','bpom','tanggal_input'
    ];
}
