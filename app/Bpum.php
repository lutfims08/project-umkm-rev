<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bpum extends Model
{
    protected $table = 'bpums';
    protected $fillable =['nik','nama_lengkap','alamat','bidang_usaha','no_telp','no_reg','kecamatan','kelurahan','status','keterangan'];
    public $incrementing = false;
}
