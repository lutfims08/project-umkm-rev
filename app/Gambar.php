<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gambar extends Model
{
    protected $table = "monev_upload";
 
    protected $fillable = ['id_anggota','file','keterangan'];
}
