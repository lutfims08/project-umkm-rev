<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bansos extends Model
{
    protected $table = 'db_bansos';
    protected $fillable =['NIK','NAMA_LENGKAP','ALAMAT_LENGKAP','status','VERIFIKASI','KELURAHAN','KECAMATAN','NO_TELP','NAMA_USAHA','ALAMAT_USAHA','JENIS_USAHA','CEKLIST_PERSYARATAN'];
    public $incrementing = false;
}
